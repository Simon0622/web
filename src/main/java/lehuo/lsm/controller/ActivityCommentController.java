package lehuo.lsm.controller;

import lehuo.lsm.model.ActivityComment;
import lehuo.lsm.model.User;
import lehuo.lsm.service.impl.ActivityCommentService;
import lehuo.lsm.service.impl.UserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityCommentController {

    protected static Logger logger = LoggerFactory.getLogger(ActivityCommentController.class);
    @Resource
    ActivityCommentService activityCommentService;

    @Resource
    private UserService userService;

    @RequestMapping(value = "/addActivityComment")
    @ResponseBody
    public String addActivityComment(ActivityComment comment,HttpSession session){

        String name = (String)session.getAttribute("name");

        if(StringUtils.isBlank(name)){
            return "false";
        }else{
            String sessionid = (String)session.getId();
            User  user = userService.select(new User(name));
            comment.setUserid(user.getId());
            activityCommentService.insert(comment);
            return "true";
        }

    }

    @RequestMapping(value = "/adminDeletecomment")
    @ResponseBody
    public void deleteComment(Integer id){
        ActivityComment ac = new ActivityComment();
        ac.setId(id);
        activityCommentService.delete(ac);
    }

}
