package lehuo.lsm.controller;

import com.alibaba.fastjson.JSON;
import lehuo.lsm.global.Global;
import lehuo.lsm.model.*;
import lehuo.lsm.service.impl.*;
import lehuo.lsm.util.ImgDownload;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/activity")
public class ActivityController {

    protected static Logger logger = LoggerFactory.getLogger(RankController.class);

    private static final int PAGE = 10;

    @Resource
    private ActivityService activityService;

    @Resource
    private UserService userService;

    @Resource
    private ImagesService imagesService;

    @Resource
    private ActivityCommentService activityCommentService;

    @RequestMapping(value = "/index")
    public String index(ModelMap map,Integer currentPage,String querytype,String hasloc,
                        String hasphoto,String keyword){
        List<Activity> list = null;

        if(currentPage==null){
            currentPage=1;
        }

        if(!(StringUtils.isBlank(querytype)||StringUtils.isBlank(hasloc))){
            if(!hasloc.equals("on")){
                hasloc = null;
            }
            if(!StringUtils.isBlank(keyword)){
                try {
                    keyword = new String(keyword.trim().getBytes("ISO-8859-1"), "utf8");
                } catch (UnsupportedEncodingException e) {
                    logger.error("UnsupportedEncodingException",e);
                }
            }

            list = activityService.simpleQuery(keyword,querytype, hasloc, (currentPage - 1) * PAGE, PAGE);
        }else{
            list = activityService.selectList(null,(currentPage-1)*PAGE,PAGE);
        }

        map.put("currentPage",currentPage);
        map.put("keyword",keyword);
        map.put("querytype",querytype);
        map.put("list",list);

        return "activityindex";
    }

    @RequestMapping(value = "/addacpage")
     public String addActivityPage(){
        return "addacpage";
    }

    @RequestMapping(value = "/activitydetail")
    public String activityDetail(Integer id,ModelMap map){
        Activity activity = new Activity();
        activity.setId(id);
        Activity result = activityService.select(activity);
        List<Images> imglist = null;
        if(result==null){
            return "activitydetail";
        }else {
            Images where = new Images();
            where.setLinks_id(result.getId());
            where.setCatagory(result.getActivitytype());
            imglist = imagesService.selectList(where,null,null);

        }
        ActivityComment ac = new ActivityComment();
        ac.setActivity_id(id);
        List<ActivityComment> comments = activityCommentService.selectList(ac,0,200);
        map.put("activity",result);
        map.put("images",imglist);
        map.put("comment",comments);
        return "activitydetail";
    }

    @RequestMapping(value = "/activityedit")
    public String activityEdit(Integer id,ModelMap map){
        Activity activity = new Activity();
        activity.setId(id);
        Activity result = activityService.select(activity);
        List<Images> imglist = null;
        if(result==null){
            return "activityedit";
        }else {
            Images where = new Images();
            where.setLinks_id(result.getId());
            where.setCatagory(result.getActivitytype());
            imglist = imagesService.selectList(where,null,null);

        }
        map.put("activity",result);
        map.put("images",imglist);
        return "activityedit";
    }

    @RequestMapping(value = "/addactivity")
    public String addactivity(Activity activity,HttpSession session){

        String sessionid = session.getId();
        String ipaddr = (String)session.getAttribute("ip");
        User item = new User();
        item.setSessionid(sessionid);
        item.setIpaddr(ipaddr);
        User user = userService.select(item);

        logger.info("activity extra {}",activity.getExtra());
        if(!StringUtils.isEmpty(activity.getExtra())){
            String extra = activity.getExtra();
            extra = extra.substring(0,extra.length()-1);
            activity.setExtra(extra);
        }

        if(user==null){
            User u = new User("","",sessionid,ipaddr);
            userService.insert(u);
            activity.setUserid(u.getId());
        }else{
            activity.setUserid(user.getId());
        }

        logger.info("activity {}",activity.getActivityname());
        activityService.insert(activity);

        String extra = activity.getExtra();
        if(!StringUtils.isEmpty(extra)){
            String[] extraarray = extra.split(",");
            for(int i=0;i<extraarray.length;i++){
                try{
                    Integer imgid = Integer.parseInt(extraarray[i]);
                    Images where = new Images();
                    where.setId(imgid);
                    Images update = new Images();
                    update.setLinks_id(activity.getId());
                    imagesService.update(update,where);
                }catch (Exception e){
                    logger.error("addactivity error",e);
                }


            }
        }

        return "redirect:/activity/index";
    }

    @RequestMapping(value = "/addcomment")
    @ResponseBody
    public String comment(String activityId,String comment,HttpSession session){
        if(StringUtils.isBlank(comment)){
            return "";
        }else{
            try{
                logger.info("add activity comment {}",comment);
                String sessionid = session.getId();
                User param = new User();
                param.setSessionid(sessionid);
                User user = userService.select(param);
                activityCommentService.insert(new ActivityComment(Integer.parseInt(activityId),comment,user.getId()));
            }catch(Exception e){
                logger.error("add comment error",e);
            }
        }
        return "success";
    }


}
