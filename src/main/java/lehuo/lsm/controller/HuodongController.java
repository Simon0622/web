package lehuo.lsm.controller;

import lehuo.lsm.dao.HuodongCommentDao;
import lehuo.lsm.dao.WeChatUserDao;
import lehuo.lsm.model.WechatModel.db.Huodong;
import lehuo.lsm.model.WechatModel.db.HuodongComment;
import lehuo.lsm.model.WechatModel.db.WeChatUser;
import lehuo.lsm.service.impl.*;
import lehuo.lsm.util.ATTimerTask;
import lehuo.lsm.util.EncoderHandler;
import lehuo.lsm.util.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/huodong")
public class HuodongController {

    protected static Logger logger = LoggerFactory.getLogger(HuodongController.class);

    private static final int PAGE = 10;

    @Resource
    private HuodongService huodongService;

    @Resource
    private HuodongCommentDao huodongCommentDao;

    @Resource
    private WeChatUserDao weChatUserDao;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping(value = "/userhdindex")
    public String index(ModelMap map,String fromuserid){
        logger.info("huodong user index fromid {}",fromuserid);
        Huodong hd = new Huodong(fromuserid);
        //hd.setCreatetime(sdf.format(new Date()));
        List<Huodong> list = huodongService.selectHuodongList(hd,0,10);
        map.put("list",list);
        return "huodongindex";
    }

    @RequestMapping(value = "/comment")
    @ResponseBody
    public String comment(HuodongComment huodongComment){

        huodongComment.setContent(huodongComment.getContent().trim());
        huodongCommentDao.insert(huodongComment);
        WeChatUser weChatUser = weChatUserDao.select(new WeChatUser(huodongComment.getFromid()));
        String result = HttpUtils.sendPost("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + ATTimerTask.accessToken,
                "{\n" +
                        "    \"touser\":\""+huodongComment.getToid()+"\",\n" +
                        "    \"msgtype\":\"text\",\n" +
                        "    \"text\":\n" +
                        "    {\n" +
                        "         \"content\":\""+weChatUser.getNickname()+"给你留言："+huodongComment.getContent()+" (回复10"+huodongComment.getId()+" 空格 你要回复的内容,可直接向他发消息) " +
                        "    \"}\n" +
                        "}");

        logger.info("comment success result {}",result);
        return "success";
    }

    @RequestMapping(value = "/huodongdetail")
    public String huodongDetail(Integer id,String viewid,ModelMap map){

        logger.info("huodong id {},viewid {}",id,viewid);

        Huodong huodong = huodongService.selectUserHuodong(new Huodong(id));
        map.put("hd",huodong);

        List<HuodongComment> comments = null;
        String fromid = huodong.getUserfromid();
        HuodongComment item = new HuodongComment();
        item.setPageId(id);
        if(!fromid.equals(viewid)){
            item.setFromid(viewid);
            item.setToid(fromid);
            List<HuodongComment> tmp = huodongCommentDao.selectCommentandInfo(item);
            item.setFromid(fromid);
            item.setToid(viewid);
            comments = huodongCommentDao.selectCommentandInfo(item);
            comments.addAll(tmp);
        }else{
            item.setToid(fromid);
            comments = huodongCommentDao.selectCommentandInfo(item);
        }

        map.put("comments",comments);
        map.put("viewid",viewid);

        return "huodongdetail";
    }

    @RequestMapping(value = "/huodongedit")
    public String huodongEdit(Integer id,ModelMap map){

        Huodong huodong = huodongService.selectUserHuodong(new Huodong(id));
        map.put("hd",huodong);

        List<HuodongComment> comments = null;
        String fromid = huodong.getUserfromid();
        HuodongComment item = new HuodongComment();
        item.setToid(fromid);
        item.setPageId(id);
        comments = huodongCommentDao.selectCommentandInfo(item);
        map.put("comments",comments);

        String jsapi_ticket = ATTimerTask.jsapiTicket;
        StringBuffer sb = new StringBuffer();
        sb.append("jsapi_ticket=").append(jsapi_ticket)
                .append("&noncestr=").append(WechatController.nonceStr)
                .append("&timestamp").append(WechatController.timestamp)
                .append("&url=http://www.ygkbnu.com/xiaoxianrou/huodong/huodongedit?id="+id);
        String signature = EncoderHandler.encode("SHA1", sb.toString());
        map.put("appId", "wx75f426630ab9a4fd");
        map.put("timestamp",WechatController.timestamp);
        map.put("nonceStr",WechatController.nonceStr);
        map.put("signature",signature);

        return "huodongedit";
    }

    @RequestMapping(value = "/addhuodong")
    public String addHuodong(Huodong huodong){
        try{
            huodongService.update(huodong,huodong);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }

        return "redirect:/huodong/huodongdetail?id="+huodong.getId()+"&viewid="+huodong.getUserfromid();
    }

}