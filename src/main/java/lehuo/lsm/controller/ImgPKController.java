package lehuo.lsm.controller;

import com.alibaba.fastjson.JSON;
import lehuo.lsm.global.Global;
import lehuo.lsm.model.*;
import lehuo.lsm.service.impl.*;
import lehuo.lsm.util.ImgDownload;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FileUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/imgpk")
public class ImgPKController {

    protected static Logger logger = LoggerFactory.getLogger(RankController.class);

    @Resource
    private ImgPKService imgPKService;

    @Resource
    private LinksService linksService;

    @Resource
    private UserService userService;

    @Resource
    private ImagesService imagesService;

    @Resource
    private DoubanProcesser doubanProcesser;

    private static final int oneBatchNum = 10;

    @RequestMapping(value = "/index")
    public String oneRound(ModelMap map, HttpSession session) {
        User user = null;
        if (Global.getLINK_MAX() == 0) {
            Global.setLINK_MAX(linksService.selectMax());
        }

        String name = (String) session.getAttribute("name");
        if (StringUtils.isBlank(name)) {
            String sessionid = session.getId();
            String ip = (String) session.getAttribute("ip");
            User item = new User();
            item.setSessionid(sessionid);
            user = userService.select(item);
            if(user!=null){
                map.put("age", user.getAge());
                map.put("birth", user.getBirth());
            }else{
                userService.insert(new User("", "", sessionid, ip));
            }

        } else {
            User item = new User();
            item.setUsername(name);
            user = userService.select(item);

            map.put("age", user.getAge());
            map.put("birth", user.getBirth());


        }


        List<Images> list = imgPKService.selectLownumPkImg(0, oneBatchNum * 2);

        List<Images> left = list.subList(0, 10);
        List<Images> right = list.subList(10, list.size());

        map.put("left", left);
        map.put("right", right);

        return "pk";
    }

    @RequestMapping(value = "/oneround")
    public void oneRound(String data, HttpSession session) {

        String sessionid = session.getId();
        User item = new User();
        item.setSessionid(sessionid);
        User user = userService.select(item);
        try {

            List<PkImg> list = new ArrayList<PkImg>();
            String[] rounds = data.split(":");
            for (String r : rounds) {
                if (StringUtils.isBlank(r)) {
                    continue;
                }
                String[] pkvo = r.split(",");
                if (StringUtils.isNumeric(pkvo[0]) && StringUtils.isNumeric(pkvo[1])) {
                    PkImg vo = new PkImg(Integer.parseInt(pkvo[0]), Integer.parseInt(pkvo[1]), user.getId(), 0);
                    list.add(vo);
                }
            }

            imgPKService.insertPKSubmit(session.getId(), list);


        } catch (Exception e) {
            logger.error("oneRound function error", e);
        }
    }

    @RequestMapping(value = "/change")
    public void change(String win, String lose, HttpSession session) {

        String sessionid = session.getId();
        User item = new User();
        item.setSessionid(sessionid);
        User user = userService.select(item);
        if (user == null) {
            userService.insert(item);
        }
        try {
            PkImg vo = null;

            if (StringUtils.isNumeric(win) && StringUtils.isNumeric(lose)) {
                vo = new PkImg(Integer.parseInt(win), Integer.parseInt(lose), user.getId(), 0);
            }


            imgPKService.insert(vo);
            imagesService.updateImgWin(Integer.parseInt(win));
            imagesService.updateImgLose(Integer.parseInt(lose));

        } catch (Exception e) {
            logger.error("change function error", e);
        }
    }

    @RequestMapping(value = "/nextbatch")
    @ResponseBody
    public String nextBatch(String offset) {
        if (!StringUtils.isNumeric(offset)) {
            offset = "0";
        }

        List<Images> newround = imgPKService.selectLownumPkImg(Integer.parseInt(offset), oneBatchNum);

        return JSON.toJSONString(newround);
    }

    @RequestMapping(value = "/agebirth")
    @ResponseBody
    public String agebirth(String age, String birth, String province, HttpSession session) {
        try {
            Integer ageint = Integer.parseInt(age);
            Integer birthint = Integer.parseInt(birth);

            session.setAttribute("age", ageint);
            session.setAttribute("birth", birthint);
            if (StringUtils.isBlank(province)) {
                province = "";
            }
            User update = new User();
            update.setAge(ageint);
            update.setBirth(birthint);
            update.setProvince(province);
            User item = new User();
            item.setSessionid(session.getId());
            userService.update(update, item);
            return "true";
        } catch (Exception e) {
            return "false";
        }

    }

    @RequestMapping(value = "/ipk")
    public String ipk() {
        return "ipk";
    }

    @RequestMapping(value = "/addpkimg")
    @ResponseBody
    public String addpkimg(@RequestParam MultipartFile myfile, HttpSession session, String catagory) {
        Images image = null;

        logger.info("session id is {}", session.getId());
        if (myfile.isEmpty()) {
            System.out.println("文件未上传");
        } else {
            System.out.println("文件长度: " + myfile.getSize());
            System.out.println("文件类型: " + myfile.getContentType());
            System.out.println("文件名称: " + myfile.getName());
            System.out.println("文件原名: " + myfile.getOriginalFilename());
            System.out.println("========================================");
            //如果用的是Tomcat服务器，则文件会上传到\\%TOMCAT_HOME%\\webapps\\YourWebProject\\WEB-INF\\upload\\文件夹中
            //String realPath = request.getSession().getServletContext().getRealPath("/WEB-INF/upload");
            //这里不必处理IO流关闭的问题，因为FileUtils.copyInputStreamToFile()方法内部会自动把用到的IO流关掉，我是看它的源码才知道的

            try {
                InputStream inputStream = myfile.getInputStream();
                byte[] btImg = ImgDownload.readInputStream(inputStream);
                image = new Images(0, session.getId() + myfile.getOriginalFilename(), 1, myfile.getOriginalFilename());
                User item = new User();
                item.setSessionid(session.getId());
                User u = userService.select(item);
                if (u == null) {
                    userService.insert(item);
                    u = userService.select(item);
                }
                if (StringUtils.isEmpty(catagory)) {
                    catagory = "1";
                }

                Integer icatagory = Integer.parseInt(catagory);
                image.setCatagory(icatagory);
                image.setDeal(1);
                imagesService.insert(image);
                logger.info("image id is {}", image.getId());
                ImgDownload.writeImageToDisk(btImg, image.getId().toString() + ".jpg");

                Images update = new Images();
                update.setUserid(u.getId());

                if (icatagory == 1) {
                    update.setImgshow(1);
                } else if (icatagory > 10) {
                    update.setImgshow(0);
                }

                Images where = new Images();
                where.setId(image.getId());
                imagesService.update(update, where);
            } catch (Exception e) {
                logger.error("addpkimg", e);
                return null;
            }

        }

        return JSON.toJSONString(image.getId().toString());
    }


}
