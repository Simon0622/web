package lehuo.lsm.controller;

import lehuo.lsm.global.Global;
import lehuo.lsm.model.*;
import lehuo.lsm.service.impl.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/rank")
public class RankController {

    protected static Logger logger = LoggerFactory.getLogger(RankController.class);

    @Resource
    ImagesService imagesService;

    @Resource
    LinksService linksService;

    @Resource
    CommentService commentService;

    @Resource
    ImgScoreService imgScoreService;

    @Resource
    UserService userService;

    private Integer pageSize = 20;

    @RequestMapping(value = "/rankList")
    public String rankList(Integer offset,ModelMap map,Integer rank,
                           String age,String birth,String province){
        if(offset==null){
            offset = 1;
        }

        User item = new User();
        try{
            Integer iage = Integer.parseInt(age);
            Integer ibirth = Integer.parseInt(birth);
            if(iage!=0){
                item.setAge(iage);
            }
            if(ibirth!=0){
                item.setBirth(ibirth);
            }

            item.setProvince(province);
        }catch (Exception e){
            logger.error("rankList param error",e);
        }

        Integer currentPage = offset;
        List<Images> list=null;
        if(rank==null||(rank!=null&&rank.equals(0))){
            list = imagesService.selectRanklist((currentPage-1)*pageSize, pageSize,null);
        }else if(rank.equals(1)){
            //胜率排行
            list = imagesService.rankbypk(item,(currentPage-1)*pageSize,pageSize);
        }else if(rank.equals(2)){
            list = imagesService.rankbyscore(item,(currentPage-1)*pageSize,pageSize);
        }else if(rank.equals(3)){
            list = imagesService.rankbyvisitnum((currentPage-1)*pageSize,pageSize);
        }


        map.put("list", list);
        map.put("currentPage", currentPage);
        return "ranklist";
    }

    @RequestMapping(value = "/userImgs")
    public String userImgs(Integer offset,ModelMap map,Integer rank){
        if(offset==null){
            offset = 1;
        }


        Integer currentPage = offset;
        List<Images> list=null;
        if(rank!=null){
            if(rank.equals(0)){
                list = imagesService.userrankbysome(null,(currentPage-1)*pageSize, pageSize);
            }else{
                list = imagesService.userrankbysome(rank,(currentPage-1)*pageSize, pageSize);
            }
        }else{
            list = imagesService.userrankbysome(null,(currentPage-1)*pageSize, pageSize);
        }

        map.put("list", list);
        map.put("currentPage", currentPage);
        return "userranklist";
    }


    @RequestMapping(value = "/detail")
    public String detail(String id,ModelMap map){
        Links link = linksService.select(new Links(Integer.parseInt(id)));

        linksService.updateClick(Integer.parseInt(id));

        List<Links> images= linksService.selectDetail(Integer.parseInt(id));

        List<Comment> comments = commentService.selectList(new Comment(Integer.parseInt(id)),null,null);

        map.put("link",link);
        map.put("images",images);
        map.put("comment",comments);
        return "detail";
    }

    @RequestMapping(value = "/score")
    @ResponseBody
    public String getScore(String data,String score,HttpSession session){
        try{
            String sessionid = session.getId();
            User param = new User();
            param.setSessionid(sessionid);
            User user = userService.select(param);

            String idStr = data.replace("rating:", "");
            Integer id = Integer.parseInt(idStr);
            Integer sc = Integer.parseInt(score);

            ImgScore imgScore = new ImgScore(id,sc,user.getId());

            imgScoreService.insert(imgScore);
            imagesService.updateImgStore(id,sc);
        }catch (Exception e){
            logger.error("getScore",e);
        }


        return "success";
    }

    @RequestMapping(value = "/comment")
    @ResponseBody
    public String comment(String linkId,String comment,HttpSession session){
        if(StringUtils.isBlank(comment)){
            return "";
        }else{
            try{
                logger.info("add comment {}",comment);
                String sessionid = session.getId();
                User param = new User();
                param.setSessionid(sessionid);
                User user = userService.select(param);
                commentService.insert(new Comment(Integer.parseInt(linkId),comment,user.getId()));
            }catch(Exception e){
                logger.error("add comment error",e);
            }
        }
        return "success";
    }



    private List<String> parseImgUrl(String url){
        List<String> images = new ArrayList<String>();
        try{
            url = url.substring(1,url.length()-1);
        }catch (Exception e){

        }

        if(!StringUtils.isBlank(url)){
            String[] surl = url.split("\\, ");
            for(String u:surl){
                images.add(u);
            }
        }

        return images;
    }
}
