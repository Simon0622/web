package lehuo.lsm.controller;

import lehuo.lsm.dao.CaoliuDao;
import lehuo.lsm.model.CaoliuVo;
import lehuo.lsm.model.Images;
import lehuo.lsm.model.SearchWord;
import lehuo.lsm.model.User;
import lehuo.lsm.service.impl.ImagesService;
import lehuo.lsm.service.impl.SearchWordService;
import lehuo.lsm.service.impl.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/ucenter")
public class UCenterController {

    protected static Logger logger = LoggerFactory.getLogger(UCenterController.class);

    @Resource
    ImagesService imagesService;
    @Resource
    UserService userService;
    @Resource
    CaoliuDao caoliuDao;
    @Resource
    private SearchWordService searchWordService;

    @RequestMapping(value = "/index")
    public String UCenterIndex(HttpServletRequest request,ModelMap map){
        HttpSession session = request.getSession();
        String username = (String)session.getAttribute("name");
        List<Images> userimgs;
        if(StringUtils.isBlank(username)){
            return "login";
        }else{
            User item = new User();
            item.setUsername(username);
            User user = userService.select(item);
            Images i = new Images();
            i.setUserid(user.getId());
            userimgs = imagesService.selectList(i,null,null);
        }

        map.put("userimgs",userimgs);
        return "userindex";

    }

    @RequestMapping(value = "/statistics")
    public String statistics(Integer imgid){

        return "statistics";
    }

    @RequestMapping(value="clsearch")
    public String clSearch(String words,ModelMap map,String username){
        List<CaoliuVo> list = new ArrayList<CaoliuVo>();
        String[] result;
        SearchWord sw = new SearchWord();

        if(StringUtils.isBlank(words)){
            map.put("list",list);
            return "caoliuresult";
        }else{
            if(StringUtils.isBlank(username)){
                sw.setUserid(0);

            }else{
                User user = userService.select(new User(username));
                sw.setUserid(user.getId());
            }
            sw.setWord(words);
            searchWordService.insert(sw);

            result = words.split(" ");
        }
        for(String word:result){
            List<CaoliuVo> l = caoliuDao.keySearch(word);
            list.addAll(l);
        }
        map.put("list",list);
        return "caoliuresult";
    }



}
