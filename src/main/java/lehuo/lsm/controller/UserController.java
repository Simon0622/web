package lehuo.lsm.controller;

import lehuo.lsm.model.User;
import lehuo.lsm.service.impl.SearchWordService;
import lehuo.lsm.service.impl.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.logging.Logger;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Resource
    private UserService userService;



    @RequestMapping(value = "/userlogin")
    public String userlogin(User user,HttpSession session){
        //TO-DO
        User item = new User();
        item.setUsername(user.getUsername());
        User result = userService.select(item);
        if(result==null){
            return "redirect:/user/login";
        }
        if(result.getPassword().equals(user.getPassword())){
            session.setAttribute("name",user.getUsername());
            return "redirect:/ucenter/index";
        }else{
            return "redirect:/user/login";
        }

    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/userregister")
    public String userregister(String username,String password,HttpSession session){
        User item = new User();
        item.setSessionid(session.getId());
        User update = new User();
        update.setUsername(username);
        update.setPassword(password);
        userService.update(update,item);
        session.setAttribute("name",username);
        return "redirect:/ucenter/index";
    }

    @RequestMapping(value = "/register")
    public String register() {
        return "register";
    }

}
