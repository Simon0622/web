package lehuo.lsm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.primitives.Doubles;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import lehuo.lsm.dao.BzHuodongDao;
import lehuo.lsm.dao.HuodongCommentDao;
import lehuo.lsm.dao.HuodongDao;
import lehuo.lsm.dao.SearchWordDao;
import lehuo.lsm.dao.WeChatCommentDao;
import lehuo.lsm.dao.WeChatImageDao;
import lehuo.lsm.dao.WeChatUserDao;
import lehuo.lsm.dao.WeChatUserfromDao;
import lehuo.lsm.dao.ZhenghunDao;
import lehuo.lsm.model.SearchWord;
import lehuo.lsm.model.WechatModel.Article;
import lehuo.lsm.model.WechatModel.ImgTextMsgVo;
import lehuo.lsm.model.WechatModel.ImgVoiceMsgVo;
import lehuo.lsm.model.WechatModel.TextMsgVo;
import lehuo.lsm.model.WechatModel.db.BzHuodong;
import lehuo.lsm.model.WechatModel.db.Huodong;
import lehuo.lsm.model.WechatModel.db.HuodongComment;
import lehuo.lsm.model.WechatModel.db.WeChatComment;
import lehuo.lsm.model.WechatModel.db.WeChatImage;
import lehuo.lsm.model.WechatModel.db.WeChatUser;
import lehuo.lsm.model.WechatModel.db.WeChatUserfrom;
import lehuo.lsm.model.WechatModel.db.WexinComment;
import lehuo.lsm.model.WechatModel.db.Zhenghun;
import lehuo.lsm.util.ATTimerTask;
import lehuo.lsm.util.EncoderHandler;
import lehuo.lsm.util.HttpUtils;

/**
 * Created by simon on 14/12/20.
 */
@Controller
@RequestMapping(value = "/wechat")
public class WechatController
{

  protected static Logger logger = LoggerFactory.getLogger(WechatController.class);
  private static String token = "AfUG9fSG";

  private static final String weburl = "www.ygkbnu.com/xiaoxianrou/";

  private static final String appid = "wx75f426630ab9a4fd";
  private static final String secret = "81668fc80beb4691b2e6bf80d8459999";

  public static volatile String timestamp;
  public static volatile String nonceStr;
  public static volatile String signature;

  private Map<String, String> eventMap = new HashMap<String, String>();

  private Map<String, String> locationMap = new HashMap<String, String>();

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

  private static final Pattern huodongpattern = Pattern.compile("^10[1-9][0-9]*");
  private static final Pattern gerenpattern = Pattern.compile("^20[1-9][0-9]*");
  private static final Pattern zhenghunpattern = Pattern.compile("^[gGmM]{2}[0-9]{1,4}$");

  Timer timer;

  @Resource
  WeChatUserDao weChatUserDao;

  @Resource
  WeChatImageDao weChatImageDao;

  @Resource
  HuodongDao huodongDao;
  @Resource
  HuodongCommentDao huodongCommentDao;
  @Resource
  private WeChatCommentDao weChatCommentDao;
  @Resource
  WeChatUserfromDao weChatUserfromDao;
  @Resource
  BzHuodongDao bzHuodongDao;
  @Resource
  SearchWordDao searchWordDao;
  @Resource
  ZhenghunDao zhenghunDao;

  public WechatController()
  {
    timer = new Timer();
    timer.schedule(new ATTimerTask(), 1 * 1000, 7200 * 1000);
  }

  @RequestMapping(value = "/callback")
  public String callback(String code, String state, String fromid)
  {
    logger.info("callback step into code {},state {},fromid {}", code, state, fromid);
    if (StringUtils.isBlank(code))
    {
      return "error";
    }
    else
    {
      String result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret
          + "&code=" + code + "&grant_type=authorization_code");
      logger.info("step one {}", result);
      JSONObject jsonObject = JSON.parseObject(result);
      String access_token = jsonObject.getString("access_token");
      String refresh_token = jsonObject.getString("refresh_token");
      String openid = jsonObject.getString("openid");

      // 延长access_token时间的代码，暂时不用延长，延长再打开即可
      result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + appid
          + "&grant_type=refresh_token&refresh_token=" + refresh_token);
      logger.info("step two {}", result);
      jsonObject = JSON.parseObject(result);
      access_token = jsonObject.getString("access_token");
      if (access_token != null)
      {
        result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid
            + "&lang=zh_CN");
        jsonObject = JSON.parseObject(result);
        logger.info("callback user is {}", jsonObject.toString());
        /*WeChatUser weChatUser = new WeChatUser(jsonObject.getString("openid"));
        weChatUser.setNickname(jsonObject.getString("nickname"));
        weChatUser.setSex(jsonObject.getString("sex"));
        weChatUser.setProvince(jsonObject.getString("province"));
        weChatUser.setCity(jsonObject.getString("city"));
        weChatUser.setCountry(jsonObject.getString("country"));
        weChatUser.setHeadimgurl(jsonObject.getString("headimgurl"));*/
        // session.setAttribute(jsonObject.getString("openid"), weChatUser);

      }
      return "redirect:/wechatuser/detail?fromid=" + fromid + "&viewid=" + openid;
    }
  }

  @RequestMapping(value = "/ucenter")
  public String ucenter(String code, String state, String fromid)
  {
    logger.info("ucenter step into code {},state {},fromid {}", code, state, fromid);

    if (StringUtils.isBlank(code))
    {
      return "error";
    }
    else
    {
      String result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret
          + "&code=" + code + "&grant_type=authorization_code");
      logger.info("step one {}", result);
      JSONObject jsonObject = JSON.parseObject(result);
      String access_token = jsonObject.getString("access_token");
      String refresh_token = jsonObject.getString("refresh_token");
      String openid = jsonObject.getString("openid");

      // 延长access_token时间的代码，暂时不用延长，延长再打开即可
      result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + appid
          + "&grant_type=refresh_token&refresh_token=" + refresh_token);
      logger.info("step two {}", result);
      jsonObject = JSON.parseObject(result);
      access_token = jsonObject.getString("access_token");
      if (access_token != null)
      {
        result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid
            + "&lang=zh_CN");
        jsonObject = JSON.parseObject(result);
        logger.info("ucenter user is {}", jsonObject.toString());
        WeChatUser weChatUser = new WeChatUser(jsonObject.getString("openid"));
        weChatUser.setNickname(jsonObject.getString("nickname"));
        weChatUser.setSex(jsonObject.getString("sex"));
        weChatUser.setProvince(jsonObject.getString("province"));
        weChatUser.setCity(jsonObject.getString("city"));
        weChatUser.setCountry(jsonObject.getString("country"));
        weChatUser.setHeadimgurl(jsonObject.getString("headimgurl"));

      }
      return "redirect:/wechatuser/index?fromid=" + jsonObject.getString("openid");
    }
  }

  @RequestMapping(value = "/uactivity")
  public String uactivity(String code, String state, String fromid)
  {
    logger.info("uactivity step into code {},state {},fromid {}", code, state, fromid);
    if (StringUtils.isBlank(code))
    {
      return "error";
    }
    else
    {
      String result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + secret
          + "&code=" + code + "&grant_type=authorization_code");
      logger.info("step one {}", result);
      JSONObject jsonObject = JSON.parseObject(result);
      String access_token = jsonObject.getString("access_token");
      String refresh_token = jsonObject.getString("refresh_token");
      String openid = jsonObject.getString("openid");

      // 延长access_token时间的代码，暂时不用延长，延长再打开即可
      result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + appid
          + "&grant_type=refresh_token&refresh_token=" + refresh_token);
      logger.info("step two {}", result);
      jsonObject = JSON.parseObject(result);
      access_token = jsonObject.getString("access_token");
      if (access_token != null)
      {
        result = HttpUtils.httpsCon("https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid
            + "&lang=zh_CN");
        jsonObject = JSON.parseObject(result);
        logger.info("ucenter user is {}", jsonObject.toString());
        WeChatUser weChatUser = new WeChatUser(jsonObject.getString("openid"));
        weChatUser.setNickname(jsonObject.getString("nickname"));
        weChatUser.setSex(jsonObject.getString("sex"));
        weChatUser.setProvince(jsonObject.getString("province"));
        weChatUser.setCity(jsonObject.getString("city"));
        weChatUser.setCountry(jsonObject.getString("country"));
        weChatUser.setHeadimgurl(jsonObject.getString("headimgurl"));

        return "redirect:/huodong/userhdindex?fromuserid=" + weChatUser.getFromid();
      }
      else
      {
        return "";
      }

    }
  }

  @RequestMapping(value = "/index")
  public void index(String signature, String timestamp, String nonce, String echostr,
      @RequestBody String body, HttpServletResponse response)
  {
    response.setCharacterEncoding("UTF-8");
    List<String> list = new ArrayList<String>();
    logger.info("signature is {},timestamp is {},nonce is {},echostr is {}", signature, timestamp, nonce, echostr);

    WechatController.timestamp = timestamp;
    WechatController.nonceStr = nonce;
    WechatController.signature = signature;

    logger.info("request body is {}", body);
    list.add(timestamp);
    list.add(token);
    list.add(nonce);
    Collections.sort(list);
    StringBuffer sb = new StringBuffer();
    for (String l : list)
    {
      sb.append(l);
    }

    logger.info("jiami list is {}", sb.toString());
    String verify = EncoderHandler.encode("SHA1", sb.toString());
    logger.info("verify is {}", verify);

    Document doc = null;
    try
    {
      doc = DocumentHelper.parseText(body);
    }
    catch (DocumentException e)
    {
      logger.error("xml parse error", e);
    }

    Element rootElement = doc.getRootElement();
    String rep = dispatchMsg(rootElement);
    if (rep == null)
    {
      rep = "";
    }

    logger.info("response result {}", rep);

    if (verify.equals(signature))
    {
      logger.info("wechar verify success");
      try
      {
        PrintWriter out = response.getWriter();
        out.print(rep);
        out.close();
      }
      catch (IOException e)
      {
        logger.error("response error", e);
      }

    }
    else
    {
      logger.info("wechar verify failed");
      try
      {
        PrintWriter out = response.getWriter();
        out.print("");
        out.close();
      }
      catch (IOException e)
      {
        logger.error("response error", e);
      }
    }
  }

  private String dispatchMsg(Element rootElement)
  {
    if (rootElement == null)
    {
      return null;
    }
    else
    {
      Node MsgType = rootElement.element("MsgType");
      if (MsgType != null)
      {
        if (MsgType.getText().equals("text"))
        {
          logger.info("text msg");
          return handleTextMsg(rootElement);
        }
        else if (MsgType.getText().equals("image"))
        {
          logger.info("image msg");
          return handleImgMsg(rootElement);
        }
        else if (MsgType.getText().equals("voice"))
        {
          logger.info("voice msg");
          return handleVoiceMsg(rootElement);
        }
        else if (MsgType.getText().equals("video"))
        {
          logger.info("video msg");
          return handleVideoMsg(rootElement);
        }
        else if (MsgType.getText().equals("location"))
        {
          logger.info("location msg");
          return handleLocationMsg(rootElement);
        }
        else if (MsgType.getText().equals("link"))
        {
          logger.info("link msg");
          return handleLinkMsg(rootElement);
        }
        else if (MsgType.getText().equals("event"))
        {
          logger.info("event msg");
          return handleEventMsg(rootElement);
        }
        else
        {
          logger.info("other msg");
          return handleOtherMsg(rootElement);
        }
      }
      else
      {
        return null;
      }
    }

  }

  private String handleTextMsg(Element rootElement)
  {
    String content = rootElement.element("Content").getText();
    String fromUserName = rootElement.element("FromUserName").getText();
    String toUserName = rootElement.element("ToUserName").getText();
    String createTime = rootElement.element("CreateTime").getText();
    if (huodongpattern.matcher(content).find())
    {
      if (StringUtils.isBlank(fromUserName))
      {
        return null;
      }
      else
      {
        // 截取10
        String[] args = content.split(" ");
        String cId = args[0].substring(2, args[0].length());
        content.replaceAll(args[0], "");
        try
        {
          Integer commentId = Integer.parseInt(cId);
          HuodongComment huodongComment = huodongCommentDao.select(new HuodongComment(commentId));

          HuodongComment newComment = new HuodongComment(huodongComment.getToid());
          newComment.setToid(huodongComment.getFromid());
          newComment.setContent(content);
          newComment.setReplyId(huodongComment.getId());
          huodongCommentDao.insert(newComment);

          sendKFMsg(newComment, "10");
          TextMsgVo textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
          textMsgVo.setContent("回复成功");
          String result = sendTextMsg(textMsgVo);
          logger.info("handleTextMsg return {}", result);
          return result;
        }
        catch (Exception e)
        {
          logger.error(e.getMessage(), e);
          return "";
        }

      }

    }
    else if (gerenpattern.matcher(content).find())
    {
      String[] args = content.split(" ");
      if (StringUtils.isBlank(fromUserName))
      {
        return null;
      }
      else
      {
        // 截取20
        String cId = args[0].substring(2, args[0].length());
        content.replaceAll(args[0], "");
        WeChatComment weChatComment = weChatCommentDao.select(new WeChatComment(Integer.parseInt(cId)));

        WeChatComment newComment = new WeChatComment(weChatComment.getToid());
        newComment.setContent(content);
        newComment.setToid(weChatComment.getFromid());
        newComment.setReplyId(weChatComment.getId());

        weChatCommentDao.insert(newComment);

        sendKFMsg(newComment, "20");
        TextMsgVo textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
        textMsgVo.setContent("回复成功");
        String result = sendTextMsg(textMsgVo);
        logger.info("handleTextMsg return {}", result);
        return result;
      }
    }
    else if (zhenghunpattern.matcher(content).find())
    {
      if (StringUtils.isBlank(fromUserName))
      {
        return null;
      }
      else
      {
        String sex = content.substring(0, 2);
        String num = content.substring(2);
        StringBuffer sb = new StringBuffer();

        if (sex.equalsIgnoreCase("GG"))
        {

          sb.append("GG").append(num);
          Zhenghun zhengh = zhenghunDao.selectGivenNum(sb.toString());
          ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
          List<Article> articles = new ArrayList<Article>();

          Article article = new Article(zhengh.getTitle(), "详情请点击查看", zhengh.getImgurl(), zhengh.getUrl());
          articles.add(article);
          imgTextMsgVo.setList(articles);
          imgTextMsgVo.setMsgType("news");
          String result = sendTWMsg(imgTextMsgVo);
          return result;

        }
        else if (sex.equalsIgnoreCase("MM"))
        {

          sb.append("MM").append(num);
          Zhenghun zhengh = zhenghunDao.selectGivenNum(sb.toString());
          ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
          List<Article> articles = new ArrayList<Article>();

          Article article = new Article(zhengh.getTitle(), "详情请点击查看", zhengh.getImgurl(), zhengh.getUrl());
          articles.add(article);
          imgTextMsgVo.setList(articles);
          imgTextMsgVo.setMsgType("news");
          String result = sendTWMsg(imgTextMsgVo);
          return result;

        }
        else
        {
          return "";
        }
      }
    }
    else if (content.startsWith("官方活动"))
    {
      String args[] = content.split(" ");
      WeChatUser wu = weChatUserDao.select(new WeChatUser(fromUserName));
      searchWordDao.insert(new SearchWord(wu.getId(), args[1]));
      TextMsgVo textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
      textMsgVo.setContent("报名成功，稍后会有客服把您拉到这周的活动群");
      String result = sendTextMsg(textMsgVo);
      logger.info("handleTextMsg return {}", result);
      return result;
    }
    else
    {
      return "";
    }
  }

  private String handleImgMsg(Element rootElement)
  {
    String fromUserName = rootElement.element("FromUserName").getText();
    String toUserName = rootElement.element("ToUserName").getText();
    String createTime = rootElement.element("CreateTime").getText();
    String result = "";

    String evententry = eventMap.get(fromUserName);
    logger.info("get key {},value is {}", fromUserName, evententry);
    if ("V1002_CREATEDATE".equals(evententry))
    {
      // 移除key

      eventMap.remove(fromUserName);
      logger.info("eventMap size {}", eventMap.size());

      String picUrl = rootElement.element("PicUrl").getText();

      Date today = new Date();
      String t = sdf.format(today);

      Huodong update = new Huodong();
      update.setHdewm(picUrl);
      Huodong huodong = huodongDao.selectTodayLast(t, fromUserName);
      huodongDao.update(update, new Huodong(fromUserName));

      TextMsgVo textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
      textMsgVo.setContent("上传二维码成功");
      result = sendTextMsg(textMsgVo);
      return result;

    }
    else
    {

      String picUrl = rootElement.element("PicUrl").getText();
      String msgId = rootElement.element("MsgId").getText();
      String mediaId = rootElement.element("MediaId").getText();
      WeChatImage weChatImage = new WeChatImage(fromUserName);
      weChatImage.setImgurl(picUrl);
      weChatImage.setMsgid(msgId);
      weChatImage.setMediaid(mediaId);
      weChatImageDao.insert(weChatImage);

      logger.info("handleImgMsg insert img success");
      ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
      List<Article> list = new ArrayList<Article>();
      Article article = new Article("上传成功", "点击查看", weChatImage.getImgurl(), "www.ygkbnu.com/xiaoxianrou/wechatuser/edit?fromid="
          + fromUserName);
      list.add(article);
      imgTextMsgVo.setList(list);
      imgTextMsgVo.setMsgType("news");
      result = sendTWMsg(imgTextMsgVo);
      return result;
    }

  }

  private String handleVoiceMsg(Element rootElement)
  {
    return null;
  }

  private String handleVideoMsg(Element rootElement)
  {
    return null;
  }

  private String handleLocationMsg(Element rootElement)
  {

    String fromUserName = rootElement.element("FromUserName").getText();
    String toUserName = rootElement.element("ToUserName").getText();
    String createTime = rootElement.element("CreateTime").getText();
    String latitude = rootElement.element("Location_X").getText();
    String longitude = rootElement.element("Location_Y").getText();
    String address = rootElement.element("Label").getText();
    String result = "";

    TextMsgVo textMsgVo = null;
    WeChatUser weChatUser = weChatUserDao.selectRecommondUser(new WeChatUser(fromUserName));
    if (weChatUser == null)
    {
      textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
      textMsgVo.setContent("请您先选择会员-上传形象照");
      result = sendTextMsg(textMsgVo);
      logger.info("handleTextMsg return {}", result);
      return result;

    }

    Huodong huodong = new Huodong();
    huodong.setHdname("修改名称");
    huodong.setLongitude(Double.parseDouble(longitude));
    huodong.setLatitude(Double.parseDouble(latitude));
    huodong.setUserfromid(fromUserName);
    huodong.setAddress(address);
    huodongDao.insert(huodong);
    ImgTextMsgVo vo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
    List<Article> list = new ArrayList<Article>();
    Article a = new Article("完善活动内容", "新建活动", weChatUser.getHeadimgurl(), weburl + "huodong/huodongedit?id=" + huodong.getId());
    list.add(a);
    vo.setList(list);
    vo.setMsgType("news");
    result = sendTWMsg(vo);
    logger.info("handleLocationMsg return {}", result);
    return result;
  }

  private String handleLinkMsg(Element rootElement)
  {
    return null;
  }

  private String handleEventMsg(Element rootElement)
  {
    Node event = rootElement.element("Event");
    logger.info("handleEventMsg {}", event.getText());
    String fromUserName = rootElement.element("FromUserName").getText();
    String toUserName = rootElement.element("ToUserName").getText();
    String createTime = rootElement.element("CreateTime").getText();
    String result = "";
    Node eventKey = rootElement.element("EventKey");
    if (event.getText().equals("CLICK"))
    {

      if (eventKey.getText().equals("V1001_UCENTER"))
      {
        ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
        WeChatUser weChatUser = weChatUserDao.select(new WeChatUser(fromUserName));
        List<Article> list = new ArrayList<Article>();
        Article article = new Article(
            "个人中心",
            "点击查看",
            weChatUser.getHeadimgurl(),
            "https://open.weixin.qq.com/connect/oauth2/authorize?appid="
                + appid
                + "&redirect_uri=http://www.ygkbnu.com/xiaoxianrou/wechat/ucenter&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
        list.add(article);
        imgTextMsgVo.setList(list);
        imgTextMsgVo.setMsgType("news");
        result = sendTWMsg(imgTextMsgVo);
        return result;
      }
      else if (eventKey.getText().equals("V1001_RECOMMEND"))
      {
        // todo 先是随机推荐吧，以后要慢慢改
        TextMsgVo textMsgVo = null;
        WeChatUser weChatUser = weChatUserDao.selectRecommondUser(new WeChatUser(fromUserName));
        if (weChatUser == null)
        {
          textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
          textMsgVo.setContent("请您先选择会员-上传形象照");
          result = sendTextMsg(textMsgVo);
          logger.info("handleTextMsg return {}", result);
          return result;

        }
        else if (weChatUser.getSex() == null || weChatUser.getSex().equals("0"))
        {
          textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
          textMsgVo.setContent("请输入您的性别,请在个人中心完善您的资料");
          result = sendTextMsg(textMsgVo);
          logger.info("handleTextMsg return {}", result);
          return result;
        }
        else if (StringUtils.isBlank(weChatUser.getProvince()))
        {
          textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
          textMsgVo.setContent("获得不到您所在的城市，请在个人中心完善");
          result = sendTextMsg(textMsgVo);
          logger.info("handleTextMsg return {}", result);
          return result;
        }
        else
        {

          WeChatUser item = new WeChatUser();

          // 设置查询异性
          if (weChatUser.getSex().equals("1"))
          {
            item.setSex("2");
          }
          else
          {
            item.setSex("1");
          }

          item.setProvince(weChatUser.getProvince());

          item.setFromid(fromUserName);

          List<WeChatUser> weChatUserList = weChatUserDao.selectListnoSelf(item);
          if (weChatUserList == null || weChatUserList.size() == 0)
          {
            textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
            textMsgVo.setContent("暂时没有和您所在同一个城市的异性，您可以邀请你的好友关注我们");
            result = sendTextMsg(textMsgVo);
            logger.info("handleTextMsg return {}", result);
            return result;
          }
          else
          {
            ImgTextMsgVo vo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
            List<Article> list = new ArrayList<Article>();
            for (WeChatUser weChatUser1 : weChatUserList)
            {
              Article a = new Article(weChatUser1.getNickname(), weChatUser1.getCareer(), weChatUser1.getWeChatImageList().get(0)
                  .getImgurl(),
                  "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appid
                      + "&redirect_uri=http://www.ygkbnu.com/xiaoxianrou/wechat/callback?fromid=" + weChatUser1.getFromid()
                      + "&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect");
              list.add(a);
              weChatUserDao.updaterecnum(weChatUser1.getFromid());
            }
            vo.setList(list);
            vo.setMsgType("news");
            result = sendTWMsg(vo);
            logger.info("handleEventMsg V1001_RECOMMEND return {}", result);
          }

        }

      }
      else if (eventKey.getText().equals("V1002_WEEKACTIVITY"))
      {

        BzHuodong bzHuodong = bzHuodongDao.select(null);
        ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
        List<Article> articles = new ArrayList<Article>();
        Article article = new Article("本周活动", "点击查看", bzHuodong.getImgurl(), bzHuodong.getUrl());
        articles.add(article);
        imgTextMsgVo.setList(articles);
        imgTextMsgVo.setMsgType("news");
        result = sendTWMsg(imgTextMsgVo);
        return result;

      }
      else if (eventKey.getText().equals("V1002_MYDATE"))
      {
        Date today = new Date();
        String t = sdf.format(today);
        Huodong item = new Huodong(fromUserName);
        item.setCreatetime(t);
        List<Huodong> list = huodongDao.selectHuodongList(item, 0, 10);
        ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
        List<Article> articles = new ArrayList<Article>();

        for (Huodong huodong : list)
        {
          Article article = new Article(huodong.getHdname(), "进入后编辑", huodong.getHeadimgurl(), weburl + "huodong/huodongdetail?id="
              + huodong.getId() + "&viewid=" + fromUserName);
          articles.add(article);
        }

        imgTextMsgVo.setList(articles);
        imgTextMsgVo.setMsgType("news");
        result = sendTWMsg(imgTextMsgVo);
        return result;

      }
      else if (eventKey.getText().equals("V1002_CANDATE"))
      {
        logger.info("{} click V1002_CANDATE", fromUserName);
        WeChatUser update = new WeChatUser();
        update.setCandate(1);// 今晚有空
        weChatUserDao.update(update, new WeChatUser(fromUserName));
        WeChatUser weChatUser = weChatUserDao.select(new WeChatUser(fromUserName));
        if (Doubles.isFinite(weChatUser.getLatitude()) && Doubles.isFinite(weChatUser.getLongitude()))
        {
          if (StringUtils.isBlank(weChatUser.getSex()))
          {
            TextMsgVo textMsgVo = null;
            textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
            textMsgVo.setContent("您还没有填写您的性别信息");
            result = sendTextMsg(textMsgVo);
            logger.info("handleTextMsg return {}", result);
            return result;
          }
          else
          {
            // 设置成异性
            if (weChatUser.getSex().equals("1"))
            {
              weChatUser.setSex("2");
            }
            else
            {
              weChatUser.setSex("1");
            }
          }
          List<WeChatUser> list = weChatUserDao.selectNearUser(weChatUser);
          if (list.size() == 0)
          {
            TextMsgVo textMsgVo = null;
            textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
            textMsgVo.setContent("您附近1公里内还没有其他用户今天有空");
            result = sendTextMsg(textMsgVo);
            logger.info("handleTextMsg return {}", result);
            return result;
          }
          ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
          List<Article> articles = new ArrayList<Article>();
          for (WeChatUser weChatUser1 : list)
          {
            Article article = new Article(weChatUser1.getNickname(), "进入给他留言可直接发送至他的微信", weChatUser1.getHeadimgurl(), weburl
                + "wechatuser/detail?fromid=" + weChatUser1.getFromid() + "&viewid=" + fromUserName);
            articles.add(article);
          }
          imgTextMsgVo.setList(articles);
          imgTextMsgVo.setMsgType("news");
          result = sendTWMsg(imgTextMsgVo);
          return result;
        }
        else
        {
          TextMsgVo textMsgVo = null;
          textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
          textMsgVo.setContent("您还没有开启提供位置信息，请点击右上角人形图标打开提供位置信息开关");
          result = sendTextMsg(textMsgVo);
          logger.info("handleTextMsg return {}", result);
          return result;
        }
      }
      else if (eventKey.getText().equals("V1002_VIEWACTIVITY"))
      {
        TextMsgVo textMsgVo = null;
        WeChatUser weChatUser = weChatUserDao.selectRecommondUser(new WeChatUser(fromUserName));
        if (weChatUser == null)
        {
          textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
          textMsgVo.setContent("请您先选择会员-上传形象照");
          result = sendTextMsg(textMsgVo);
          logger.info("handleTextMsg return {}", result);
          return result;

        }
        // 查看附近约会
        WeChatUser wu = weChatUserDao.select(new WeChatUser(fromUserName));

        if (wu.getLatitude() == null || wu.getLongitude() == null)
        {
          TextMsgVo vo = new TextMsgVo(fromUserName, toUserName, createTime);
          vo.setContent("您还没有开启提供位置信息，请点击右上角人形图标打开提供位置信息开关");
          result = sendTextMsg(vo);
          logger.info("handleTextMsg return {}", result);
          return result;
        }
        ImgTextMsgVo imgTextMsgVo = new ImgTextMsgVo(fromUserName, toUserName, createTime);
        List<Article> articles = new ArrayList<Article>();

        List<Huodong> list = huodongDao.selectNear(wu.getLatitude(), wu.getLongitude(), 5);
        if (list == null || list.size() == 0)
        {
          TextMsgVo vo = new TextMsgVo(fromUserName, toUserName, createTime);
          vo.setContent("您附近暂时没有活动，您可以尝试发起一个活动");
          result = sendTextMsg(vo);
          logger.info("handleTextMsg return {}", result);
          return result;
        }
        else
        {
          for (Huodong huodong : list)
          {
            if (huodong.getHdname() == null)
            {
              huodong.setHdname("点击查看");
            }
            Article article = new Article(huodong.getHdname(), "进入后识别二维码添加联系人", huodong.getImgurl(), weburl
                + "huodong/huodongdetail?id=" + huodong.getId() + "&viewid=" + fromUserName);
            articles.add(article);
          }
        }

        imgTextMsgVo.setList(articles);
        imgTextMsgVo.setMsgType("news");
        result = sendTWMsg(imgTextMsgVo);
        return result;
      }
    }
    else if (event.getText().equals("subscribe"))
    {
      WeChatUser vo = new WeChatUser(fromUserName);

      String userInfo = getUserInfo(fromUserName);
      JSONObject jsonObject = JSON.parseObject(userInfo);
      String nickname = jsonObject.getString("nickname");
      String sex = jsonObject.getString("sex");
      String city = jsonObject.getString("city");
      String province = jsonObject.getString("province");
      String country = jsonObject.getString("country");
      String headimgurl = jsonObject.getString("headimgurl");
      vo.setNickname(nickname);
      vo.setSex(sex);
      vo.setProvince(province);
      vo.setCity(city);
      vo.setCountry(country);
      vo.setHeadimgurl(headimgurl);

      TextMsgVo textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
      textMsgVo.setContent("欢迎关注\"他她社区\"！他她社区缘起于北京高校，是一个优质的单身青年婚恋交友平台！回复GG1,GG2,GG3...查看男生征婚信息，回复MM1,MM2,MM3,...查看女生征婚信息");
      result = sendTextMsg(textMsgVo);
      logger.info("handleTextMsg return {}", result);

      try
      {
        weChatUserDao.insert(vo);
      }
      catch (Exception e)
      {
        logger.error(e.getMessage(), e);
      }
      return result;
    }
    else if (event.getText().equals("LOCATION"))
    {
      String Latitude = rootElement.element("Latitude").getText();
      String Longitude = rootElement.element("Longitude").getText();
      WeChatUser weChatUser = new WeChatUser(fromUserName);
      weChatUser.setLatitude(Double.parseDouble(Latitude));
      weChatUser.setLongitude(Double.parseDouble(Longitude));
      WeChatUser wu = weChatUserDao.select(weChatUser);
      if (wu == null)
      {
        weChatUserDao.insert(weChatUser);
      }
      else
      {
        if (StringUtils.isBlank(wu.getProvince()))
        {
          // todo,增加根据经纬度获取省份
        }
        weChatUserDao.update(weChatUser, wu);
      }

    }
    else if (event.getText().equals("pic_photo_or_album"))
    {
      logger.info("pic_photo_or_album event");
      if (eventKey.getText().equals("V1001_SHOW"))
      {

        eventMap.put(fromUserName + createTime, "V1001_SHOW");
      }
      else
      {

      }
    }
    else if (event.getText().equals("pic_weixin"))
    {
      logger.info("pic_weixin event");

      if (eventKey.getText().equals("V1002_CREATEDATE"))
      {
        eventMap.put(fromUserName, "V1002_CREATEDATE");
        logger.info("map key is {},value V1002_CREATEDATE", fromUserName);
      }
    }
    else if (event.getText().equals("location_select"))
    {
      logger.info("location_select event");
      TextMsgVo textMsgVo = null;
      WeChatUser weChatUser = weChatUserDao.selectRecommondUser(new WeChatUser(fromUserName));
      if (weChatUser == null)
      {
        textMsgVo = new TextMsgVo(fromUserName, toUserName, createTime);
        textMsgVo.setContent("请您先选择会员-上传形象照");
        result = sendTextMsg(textMsgVo);
        logger.info("handleTextMsg return {}", result);
        return result;

      }
    }
    else if (event.getText().equals("SCAN"))
    {
      WeChatUserfrom wechatUserfrom = new WeChatUserfrom(fromUserName);
      wechatUserfrom.setEventkey(eventKey.getText());
      weChatUserfromDao.insert(wechatUserfrom);
      logger.info("scan ok");
    }
    return result;
  }

  private String handleOtherMsg(Element rootElement)
  {
    return "";
  }

  private void getBaseNode(Element rootElement, Node toUserName, Node fromUserName, Node createTime)
  {

  }

  public static String sendTextMsg(TextMsgVo vo)
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<xml>")
        .append("<ToUserName><![CDATA[").append(vo.getFromUserName()).append("]]></ToUserName>")
        .append("<FromUserName><![CDATA[").append(vo.getToUserName()).append("]]></FromUserName>")
        .append("<CreateTime>").append(vo.getCreateTime()).append("</CreateTime>")
        .append("<MsgType><![CDATA[").append("text").append("]]></MsgType>")
        .append("<Content><![CDATA[").append(vo.getContent()).append("]]></Content>")
        .append("</xml>");
    return sb.toString();
  }

  private String sendImgVoiceMsg(ImgVoiceMsgVo vo)
  {
    StringBuffer sb = new StringBuffer();
    if (vo.getMsgType().equals("image"))
    {
      sb.append("<xml>")
          .append("<ToUserName><![CDATA[").append(vo.getFromUserName()).append("]]></ToUserName>")
          .append("<FromUserName><![CDATA[").append(vo.getToUserName()).append("]]></FromUserName>")
          .append("<CreateTime>").append(vo.getCreateTime()).append("</CreateTime>")
          .append("<MsgType><![CDATA[").append(vo.getMsgType()).append("]]></MsgType>")
          .append("<Image><MediaId><![CDATA[").append(vo.getMediaId()).append("]]></MediaId></Image>")
          .append("</xml>");
    }
    else if (vo.getMsgType().equals("voice"))
    {
      sb.append("<xml>")
          .append("<ToUserName><![CDATA[").append(vo.getFromUserName()).append("]]></ToUserName>")
          .append("<FromUserName><![CDATA[").append(vo.getToUserName()).append("]]></FromUserName>")
          .append("<CreateTime>").append(vo.getCreateTime()).append("</CreateTime>")
          .append("<MsgType><![CDATA[").append(vo.getMsgType()).append("]]></MsgType>")
          .append("<Voice><MediaId><![CDATA[").append(vo.getMediaId()).append("]]></MediaId></Voice>")
          .append("</xml>");
    }

    return sb.toString();
  }

  private String sendTWMsg(ImgTextMsgVo vo)
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<xml>")
        .append("<ToUserName><![CDATA[").append(vo.getFromUserName()).append("]]></ToUserName>")
        .append("<FromUserName><![CDATA[").append(vo.getToUserName()).append("]]></FromUserName>")
        .append("<CreateTime>").append(vo.getCreateTime()).append("</CreateTime>")
        .append("<MsgType><![CDATA[").append(vo.getMsgType()).append("]]></MsgType>")
        .append("<ArticleCount><![CDATA[").append(vo.getList().size()).append("]]></ArticleCount>")
        .append("<Articles>");
    for (Article article : vo.getList())
    {
      sb.append("<item>");
      sb.append("<Title><![CDATA[").append(article.getTitle()).append("]]></Title>")
          .append("<Description><![CDATA[").append(article.getDescription()).append("]]></Description>")
          .append("<PicUrl><![CDATA[").append(article.getPicurl()).append("]]></PicUrl>")
          .append("<Url><![CDATA[").append(article.getUrl()).append("]]></Url>");
      sb.append("</item>");
    }
    sb.append("</Articles>");
    sb.append("</xml>");
    return sb.toString();
  }

  public String getUserInfo(String openid)
  {
    String userInfoUrl = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + ATTimerTask.accessToken +
        "&openid=" + openid + "&lang=zh_CN";
    return HttpUtils.httpsCon(userInfoUrl);

  }

  public void sendKFMsg(WexinComment huodongComment, String seq)
  {
    String content = huodongComment.getContent();
    String[] args = content.split(" ");
    content = content.replaceAll(args[0], "");
    WeChatUser weChatUser = weChatUserDao.select(new WeChatUser(huodongComment.getFromid()));
    String cc = weChatUser.getNickname() + "给你留言：" + content + " (回复" + seq + huodongComment.getId() + " 空格 你要回复的内容,可直接向他发消息)";
    String result = HttpUtils.sendKFMsg(huodongComment.getToid(), cc);

    logger.info("comment success result {}", result);
  }

  @RequestMapping(value = "emptyUserInfo")
  @ResponseBody
  public void getEmptyUserinfo()
  {
    List<WeChatUser> weChatUserList = weChatUserDao.selectEmptyUser();
    if (weChatUserList != null)
    {
      for (WeChatUser weChatUser : weChatUserList)
      {
        WeChatUser update = new WeChatUser();

        String userInfo = getUserInfo(weChatUser.getFromid());
        JSONObject jsonObject = JSON.parseObject(userInfo);
        String nickname = jsonObject.getString("nickname");
        String sex = jsonObject.getString("sex");
        String city = jsonObject.getString("city");
        String province = jsonObject.getString("province");
        String country = jsonObject.getString("country");
        String headimgurl = jsonObject.getString("headimgurl");
        logger.info("nickname {},sex {},city {},province {},country {},headimgurl {}",
            nickname, sex, city, province, country, headimgurl);
        update.setNickname(nickname);
        update.setSex(sex);
        update.setProvince(province);
        update.setCity(city);
        update.setCountry(country);
        update.setHeadimgurl(headimgurl);

        weChatUserDao.update(update, new WeChatUser(weChatUser.getFromid()));
      }
    }
  }

}
