package lehuo.lsm.controller;

import lehuo.lsm.dao.WeChatCommentDao;
import lehuo.lsm.dao.WeChatImageDao;
import lehuo.lsm.dao.WeChatUserDao;
import lehuo.lsm.model.WechatModel.db.WeChatComment;
import lehuo.lsm.model.WechatModel.db.WeChatImage;
import lehuo.lsm.model.WechatModel.db.WeChatUser;
import lehuo.lsm.service.impl.HuodongService;
import lehuo.lsm.util.ATTimerTask;
import lehuo.lsm.util.HttpUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/wechatuser")
public class WechatUserController {

    protected static Logger logger = LoggerFactory.getLogger(WechatUserController.class);

    private static final int PAGE = 10;

    @Resource
    private HuodongService huodongService;

    @Resource
    private WeChatUserDao weChatUserDao;

    @Resource
    private WeChatImageDao weChatImageDao;

    @Resource
    private WeChatCommentDao weChatCommentDao;

    @RequestMapping(value = "/index")
    public String index(String fromid){
        WeChatUser weChatUser = weChatUserDao.selectUserBrief(new WeChatUser(fromid));
        if(weChatUser==null){
            return "redirect:/wechatuser/edit?fromid="+fromid;
        }else{
            return "redirect:/wechatuser/detail?fromid="+fromid+"&viewid="+fromid;
        }
    }

    @RequestMapping(value = "/detail")
    public String detail(ModelMap map,String fromid,String viewid){
        logger.info("detail fromid {},viewid {}",fromid,viewid);

        List<WeChatComment> comments = null;
        WeChatComment item = new WeChatComment();
        item.setPagefromid(fromid);
        if(!fromid.equals(viewid)){
            item.setFromid(viewid);
            item.setToid(fromid);
            List<WeChatComment> tmp = weChatCommentDao.selectCommentandInfo(item);
            item.setFromid(fromid);
            item.setToid(viewid);
            comments = weChatCommentDao.selectCommentandInfo(item);
            comments.addAll(tmp);
            weChatUserDao.updateViewnum(fromid);
        }else{
            item.setToid(fromid);
            comments = weChatCommentDao.selectCommentandInfo(item);
        }
        map.put("comments",comments);

        WeChatUser weChatUser = weChatUserDao.selectUserBrief(new WeChatUser(fromid));
        map.put("user",weChatUser);
        map.put("viewid",viewid);
        return "wechatuserdetail";
    }

    @RequestMapping(value = "/edit")
    public String edit(ModelMap map,String fromid){
        logger.info("edit fromid {},viewid {}",fromid);

        List<WeChatComment> comments = null;
        WeChatComment item = new WeChatComment();
        item.setToid(fromid);
        item.setPagefromid(fromid);
        comments = weChatCommentDao.selectCommentandInfo(item);

        WeChatUser weChatUser = weChatUserDao.selectUserBrief(new WeChatUser(fromid));
        map.put("comments",comments);
        map.put("user",weChatUser);
        return "wechatuseredit";
    }

    @RequestMapping(value = "/updatedetail")
    public String updatedetail(WeChatUser weChatUser){

        weChatUserDao.update(weChatUser,new WeChatUser(weChatUser.getFromid()));
        return "redirect:/wechatuser/detail?fromid="+weChatUser.getFromid()+"&viewid="+weChatUser.getFromid();
    }

    @RequestMapping(value = "/updatescore")
    @ResponseBody
    public String updateScore(String data,String score,String fromid){

        try{
            logger.info("updateScore data {},score {}",data,score);
            data = data.replace("rating:", "");
            WeChatImage weChatImage = weChatImageDao.select(new WeChatImage(Integer.parseInt(data)));
            String content = "\"content\":\" 有位异性给您打分了"+score+"分\"\n";
            kefuMsg(weChatImage.getUserfromid(),"text",content);
            weChatImageDao.updateScore(Integer.parseInt(data),Integer.parseInt(score));
        }catch (Exception e){
            logger.error("updateScore",e);
        }

        return "success";
    }

    @RequestMapping(value = "/comment")
    public String comment(WeChatComment weChatComment){
        weChatComment.setContent(weChatComment.getContent().trim());
        weChatCommentDao.insert(weChatComment);
        WeChatUser weChatUser = weChatUserDao.select(new WeChatUser(weChatComment.getFromid()));
        String content = "\"content\":\""+weChatUser.getNickname()+"给你留言："+weChatComment.getContent()+" (回复20"+weChatComment.getId()+" 空格 你要回复的内容,可直接向他发消息) \"\n";
        kefuMsg(weChatComment.getToid(),"text",content);
        return "redirect:/wechatuser/detail?fromid="+weChatComment.getFromid()+"&viewid="+weChatComment.getToid();
    }

    public static void kefuMsg(String toid,String msgtype,String content){
        logger.info("kefuMsg is {}",content);
        String result = HttpUtils.sendPost("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="+ ATTimerTask.accessToken,
                "{\n" +
                        "    \"touser\":\""+toid+"\",\n" +
                        "    \"msgtype\":\""+msgtype+"\",\n" +
                        "    \""+msgtype+"\":\n" +
                        "    {\n" + content + "}\n}");


        logger.info("comment success result {}",result);
    }

    public static void main(String[] args){
        /*String result = HttpUtils.sendPost("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=Ld7sv4hITcHVlBAYhVofno37Y8FNEjAVK12Lbmsd8RhtG0vq8Qn_TNvKNwAL3xeWhfIt1I1sZ4ZIXFCyy5sqZ9zpYR2XTWABq7hpuSUV5c8",
                "{\n" +
                        "    \"touser\":\"o_lAJj0UWeutodZVfxKMSA_MafoQ\",\n" +
                        "    \"msgtype\":\"text\",\n" +
                        "    \"text\":\n" +
                        "    {\n" +
                        "         \"content\":\"直接回复官方活动+空格+您的微信号可直接报名（例：官方活动 pku201548367）\"\n" +
                        "    }\n" +
                        "}");
*/
        String content="\"content\":\" 有位异性给您打分了分\"\n";
        System.out.println(content);

    }

    @RequestMapping(value = "changeshow")
    public void changeShow(String imgurl,Integer imgshow){
        logger.info("changeShow imgurl {},imgshow {}",imgurl,imgshow);
        WeChatImage item = new WeChatImage();
        item.setImgurl(imgurl);
        WeChatImage update = new WeChatImage();
        update.setImgtype(imgshow);
        weChatImageDao.update(update,item);
    }

    @RequestMapping(value = "importuser")
    public String importUser(WeChatUser weChatUser,String[] imgurl){
        String fromid = UUID.randomUUID().toString();

        weChatUser.setFromid(fromid);
        weChatUserDao.insert(weChatUser);

        List<WeChatImage> list = new ArrayList<WeChatImage>();
        if(imgurl==null){

        }else{
            for(String url:imgurl){
                WeChatImage weChatImage = new WeChatImage(fromid);
                weChatImage.setImgurl(url);
                list.add(weChatImage);
            }
            weChatImageDao.batchInsert(list);
        }

        return "redirect:/admin/judgeimg";
    }

    @RequestMapping(value = "qunfajiekou")
    @ResponseBody
    public void qunfajiekou(String fromid,String content,Integer offset){

        if(StringUtils.isBlank(content)){
            content = "直接回复官方活动 空格 您的微信号 可直接报名\n如：官方活动 stev897";
        }

        String c = "\"content\":\" "+content+"\"\n";

        if(!StringUtils.isBlank(fromid)){
            kefuMsg(fromid,"text",c);
        }else{
            List<WeChatUser> list = weChatUserDao.selectList(null,offset,500);
            for(WeChatUser user:list){
                logger.info("qunfajiekou {}",user.getFromid());
                kefuMsg(user.getFromid(), "text", content);
            }
        }

    }


}
