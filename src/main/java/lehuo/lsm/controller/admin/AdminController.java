package lehuo.lsm.controller.admin;

import lehuo.lsm.global.Global;
import lehuo.lsm.model.Comment;
import lehuo.lsm.model.Images;
import lehuo.lsm.model.Links;
import lehuo.lsm.model.PkImg;
import lehuo.lsm.service.impl.*;
import lehuo.lsm.util.ImgDownload;
import lehuo.lsm.util.ImgUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    @Resource
    DoubanProcesser doubanProcesser;

    @Resource
    ShuimuProcesser shuimuProcesser;
    @Resource
    LinksService linksService;


    private Integer pageSize = 30;

    private volatile Boolean doubanstart = false;
    private volatile Boolean tiebastart = false;
    private volatile Boolean shuimustart = false;
    @Resource
    ImagesService imagesService;

    @RequestMapping(value = "/test")
    @ResponseBody
    public String test(){
        return "test success";
    }

    @RequestMapping(value = "/doubanaction")
    @ResponseBody
    public String doubanaction(){
        doubanProcesser.action(null);
        doubanstart = true;
        return "doubanaction begin";
    }

    @RequestMapping(value = "/tiebaaction")
    @ResponseBody
    public String tiebaaction(){

        tiebastart = true;
        return "tiebaaction begin";
    }

    @RequestMapping(value = "/shuimuaction")
    @ResponseBody
    public String shuimuaction(){
        shuimuProcesser.action(null);
        shuimustart = true;
        return "shuimuaction begin";
    }

    @RequestMapping(value = "/allaction")
    @ResponseBody
    public String allaction(){
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                doubanstart = true;
                doubanProcesser.action(null);

            }
        };
        Runnable r2 = new Runnable() {
            @Override
            public void run() {
                tiebastart = true;

            }
        };

        Runnable r3 = new Runnable() {
            @Override
            public void run() {
                shuimustart = true;
                shuimuProcesser.action(null);
            }
        };

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        t1.start();
        t2.start();
        t3.start();


        return "allaction begin";
    }

    @RequestMapping(value = "/stopaction")
    @ResponseBody
    public String stopaction(){
        if(doubanstart){
            doubanProcesser.stopSpider();
            doubanstart = false;
        }

        if(tiebastart){

            tiebastart = false;
        }

        if(shuimustart){
            shuimuProcesser.stopSpider();
            shuimustart = false;
        }

        return "stoped successful";
    }

    @RequestMapping(value = "/imgutils")
    @ResponseBody
    public void imgutils(String start){
        if(StringUtils.isBlank(start)){
            start = "0";
        }
        ImgUtils.generate(Integer.parseInt(start));
    }

    @RequestMapping(value = "/imgdownload")
    @ResponseBody
    public void imgdownload(String start){
        if(StringUtils.isBlank(start)){
            start = "0";
        }
        ImgDownload.generate(Integer.parseInt(start));
    }

    @RequestMapping(value = "/judgeimg")
    public String judgeimg(Integer offset,ModelMap map){
        if(offset==null){
            offset = 1;
        }

        Integer currentPage = offset;

        List<Images> list = imagesService.selectNotJudged((currentPage-1)*pageSize, pageSize);

        map.put("list", list);
        map.put("currentPage", currentPage);
        return "admin";
    }

    @RequestMapping(value = "/openimport")
    public String openImport(String id,ModelMap map){
        Links link = linksService.select(new Links(Integer.parseInt(id)));

        List<Links> images= linksService.selectDetail(Integer.parseInt(id));

        map.put("link",link);
        map.put("images",images);
        return "wechatuserimport";
    }


    @RequestMapping(value = "changeshow")
    public void changeShow(Integer imgid,Integer imgshow){
        Images image = new Images();
        image.setImgshow(imgshow);
        image.setDeal(2);
        imagesService.update(image,new Images(imgid));
    }

    @RequestMapping(value = "changecatagory")
    public void changeCatagory(Integer imgid,Integer catagory){
        Images image = new Images();
        image.setCatagory(catagory);
        image.setDeal(2);
        imagesService.update(image,new Images(imgid));
    }

}
