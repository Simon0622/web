package lehuo.lsm.dao;


import lehuo.lsm.model.ActivityComment;
import lehuo.lsm.model.Comment;
import org.springframework.stereotype.Component;

@Component
public interface ActivityCommentDao extends CommonCRUD<ActivityComment> {

}
