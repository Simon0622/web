package lehuo.lsm.dao;


import lehuo.lsm.model.Activity;
import lehuo.lsm.model.Images;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ActivityDao extends CommonCRUD<Activity> {

    public List<Activity> simpleQuery(@Param("keyword") String keyword,@Param("querytype") String querytype,@Param("hasloc") String hasloc,
                                      @Param("offset") Integer offset,@Param("limit") Integer limit);

    }
