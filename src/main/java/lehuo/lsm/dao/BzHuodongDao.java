package lehuo.lsm.dao;

import lehuo.lsm.model.WechatModel.db.BzHuodong;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by simonliu on 2014/8/2.
 */
@Component
public interface BzHuodongDao extends CommonCRUD<BzHuodong>{

}
