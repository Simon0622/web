package lehuo.lsm.dao;

import lehuo.lsm.model.CaoliuVo;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by simonliu on 2014/8/2.
 */
@Component
public interface CaoliuDao extends CommonCRUD<CaoliuVo>{


    public void insertCL(CaoliuVo vo);

    public void batchInsertCL(List<CaoliuVo> list);

    public List<CaoliuVo> keySearch(String keyword);
}
