package lehuo.lsm.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommonCRUD<T>{
	public int insert(T t);
	
	public int delete(@Param("item") T t);
	
	public int update(@Param("update") T update,@Param("item") T item);
	
	public T select(@Param("item") T t);

    public List<T> selectList(@Param("item") T t,
                                   @Param("offset") Integer offset,@Param("limit") Integer limit);

    public void batchInsert(List<T> list);

    public Integer selectCount(@Param("item") T t);
}
