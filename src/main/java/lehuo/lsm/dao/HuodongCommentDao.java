package lehuo.lsm.dao;


import lehuo.lsm.model.WechatModel.db.HuodongComment;
import lehuo.lsm.model.WechatModel.db.WeChatComment;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface HuodongCommentDao extends CommonCRUD<HuodongComment> {
    public List<HuodongComment> selectCommentandInfo(HuodongComment item);
}
