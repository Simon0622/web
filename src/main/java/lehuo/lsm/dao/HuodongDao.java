package lehuo.lsm.dao;


import lehuo.lsm.model.WechatModel.db.Huodong;
import lehuo.lsm.model.WechatModel.db.WeChatUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface HuodongDao extends CommonCRUD<Huodong> {

    public List<Huodong> selectHuodongList(@Param("item") Huodong h,
                                           @Param("offset") Integer offset,@Param("limit") Integer limit);

    public List<Huodong> selectNear(@Param("latitude") Double latitude,@Param("longitude") Double longitude,@Param("limit") Integer limit);

    public Huodong selectTodayLast(@Param("today") String today,@Param("userfromid") String userfromid);

    public Huodong selectUserHuodong(@Param("item") Huodong h);

    public void updatehwm(@Param("update") Huodong update,@Param("item") Huodong item);

}
