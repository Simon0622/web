package lehuo.lsm.dao;


import lehuo.lsm.model.Images;
import lehuo.lsm.model.Links;
import lehuo.lsm.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ImagesDao extends CommonCRUD<Images> {

    public Integer selectMax();

    public List<Images> selectbyoffset(@Param("offset") Integer offset,
                                       @Param("limit") Integer limit);

    public List<Images> selectRanklist(@Param("offset") Integer offset,
                                       @Param("limit") Integer limit,
                                       @Param("catagory") Integer catagory);

    public List<Images> selectLowpknumImg(@Param("offset") Integer offset,
                                          @Param("limit") Integer limit);

    public List<Images> selectNotJudged(@Param("offset") Integer offset,
                                        @Param("limit") Integer limit);

    public void updateImgWin(@Param("id") Integer id);

    public void updateImgLose(@Param("id") Integer id);

    public void updateImgStore(@Param("id") Integer imgid,@Param("score") Integer score);

    public List<Images> rankbypk(@Param("user") User user,@Param("offset") Integer offset,
                                 @Param("limit") Integer pagesize);

    public List<Images> rankbyscore(@Param("user") User user,@Param("offset") Integer offset,
                                    @Param("limit") Integer pagesize);

    public List<Images> rankbyvisitnum(@Param("offset") Integer offset,@Param("limit") Integer pagesize);

    public List<Images> userrankbysome(@Param("rankby") Integer rankby,@Param("offset") Integer offset,
                                       @Param("limit") Integer pagesize);
}
