package lehuo.lsm.dao;


import lehuo.lsm.model.PkImg;
import org.springframework.stereotype.Component;

@Component
public interface ImgScoreDao extends CommonCRUD<PkImg> {
	
}
