package lehuo.lsm.dao;


import lehuo.lsm.model.RTStock;

import java.util.List;

/**
 * Created by simonliu on 2014/8/2.
 */
public interface RTStockDao  extends CommonCRUD<RTStock>{

    public List<String> selectRTStock();

    public List<RTStock> selectbyCode();

}
