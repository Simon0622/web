package lehuo.lsm.dao;

import lehuo.lsm.model.CaoliuVo;
import lehuo.lsm.model.SearchWord;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by simonliu on 2014/8/2.
 */
@Component
public interface SearchWordDao extends CommonCRUD<SearchWord>{

}
