package lehuo.lsm.dao;


import lehuo.lsm.model.Stock;


import java.util.List;

/**
 * Created by simonliu on 2014/8/2.
 */
public interface StockDao extends CommonCRUD<Stock>{
    public List<String> selectALL();

}
