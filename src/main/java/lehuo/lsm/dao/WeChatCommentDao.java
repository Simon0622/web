package lehuo.lsm.dao;


import lehuo.lsm.model.WechatModel.db.WeChatComment;
import lehuo.lsm.model.WechatModel.db.WeChatImage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface WeChatCommentDao extends CommonCRUD<WeChatComment> {
    public List<WeChatComment> selectCommentandInfo(WeChatComment item);
}
