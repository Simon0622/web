package lehuo.lsm.dao;


import lehuo.lsm.model.WechatModel.db.WeChatImage;
import lehuo.lsm.model.WechatModel.db.WeChatUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component
public interface WeChatImageDao extends CommonCRUD<WeChatImage> {

    public WeChatImage selectClickMin(@Param("item") WeChatImage item);

    public void updateScore(@Param("id") Integer id,@Param("score") Integer score);
}
