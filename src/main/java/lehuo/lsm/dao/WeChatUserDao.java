package lehuo.lsm.dao;


import lehuo.lsm.model.Comment;
import lehuo.lsm.model.WechatModel.db.WeChatUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface WeChatUserDao extends CommonCRUD<WeChatUser> {

    public List<WeChatUser> selectListnoSelf(@Param("item") WeChatUser item);

    public WeChatUser selectRecommondUser(@Param("item") WeChatUser item);

    public void updaterecnum(@Param("fromid") String fromid);

    public WeChatUser selectUserBrief(@Param("item") WeChatUser item);

    public void updateViewnum(String fromid);

    public List<WeChatUser> selectNearUser(WeChatUser item);

    public List<WeChatUser> selectEmptyUser();

}
