package lehuo.lsm.dao;


import lehuo.lsm.model.WechatModel.db.WeChatUserfrom;
import org.springframework.stereotype.Component;


@Component
public interface WeChatUserfromDao extends CommonCRUD<WeChatUserfrom> {

}
