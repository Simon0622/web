package lehuo.lsm.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import lehuo.lsm.model.WechatModel.db.Zhenghun;

/**
 * Created on 2015/5/10.
 */
@Component
public interface ZhenghunDao extends CommonCRUD<Zhenghun>
{

  public Zhenghun selectGivenNum(@Param("item") String item);
}
