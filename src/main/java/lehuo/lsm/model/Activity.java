package lehuo.lsm.model;

/**
 * Created by simon on 14/11/16.
 */
public class Activity {
    Integer id;
    String activityname;
    Integer userid;
    Integer activitytype;
    String activitycontent;
    String location;
    String extra;

    String username;

    String address;
    String tel;
    String qq;
    String staffnum;
    String renjun;
    String renqi;
    String originlink;
    Integer imgId;

    public Activity(){}

    public Activity(String activityname,Integer userid,Integer activitytype,String activitycontent,String location,String extra,String originlink){
        this.activityname = activityname;
        this.userid = userid;
        this.activitytype = activitytype;
        this.activitycontent = activitycontent;
        this.location = location;
        this.extra = extra;
        this.originlink = originlink;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivityname() {
        return activityname;
    }

    public void setActivityname(String activityname) {
        this.activityname = activityname;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getActivitytype() {
        return activitytype;
    }

    public void setActivitytype(Integer activitytype) {
        this.activitytype = activitytype;
    }

    public String getActivitycontent() {
        return activitycontent;
    }

    public void setActivitycontent(String activitycontent) {
        this.activitycontent = activitycontent;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getStaffnum() {
        return staffnum;
    }

    public void setStaffnum(String staffnum) {
        this.staffnum = staffnum;
    }

    public String getRenjun() {
        return renjun;
    }

    public void setRenjun(String renjun) {
        this.renjun = renjun;
    }

    public String getRenqi() {
        return renqi;
    }

    public void setRenqi(String renqi) {
        this.renqi = renqi;
    }

    public String getOriginlink() {
        return originlink;
    }

    public void setOriginlink(String originlink) {
        this.originlink = originlink;
    }

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }
}
