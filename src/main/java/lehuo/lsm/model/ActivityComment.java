package lehuo.lsm.model;

/**
 * Created with IntelliJ IDEA.
 * User: shumeng.liu
 * Date: 14-11-27
 * Time: 下午2:27
 * To change this template use File | Settings | File Templates.
 */
public class ActivityComment {
    Integer id;

    Integer activity_id;

    String comment;

    Integer userid;

    public ActivityComment(){

    }

    public ActivityComment(Integer activity_id, String comment, Integer userid) {
        this.activity_id = activity_id;
        this.comment = comment;
        this.userid = userid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(Integer activity_id) {
        this.activity_id = activity_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }
}
