package lehuo.lsm.model;

/**
 * Created with IntelliJ IDEA.
 * User: simonliu
 * Date: 14-11-3
 * Time: 下午10:56
 * To change this template use File | Settings | File Templates.
 */
public class Comment {
    Integer id;

    Integer links_id;

    String comment;

    Integer userid;

    public Comment(){}

    public Comment(Integer linkId, String comment,Integer userid) {
        this.links_id = linkId;
        this.comment = comment;
        this.userid = userid;
    }

    public Comment(Integer links_id){
        this.links_id = links_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLinks_id() {
        return links_id;
    }

    public void setLinks_id(Integer links_id) {
        this.links_id = links_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }
}
