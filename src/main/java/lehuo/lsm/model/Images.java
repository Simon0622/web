package lehuo.lsm.model;

/**
 * Created with IntelliJ IDEA.
 * User: simonliu
 * Date: 14-11-1
 * Time: 下午1:41
 * To change this template use File | Settings | File Templates.
 */
public class Images {
    Integer id;
    Integer links_id;

    String img_link;
    String path;

    Integer catagory;
    Integer imgshow;

    Integer pknum;
    Integer pkscore;
    Integer startnum;
    Integer startscore;

    Integer deal;
    Integer userid;

    String title;

    Integer visit_num;

    Integer count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public Images(){}

    public Images(Integer id){
        this.id = id;
    }

    public Images(String img_link){
        this.img_link = img_link;
    }

    public Images(Integer links_id,String img_link){
        this.links_id = links_id;
        this.img_link = img_link;
    }


    public Images(Integer links_id,String img_link,String path){
        this.links_id = links_id;
        this.img_link = img_link;
        this.path = path;
    }

    public Images(Integer links_id,String img_link,Integer catagory){
        this.links_id = links_id;
        this.img_link = img_link;
        this.catagory = catagory;
    }

    public Images(Integer links_id,String img_link,Integer catagory,String path){
        this.links_id = links_id;
        this.img_link = img_link;
        this.catagory = catagory;
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLinks_id() {
        return links_id;
    }

    public void setLinks_id(Integer links_id) {
        this.links_id = links_id;
    }

    public String getImg_link() {
        return img_link;
    }

    public void setImg_link(String img_link) {
        this.img_link = img_link;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getCatagory() {
        return catagory;
    }

    public void setCatagory(Integer catagory) {
        this.catagory = catagory;
    }

    public Integer getImgshow() {
        return imgshow;
    }

    public void setImgshow(Integer imgshow) {
        this.imgshow = imgshow;
    }

    public Integer getPknum() {
        return pknum;
    }

    public void setPknum(Integer pknum) {
        this.pknum = pknum;
    }

    public Integer getPkscore() {
        return pkscore;
    }

    public void setPkscore(Integer pkscore) {
        this.pkscore = pkscore;
    }

    public Integer getStartnum() {
        return startnum;
    }

    public void setStartnum(Integer startnum) {
        this.startnum = startnum;
    }

    public Integer getStartscore() {
        return startscore;
    }

    public void setStartscore(Integer startscore) {
        this.startscore = startscore;
    }

    public Integer getDeal() {
        return deal;
    }

    public void setDeal(Integer deal) {
        this.deal = deal;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getVisit_num() {
        return visit_num;
    }

    public void setVisit_num(Integer visit_num) {
        this.visit_num = visit_num;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
