package lehuo.lsm.model;

/**
 * Created by simon on 14/11/14.
 */
public class ImgScore {
    Integer id;
    Integer score;
    Integer userid;
    Integer img_id;

    public ImgScore(){}

    public ImgScore(Integer img_id,Integer score,Integer userid){
        this.img_id = img_id;
        this.score = score;
        this.userid = userid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public void setScore(Integer score) {
        this.score = score;
    }



    public Integer getImg_id() {
        return img_id;
    }

    public void setImg_id(Integer img_id) {
        this.img_id = img_id;
    }
}
