package lehuo.lsm.model;

/**
 * Created with IntelliJ IDEA.
 * User: simonliu
 * Date: 14-11-1
 * Time: 下午1:41
 * To change this template use File | Settings | File Templates.
 */
public class Links {
    int id;

    String link;

    String domains;

    Integer reply;

    String extra;

    String author;

    String content;

    String img;

    String title;

    String authorlink;

    String posttime;

    Integer pageno;

    Integer imagesId;

    Integer deal;

    Integer visit_num;

    String img_link;

    public Links() {}

    public Links(Integer id,Integer deal) {
        this.id = id;
        this.deal = deal;
    }

    public Links(String link) {
        this.link = link;
    }

    public Links(BaseVo vo) {
        this.title = vo.getTitle();

        this.author =  vo.getAuthor();

        this.authorlink =  vo.getAuthorlink();

        this.content =  vo.getContent();

        this.posttime =  vo.getPosttime();

        this.img = vo.getImgLinks().toString();
    }

    public Links(String link,String domains,Integer reply,String extra,Integer pageno){
        this.link = link;
        this.domains = domains;
        this.reply = reply;
        this.extra = extra;
        this.pageno = pageno;
    }

    public Integer getImagesId() {
        return imagesId;
    }

    public void setImagesId(Integer imagesId) {
        this.imagesId = imagesId;
    }

    public Links(Integer id) {this.id = id;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDomains() {
        return domains;
    }

    public void setDomains(String domains) {
        this.domains = domains;
    }

    public Integer getReply() {
        return reply;
    }

    public void setReply(Integer reply) {
        this.reply = reply;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorlink() {
        return authorlink;
    }

    public void setAuthorlink(String authorlink) {
        this.authorlink = authorlink;
    }

    public String getPosttime() {
        return posttime;
    }

    public void setPosttime(String posttime) {
        this.posttime = posttime;
    }

    public Integer getPageno() {
        return pageno;
    }

    public void setPageno(Integer pageno) {
        this.pageno = pageno;
    }

    public Integer getDeal() {
        return deal;
    }

    public void setDeal(Integer deal) {
        this.deal = deal;
    }

    public Integer getVisit_num() {
        return visit_num;
    }

    public void setVisit_num(Integer visit_num) {
        this.visit_num = visit_num;
    }

    public String getImg_link() {
        return img_link;
    }

    public void setImg_link(String img_link) {
        this.img_link = img_link;
    }
}
