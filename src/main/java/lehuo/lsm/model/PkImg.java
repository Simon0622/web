package lehuo.lsm.model;

/**
 * Created with IntelliJ IDEA.
 * User: shumeng.liu
 * Date: 14-10-30
 * Time: 下午9:12
 * To change this template use File | Settings | File Templates.
 */
public class PkImg {
    Integer id;
    Integer win;
    Integer lose;
    Integer userid;
    Integer deal;


    public PkImg(){}

    public PkImg(Integer w, Integer l){
        win = w;
        lose = l;
    }

    public PkImg(Integer w, Integer l,Integer userid,Integer deal){
        win = w;
        lose = l;
        this.userid = userid;
        this.deal = deal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWin() {
        return win;
    }

    public void setWin(Integer win) {
        this.win = win;
    }

    public Integer getLose() {
        return lose;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getDeal() {
        return deal;
    }

    public void setDeal(Integer deal) {
        this.deal = deal;
    }

    public void setLose(Integer lose) {
        this.lose = lose;
    }



}
