package lehuo.lsm.model;

/**
 * Created by simon on 4/17/15.
 */
public class RTStock {
    Integer id;
    String code;
    Double v;
    Double p;
    Double a;
    String m;
    String sdate;

    Double avgp;

    Double maxb5;
    Double maxb15;
    Double maxa5;
    Double maxa15;

    Double chigh;
    Double clow;
    Double zdper;

    Double pyper;
    Double b5per;
    Double b15per;

    public Double getV() {
        return v;
    }

    public void setV(String v) {
        try{
            this.v = Double.valueOf(v);
        }catch (Exception e){
            this.v = null;
        }
    }

    public void setVDouble(Double vDouble){
        this.v = vDouble;
    }

    public void setADouble(Double aDouble){
        this.a = aDouble;
    }

    public Double getP() {
        return p;
    }

    public void setP(String p) {
        try{
            this.p = Double.valueOf(p);
        }catch (Exception e){
            this.p = null;
        }
    }

    public Double getA() {
        return a;
    }

    public void setA(String a) {
        try{
            this.a = Double.valueOf(a);
        }catch (Exception e){
            this.a = null;
        }
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    @Override
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("m=").append(m+",").append("p=").append(p+",")
                .append("v=").append(v+",").append("a=").append(a);
        return sb.toString();
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getAvgp() {
        return avgp;
    }

    public void setAvgp(Double avgp) {
        this.avgp = avgp;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getMaxb5() {
        return maxb5;
    }

    public void setMaxb5(Double maxb5) {
        this.maxb5 = maxb5;
    }

    public Double getMaxb15() {
        return maxb15;
    }

    public void setMaxb15(Double maxb15) {
        this.maxb15 = maxb15;
    }

    public Double getMaxa5() {
        return maxa5;
    }

    public void setMaxa5(Double maxa5) {
        this.maxa5 = maxa5;
    }

    public Double getMaxa15() {
        return maxa15;
    }

    public void setMaxa15(Double maxa15) {
        this.maxa15 = maxa15;
    }

    public Double getChigh() {
        return chigh;
    }

    public void setChigh(Double chigh) {
        this.chigh = chigh;
    }

    public Double getClow() {
        return clow;
    }

    public void setClow(Double clow) {
        this.clow = clow;
    }

    public Double getZdper() {
        return zdper;
    }

    public void setZdper(Double zdper) {
        this.zdper = zdper;
    }

    public Double getPyper() {
        return pyper;
    }

    public void setPyper(Double pyper) {
        this.pyper = pyper;
    }

    public Double getB5per() {
        return b5per;
    }

    public void setB5per(Double b5per) {
        this.b5per = b5per;
    }

    public Double getB15per() {
        return b15per;
    }

    public void setB15per(Double b15per) {
        this.b15per = b15per;
    }
}
