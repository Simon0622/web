package lehuo.lsm.model;

/**
 * Created by simon on 14/12/7.
 */
public class SearchWord {
    Integer id;
    Integer userid;
    String word;

    public SearchWord(){}

    public SearchWord(Integer userid,String word){
        this.userid = userid;
        this.word = word;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
