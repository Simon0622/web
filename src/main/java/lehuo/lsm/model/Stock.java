package lehuo.lsm.model;

import java.math.BigDecimal;

/**
 * Created by shumeng.liu on 2015/4/16.
 */
public class Stock {
    String code;
    String sname;
    Double current;
    Double percent;
    Double changes;
    Double high;
    Double low;
    Double high52w;
    Double low52w;
    BigDecimal marketcapital;
    Double amount;
    String pettm;
    Double volume;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Double getCurrent() {
        return current;
    }

    public void setCurrent(Double current) {
        this.current = current;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Double getChanges() {
        return changes;
    }

    public void setChanges(Double changes) {
        this.changes = changes;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getHigh52w() {
        return high52w;
    }

    public void setHigh52w(Double high52w) {
        this.high52w = high52w;
    }

    public Double getLow52w() {
        return low52w;
    }

    public void setLow52w(Double low52w) {
        this.low52w = low52w;
    }

    public BigDecimal getMarketcapital() {
        return marketcapital;
    }

    public void setMarketcapital(BigDecimal marketcapital) {
        this.marketcapital = marketcapital;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPettm() {
        return pettm;
    }

    public void setPettm(String pettm) {
        this.pettm = pettm;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
