package lehuo.lsm.model;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by simon on 4/17/15.
 */
public class StockWrapper {

    Map<String, RTStock> rtStockMap = new LinkedHashMap<String, RTStock>();
    List<RTStock> rtStocks = new LinkedList<RTStock>();
    Double vol = 0.0;
    Double amount = 0.0;
    Double jk = 0.0;
    Double zs = 0.0;

    public StockWrapper() {
    }

    public StockWrapper(List<RTStock> list) {
        rtStocks = list;
    }

    public void putin15(RTStock rtStock) {
        if(rtStockMap.containsKey(rtStock.getM())){
            return;
        }else{
            if(rtStocks.size()>15){
                String key = rtStocks.get(0).getM();
                rtStocks.remove(0);
                rtStockMap.remove(key);
            }else{
                rtStocks.add(rtStock);
            }
            rtStockMap.put(rtStock.getM(),rtStock);
            if(rtStocks.size()!=rtStockMap.size()){
                System.out.println("rtStocks.size()!=rtStockMap.size()");
                //todo add log
            }
        }

        double maxb5 = 0;
        double maxb15 = 0;

        int start5 = (rtStocks.size()-5)>0?(rtStocks.size()-5):0;
        for(int i=start5;i<rtStocks.size();i++){
            maxb5=maxb5>rtStocks.get(i).getP()?maxb5:rtStocks.get(i).getP();
        }

        for(int i=0;i<rtStocks.size();i++){
            maxb15 = maxb15>rtStocks.get(i).getP()?maxb15:rtStocks.get(i).getP();
        }

        rtStock.setMaxb5(maxb5);
        rtStock.setB5per((maxb5 - rtStock.getP()) / rtStock.getP());

        rtStock.setMaxb15(maxb15);
        rtStock.setB15per((maxb15 - rtStock.getP()) / rtStock.getP());
    }

    public Double getVol() {
        return vol;
    }

    public void setVol(Double vol) {
        this.vol = vol;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getJk() {
        return jk;
    }

    public void setJk(Double jk) {
        this.jk = jk;
    }

    public Double getZs() {
        return zs;
    }

    public void setZs(Double zs) {
        this.zs = zs;
    }
}
