package lehuo.lsm.model.WechatModel;

/**
 * Created by simon on 14/12/24.
 */
public class BaseXmlVo {
    String fromUserName;
    String toUserName;
    String createTime;
    String msgType;

    public BaseXmlVo(String fromUserName, String toUserName, String createTime) {
        this.fromUserName = fromUserName;
        this.toUserName = toUserName;
        this.createTime = createTime;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }
}
