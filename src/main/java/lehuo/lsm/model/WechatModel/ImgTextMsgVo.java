package lehuo.lsm.model.WechatModel;

import java.util.List;

/**
 * Created by simon on 14/12/24.
 */
public class ImgTextMsgVo extends BaseXmlVo{

    List<Article> list;

    public ImgTextMsgVo(String fromUserName, String toUserName, String createTime,List<Article> list) {
        super(fromUserName, toUserName, createTime);
        this.list = list;
    }

    public ImgTextMsgVo(String fromUserName, String toUserName, String createTime) {
        super(fromUserName, toUserName, createTime);
    }

    public List<Article> getList() {
        return list;
    }

    public void setList(List<Article> list) {
        this.list = list;
    }

}
