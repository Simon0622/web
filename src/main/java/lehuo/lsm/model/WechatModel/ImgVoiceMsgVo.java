package lehuo.lsm.model.WechatModel;

/**
 * Created by simon on 14/12/24.
 */
public class ImgVoiceMsgVo extends BaseXmlVo{

    String mediaId;

    public ImgVoiceMsgVo(String fromUserName, String toUserName, String createTime) {
        super(fromUserName, toUserName, createTime);
    }

    public ImgVoiceMsgVo(String fromUserName, String toUserName, String createTime,String mediaId) {
        super(fromUserName, toUserName, createTime);
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
