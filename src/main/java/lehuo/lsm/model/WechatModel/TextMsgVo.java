package lehuo.lsm.model.WechatModel;

/**
 * Created by simon on 14/12/24.
 */
public class TextMsgVo extends BaseXmlVo {

    String content;

    public TextMsgVo(String fromUserName, String toUserName, String createTime) {
        super(fromUserName, toUserName, createTime);
    }

    public TextMsgVo(String fromUserName, String toUserName, String createTime,String content) {
        super(fromUserName, toUserName, createTime);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
