package lehuo.lsm.model.WechatModel.db;

/**
 * Created by simon on 15/1/11.
 */
public class BzHuodong {
    Integer id;
    String url;
    String imgurl;


    public BzHuodong(Integer id) {
        this.id = id;
    }

    public BzHuodong(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
}
