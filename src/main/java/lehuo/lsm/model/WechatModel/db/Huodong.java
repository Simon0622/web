package lehuo.lsm.model.WechatModel.db;

/**
 * Created by simon on 14/12/27.
 */
public class Huodong {
    Integer id;
    String hdname;
    String userfromid;
    String activitycontent;
    Double latitude;
    Double longitude;
    String extra;
    String lianxi;
    String maidan;
    String createtime;
    String address;
    String hdewm;

    String nickname;
    String headimgurl;
    String imgurl;

    public Huodong(){}

    public Huodong(Integer id){
        this.id = id;
    }

    public Huodong(String userfromid){
        this.userfromid = userfromid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHdname() {
        return hdname;
    }

    public void setHdname(String hdname) {
        this.hdname = hdname;
    }

    public String getUserfromid() {
        return userfromid;
    }

    public void setUserfromid(String userfromid) {
        this.userfromid = userfromid;
    }

    public String getActivitycontent() {
        return activitycontent;
    }

    public void setActivitycontent(String activitycontent) {
        this.activitycontent = activitycontent;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getLianxi() {
        return lianxi;
    }

    public void setLianxi(String lianxi) {
        this.lianxi = lianxi;
    }

    public String getMaidan() {
        return maidan;
    }

    public void setMaidan(String maidan) {
        this.maidan = maidan;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHdewm() {
        return hdewm;
    }

    public void setHdewm(String hdewm) {
        this.hdewm = hdewm;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
}
