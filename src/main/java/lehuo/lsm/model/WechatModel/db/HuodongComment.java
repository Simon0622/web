package lehuo.lsm.model.WechatModel.db;

/**
 * Created by simon on 15/1/2.
 */
public class HuodongComment extends WexinComment{

    String nickname;
    String headimgurl;

    String userfromid;

    Integer pageId;

    public HuodongComment(){}

    public HuodongComment(String fromid){
        this.fromid = fromid;
    }

    public HuodongComment(Integer id){
        this.id = id;
    }



    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getUserfromid() {
        return userfromid;
    }

    public void setUserfromid(String userfromid) {
        this.userfromid = userfromid;
    }

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

}
