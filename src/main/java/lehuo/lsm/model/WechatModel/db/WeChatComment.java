package lehuo.lsm.model.WechatModel.db;

import java.util.Date;

/**
 * Created by simon on 15/1/2.
 */
public class WeChatComment extends WexinComment{

    String nickname;
    String headimgurl;

    String pagefromid;

    String userfromid;

    Date createtime;

    public WeChatComment(){}

    public WeChatComment(Integer id){
        this.id=id;
    }

    public WeChatComment(String fromid){
        this.fromid = fromid;
    }



    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getUserfromid() {
        return userfromid;
    }

    public void setUserfromid(String userfromid) {
        this.userfromid = userfromid;
    }

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getCreatetime() {
		return createtime;
	}

    public String getPagefromid() {
        return pagefromid;
    }

    public void setPagefromid(String pagefromid) {
        this.pagefromid = pagefromid;
    }
}
