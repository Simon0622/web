package lehuo.lsm.model.WechatModel.db;

/**
 * Created by simon on 14/12/24.
 */
public class WeChatImage {
    Integer id;
    String userfromid;
    String imgurl;
    String msgid;
    String mediaid;
    Integer click;
    Integer score;
    Integer scorenum;
    Integer imgtype;

    public WeChatImage(){}

    public WeChatImage(Integer id){
        this.id = id;
    }

    public WeChatImage(String userfromid){
        this.userfromid = userfromid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserfromid() {
        return userfromid;
    }

    public void setUserfromid(String userfromid) {
        this.userfromid = userfromid;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getMediaid() {
        return mediaid;
    }

    public void setMediaid(String mediaid) {
        this.mediaid = mediaid;
    }

    public Integer getClick() {
        return click;
    }

    public void setClick(Integer click) {
        this.click = click;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getScorenum() {
        return scorenum;
    }

    public void setScorenum(Integer scorenum) {
        this.scorenum = scorenum;
    }

    public Integer getImgtype() {
        return imgtype;
    }

    public void setImgtype(Integer imgtype) {
        this.imgtype = imgtype;
    }
}
