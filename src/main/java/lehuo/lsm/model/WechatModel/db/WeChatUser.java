package lehuo.lsm.model.WechatModel.db;

import java.util.Date;
import java.util.List;

import lehuo.lsm.util.DateConverter;

/**
 * Created by simon on 14/12/24.
 */
public class WeChatUser {
    Integer id;
    String fromid;
    String birth;
    Date birthday;
    String career;
    String city;
    String height;
    String nickname;
    String sex;
    String brief;
    String headimgurl;
    String province;
    String country;

    Double latitude;
    Double longitude;

    List<WeChatImage> weChatImageList;
    Integer recnum;
    Integer viewnum;

    Integer candate;

    public WeChatUser(){}

    public WeChatUser(Integer id){
        this.id = id;
    }

    public WeChatUser(String fromid){
        this.fromid = fromid;
    }

    public WeChatUser(String fromid,String birth,String career,String city,String height){
        this.fromid = fromid;
        setBirth(birth);
        this.career = career;
        this.city = city;
        this.height = height;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromid() {
        return fromid;
    }

    public void setFromid(String fromid) {
        this.fromid = fromid;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
        setBirthday(DateConverter.convertFromString(birth));
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<WeChatImage> getWeChatImageList() {
        return weChatImageList;
    }

    public void setWeChatImageList(List<WeChatImage> weChatImageList) {
        this.weChatImageList = weChatImageList;
    }

    public Integer getRecnum() {
        return recnum;
    }

    public void setRecnum(Integer recnum) {
        this.recnum = recnum;
    }

    public Integer getViewnum() {
        return viewnum;
    }

    public void setViewnum(Integer viewnum) {
        this.viewnum = viewnum;
    }

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getBirthday() {
		return birthday;
	}

    public Integer getCandate() {
        return candate;
    }

    public void setCandate(Integer candate) {
        this.candate = candate;
    }
}
