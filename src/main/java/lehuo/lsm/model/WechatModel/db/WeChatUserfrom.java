package lehuo.lsm.model.WechatModel.db;

/**
 * Created with IntelliJ IDEA.
 * User: shumeng.liu
 * Date: 15-1-5
 * Time: 下午2:11
 * To change this template use File | Settings | File Templates.
 */
public class WeChatUserfrom {
    Integer id;
    String fromid;
    String eventkey;
    String createtime;

    public WeChatUserfrom(){}

    public WeChatUserfrom(String fromid){
        this.fromid = fromid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromid() {
        return fromid;
    }

    public void setFromid(String fromid) {
        this.fromid = fromid;
    }

    public String getEventkey() {
        return eventkey;
    }

    public void setEventkey(String eventkey) {
        this.eventkey = eventkey;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
