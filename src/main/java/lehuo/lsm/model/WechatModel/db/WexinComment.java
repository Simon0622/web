package lehuo.lsm.model.WechatModel.db;

/**
 * Created with IntelliJ IDEA.
 * User: shumeng.liu
 * Date: 15-2-27
 * Time: 下午2:42
 * To change this template use File | Settings | File Templates.
 */
public class WexinComment {
    Integer id;
    String content;
    String fromid;
    String toid;
    Integer isread;



    Integer replyId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFromid() {
        return fromid;
    }

    public void setFromid(String fromid) {
        this.fromid = fromid;
    }

    public String getToid() {
        return toid;
    }

    public void setToid(String toid) {
        this.toid = toid;
    }

    public Integer getIsread() {
        return isread;
    }

    public void setIsread(Integer isread) {
        this.isread = isread;
    }



    public Integer getReplyId() {
        return replyId;
    }

    public void setReplyId(Integer replyId) {
        this.replyId = replyId;
    }
}
