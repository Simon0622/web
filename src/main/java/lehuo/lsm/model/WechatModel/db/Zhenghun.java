/********************************************************************
 * File Name:    Zhenghun.java
 *
 * Date Created: May 10, 2015
 *
 *
 *******************************************************************/

// PACKAGE/IMPORTS --------------------------------------------------
package lehuo.lsm.model.WechatModel.db;

/**
 * TODO: Update with a detailed description of the interface/class.
 *
 */
public class Zhenghun
{
  // CONSTANTS ------------------------------------------------------

  // CLASS VARIABLES ------------------------------------------------

  // INSTANCE VARIABLES ---------------------------------------------

  Integer id;
  String title;
  String url;
  String imgurl;
  String num;

  // CONSTRUCTORS ---------------------------------------------------

  public Zhenghun()
  {
  }

  public Zhenghun(Integer id)
  {
    this.id = id;
  }

  public Zhenghun(String num)
  {
    this.num = num;
  }

  // PUBLIC METHODS -------------------------------------------------
  public Integer getId()
  {
    return id;
  }

  public void setId(Integer id)
  {
    this.id = id;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public String getImgurl()
  {
    return imgurl;
  }

  public void setImgurl(String imgurl)
  {
    this.imgurl = imgurl;
  }

  public String getNum()
  {
    return num;
  }

  public void setNum(String num)
  {
    this.num = num;
  }

  // PROTECTED METHODS ----------------------------------------------

  // PRIVATE METHODS ------------------------------------------------

  // ACCESSOR METHODS -----------------------------------------------

}
