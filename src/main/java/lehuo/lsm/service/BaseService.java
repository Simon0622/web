package lehuo.lsm.service;


import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.dao.UserDao;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
public abstract class BaseService<T>{

    public abstract CommonCRUD<T> getDao();

    public void insert(T t){
        getDao().insert(t);
    }

    public void delete(T t){
        getDao().delete(t);
    }

    public void update(T update,T item){
        getDao().update(update,item);
    }

    public T select(T t){return getDao().select(t);}

    public List<T> selectList(T t,Integer offset,Integer limit){
        if(offset==null){
            offset = 0;
        }

        if(limit==null){
            limit = 1000;
        }
        return getDao().selectList(t,offset,limit);
    }


    public Integer selectCount(T t){ return getDao().selectCount(t);}

    public void batchInsert(List<T> list){getDao().batchInsert(list);}
}
