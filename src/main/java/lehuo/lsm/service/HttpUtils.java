package lehuo.lsm.service;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 14/12/27.
 */
public class HttpUtils {

    protected static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    public static String httpsCon(String aturl){
    	HttpsURLConnection httpsConn = null;
		try {
			URL reqURL = new URL(aturl);
			httpsConn = (HttpsURLConnection) reqURL.openConnection();
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpsConn.getInputStream(), "utf-8"));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			rd.close();
			return sb.toString();
		} catch (Exception e) {
			logger.info("https error", e);
			return "";
		} finally {
			if (httpsConn != null)
				httpsConn.disconnect();
		}
	}

    public static String sendPost(String url, String params) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            //out = new PrintWriter(conn.getOutputStream());
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "utf-8")));
            // 发送请求参数
            out.print(params);
            // flush输出流的缓冲
            out.flush();

            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += "\n" + line;
            }
        } catch (Exception e) {
            System.out.println("发送POST请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        String loginurl="http://www.newsmth.net/nForum/user/ajax_login.json";
        //POST的URL
        /*HttpPost httppost=new HttpPost(url);
        httppost.addHeader("cookie","tmc=2.88525828.78576836.1422455916483.1422455916483.1422455958882; tma=88525828.62951914.1422070425659.1422280304238.1422455916495.4; tmd=21.88525828.62951914.1422070425659.; Hm_lvt_9c7f4d9b7c00cb5aba2c637c64a41567=1422070425; Hm_lpvt_9c7f4d9b7c00cb5aba2c637c64a41567=1422455959; bfd_session_id=bfd_g=b56c782bcb75035d000044dd00082f91548273f7&bfd_s=88525828.22173331.1422455916477; main[UTMPUSERID]=lsm0622; main[UTMPKEY]=65661936; main[UTMPNUM]=4924; main[PASSWORD]=P%255D%252CD%2512%255C%250F%2503%2519%2518BWn%2516gv%2501ZD%253F%252Ce_X; main[XWJOKE]=hoho");
        httppost.addHeader("host","www.newsmth.net");
        //建立HttpPost对象
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        //建立一个NameValuePair数组，用于存储欲传送的参数
        params.add(new BasicNameValuePair("id","2248873"));
        params.add(new BasicNameValuePair("subject","Re%3A%20%E4%BB%A3%E7%BE%8E%E4%B8%BD%E3%80%81%E8%81%AA%E6%98%8E%E3%80%81%E5%96%84%E8%89%AF%E3%80%81%E9%87%8D%E6%84%9F%E6%83%85%E7%9A%84%E9%9D%A0%E8%B0%B1%E5%8C%97%E4%BA%ACMM%E5%BE%81%E5%8F%8B"));
        params.add(new BasicNameValuePair("content","haibucuo"));
        //添加参数
        httppost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
        //设置编码
        HttpResponse response=new DefaultHttpClient().execute(httppost);
        //发送Post,并返回一个HttpResponse对象
        //Header header = response.getFirstHeader("Content-Length");
        //String Length=header.getValue();
        // 上面两行可以得到指定的Header
        if(response.getStatusLine().getStatusCode()==200){//如果状态码为200,就是正常返回
            String result= EntityUtils.toString(response.getEntity());
            //得到返回的字符串
            System.out.println(result);
            //打印输出
            //如果是下载文件,可以用response.getEntity().getContent()返回InputStream
        }*/

        String id = "lsm0622";
        String passwd = "870622";
        HttpClient httpClient = new DefaultHttpClient();

        HttpPost httpPost = new HttpPost(loginurl);
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        httpPost.setHeader("X-Requested-With","XMLHttpRequest");
        httpPost.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36");
        //建立一个NameValuePair数组，用于存储欲传送的参数
        params.add(new BasicNameValuePair("id",id));
        params.add(new BasicNameValuePair("passwd",passwd));
        //添加参数
        httpPost.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
        HttpResponse response=httpClient.execute(httpPost);
        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println(statusCode);

        String body=EntityUtils.toString(response.getEntity());
        System.out.println(body);

        HttpPost httpPost1 = new HttpPost("http://www.newsmth.net/nForum/article/ITExpress/ajax_post.json");
        httpPost1.setHeader("X-Requested-With","XMLHttpRequest");
        httpPost1.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36");
        String sid="1545175";
        String subject = "Re: 一个司长就能把中国首富拉下神坛";
        String content = "我勒个去....";


        List<NameValuePair> pairs=new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("id",sid));
        pairs.add(new BasicNameValuePair("subject",subject));
        pairs.add(new BasicNameValuePair("content",content));
        httpPost1.setEntity(new UrlEncodedFormEntity(pairs,HTTP.UTF_8));
        HttpResponse response1=httpClient.execute(httpPost1);
        int statusCode1 = response1.getStatusLine().getStatusCode();
        System.out.println(statusCode1);

        String body1=EntityUtils.toString(response1.getEntity());
        System.out.println(body1);

    }


}
