package lehuo.lsm.service.impl;

import lehuo.lsm.dao.ActivityCommentDao;
import lehuo.lsm.dao.CommentDao;
import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.model.ActivityComment;
import lehuo.lsm.model.Comment;
import lehuo.lsm.service.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class ActivityCommentService extends BaseService<ActivityComment>{

    @Resource
    ActivityCommentDao dao;

    @Override
    public CommonCRUD<ActivityComment> getDao() {
        return dao;
    }
}
