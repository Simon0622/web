package lehuo.lsm.service.impl;

import lehuo.lsm.dao.ActivityDao;
import lehuo.lsm.dao.CommentDao;
import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.model.Activity;
import lehuo.lsm.model.Comment;
import lehuo.lsm.service.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class ActivityService extends BaseService<Activity>{

    @Resource
    ActivityDao dao;

    @Override
    public CommonCRUD<Activity> getDao() {
        return dao;
    }

    public List<Activity> simpleQuery(String keyword,String querytype,String hasloc,
                                      Integer offset,Integer limit){
        return dao.simpleQuery(keyword,querytype,hasloc,offset,limit);
    }

    /*public List<Activity> likeQuery(String keyword,Integer offset,Integer limit){
        return dao.likeQuery(keyword,offset,limit);
    }*/
}
