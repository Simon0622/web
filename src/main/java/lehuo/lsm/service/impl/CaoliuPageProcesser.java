package lehuo.lsm.service.impl;

import com.ning.http.client.ProxyServer;
import lehuo.lsm.dao.CaoliuDao;
import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.model.CaoliuVo;
import lehuo.lsm.util.Proxy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.LsmSpider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CaoliuPageProcesser implements PageProcessor {

    protected static Logger logger = LoggerFactory.getLogger(CaoliuPageProcesser.class);
    public static ClassPathXmlApplicationContext context;
    private Site site = Site.me().setDomain("dzdz.tv")
            .addStartUrl("http://184.154.178.130/thread0806.php?fid=15&search=&page=1").setSleepTime(3000)
            /*.addHeader("Cookie","CNZZDATA950900=cnzz_eid%3D121532405-1412608782-%26ntime%3D1412671135; 227c9_lastfid=2; 227c9_ck_info=%2F%09; 227c9_winduser=BQhXAwE6BVUGBlxRV1EOBVBRUAYFClNRUQFcA1BRVgILU1BUUAg%3D; 227c9_groupid=8; 227c9_lastvisit=0%091412671611%09%2Fthread0806.php%3Ffid%3D2%26search%3D%26page%3D101")
            .addHeader("Host","caoliu2014.com")*/
            .addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.2; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0")
            .addHeader("Connection","keep-alive")
            /*.addHeader("Host","caoliu2014.com")
            .addCookie("227c9_lastfid","2")
            .addCookie("expires","Wed, 07-Oct-2015 08:47:03 GMT")
            .addCookie("path","/")
            .addCookie("227c9_lastvisit","0%091412671623%09%2Fthread0806.php%3Ffid%3D2%26search%3D%26page%3D110")*/
            ;

    Spider lsm;
    @Resource
    private CaoliuDao dao;

    public CaoliuDao getCaoliuDao(){
        if(dao!=null){
            return dao;
        }else{
            return (CaoliuDao)context.getBean("caoliuDao");
        }
    }

    @Override
    public void process(Page page) {
        Html html = page.getHtml();
        //List<String> tlinks = html.xpath("//div[@class='pages']/a[@style='font-weight:bold']/@href").all();
        String pagenum = html.xpath("//div[@class='pages']/b/text()").get();
        pagenum=pagenum.trim();
        //page.addTargetRequest(tlinks.get(1));

        String s = html.xpath("//tr[@class='tr3 t_one']/td[@style='text-align:left;padding-left:8px']/h3/a/text()").get();

            List<String> titles = html.xpath("//tr[@class='tr3 t_one']/td[@style='text-align:left;padding-left:8px']/h3/a/text()").all();
            List<String> links = html.xpath("//tr[@class='tr3 t_one']/td[@style='text-align:left;padding-left:8px']/h3/a/@href").all();
            List<String> reply = html.xpath("//tr[@class='tr3 t_one']/td[@class='tal f10 y-style']/text()").all();
            List<CaoliuVo> result = new ArrayList<CaoliuVo>();

            if(titles.size()==links.size()){
                for(int i=0;i<titles.size();i++){
                    if(titles.get(i)==null||titles.get(i).equals(""))
                        continue;
                    try{
                        if(i>reply.size()){
                            result.add(new CaoliuVo(titles.get(i),links.get(i),0,Integer.parseInt(pagenum+600)));
                        }else{
                            result.add(new CaoliuVo(titles.get(i),links.get(i),Integer.parseInt(reply.get(i)),Integer.parseInt(pagenum+600)));
                        }

                    }catch (Exception e){
                        continue;
                    }

                }
            }
        getCaoliuDao().batchInsertCL(result);

        }
        /*page.putField("content", page.gretHtml().$("div.content").toString());
        page.putField("tags",page.getHtml().xpath("//div[@class='BlogTags']/a/text()").all());*/

    @Override
    public Site getSite() {
        return site;

    }

    public void action(String starturl){
        context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        String requests;

        int PAGESIZE = 305;
        Request[] urls = new Request[PAGESIZE];
        for(int i=0;i<PAGESIZE;i++){
            urls[i] = new Request("http://184.154.178.130/thread0806.php?fid=15&search=&page="+(i+1));
        }

        Proxy.init();
        lsm = Spider.create(new CaoliuPageProcesser());

        lsm.pipeline(new ConsolePipeline()).addRequest(urls).thread(5).runAsync();

        logger.info("lsm.getSchedulerSize() is {}",lsm.getSchedulerSize());
        while(!lsm.getSpiderExit()){
            try {
                Thread.sleep(3000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("not exit");
        }
        logger.info("caoliu stop begin");
        lsm.stop();
        logger.info("caoliu stop success");
        return;
    }

    public void stopSpider(){
        lsm.setSpiderexit(true);
    }

    public void addUrl(List<String> list,Page page){
        logger.info("add target links size :{}",list.size());
        page.addTargetRequests(list);
    }

    public void addUrl(String s,Page page){
        logger.info("add target {}",s);
        page.addTargetRequest(s);
    }

    public static void main(String[] args){
        CaoliuPageProcesser caoliuPageProcesser = new CaoliuPageProcesser();
        caoliuPageProcesser.action(null);
    }
}