package lehuo.lsm.service.impl;

import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.dao.PkImgDao;
import lehuo.lsm.model.DoubanVo;
import lehuo.lsm.model.Links;
import lehuo.lsm.util.Proxy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.LsmSpider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 14/11/10.
 */
@Service
public class DoubanProcesser implements PageProcessor {

    protected static Logger logger = LoggerFactory.getLogger(DoubanProcesser.class);
    public static ClassPathXmlApplicationContext context;
    private Site site = Site.me().setDomain("www.douban.com")
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0")

            ;
    Spider lsm;
    @Resource
    LinksDao linksDao;

    public LinksDao getLinksDao(){
        if(linksDao!=null){
            return linksDao;
        }else{
            return (LinksDao)context.getBean("linksDao");
        }
    }

    @Override
    public void process(Page page) {

        logger.info("process start : {}", page.getUrl().get());

        Html html = page.getHtml();

        if(page.getUrl().regex("http://www.douban.com/group/topic/\\d+").get()!=null){
            //TODO
            logger.info("link is "+page.getUrl().get());
            String title = html.xpath("//div[@id='content']/h1/text()").get();
            String authorlink = html.xpath("//div[@class='user-face']/a/@href").get();
            String author = html.xpath("//div[@class='topic-doc']/h3/span[@class='from']/a/text()").get();
            String posttime = html.xpath("//span[@class='color-green']/text()").get();
            if(posttime==null){
                posttime="";
            }
            String content = html.xpath("//div[@class='topic-content']/p/text()").get();
            List<String> imgLinks = html.xpath("//div[@id='link-report']/div[@class='topic-content']//img/@src").all();
            DoubanVo vo = new DoubanVo(title,author,authorlink,content,posttime,imgLinks);

            logger.info("result is {}",vo);

            Links lvo = new Links(vo);
            try{
                getLinksDao().update(lvo,new Links(page.getUrl().toString()));
            }catch(Exception e){
                lvo.setContent("");
                getLinksDao().update(lvo,new Links(page.getUrl().toString()));
            }

            logger.info("detail page update success");
        }else{

            List<String> tlinks = html.xpath("//table[@class='olt']/tbody/tr[@class=' ']/td[@class='title']/a/@href").all();
            List<String> reply = html.xpath("//table[@class='olt']/tbody/tr[@class=' ']/td[@class=' ']/text()").all();
            String next = html.xpath("//span[@class='next']/a/@href").get();
            logger.info("next is {}",next);
            String current = html.xpath("//span[@class='thispage']/text()").get();
            logger.info("target links size :{}"+tlinks.size());

            if(tlinks!=null&&tlinks.size()>0){
                addUrl(tlinks,page);
            }

            if(next!=null&&!next.equals("")){
                addUrl(next,page);
            }else{
                addUrl(page.getUrl().toString(),page);
            }

            List<Links> result = new ArrayList<Links>();
            for(int i=0;i<reply.size();i++){
                Links l = new Links();
                l.setLink(tlinks.get(i));
                try{
                    l.setReply(Integer.parseInt(reply.get(i)));
                }catch(Exception e){
                    l.setReply(0);
                }

                l.setDomains("http://www.douban.com/haixiu");
                try{
                    l.setPageno(Integer.parseInt(current));
                }catch(Exception e){
                    l.setPageno(-1);
                }

                l.setExtra("");
                result.add(l);
            }
            getLinksDao().batchInsert(result);
            logger.info("navigation page load success");
        }

    }

    @Override
    public Site getSite() {
        return site;
    }

    public void action(String starturl){
        context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        String requests;
        if(StringUtils.isBlank(starturl)){
            requests = "http://www.douban.com/group/haixiuzu/discussion?start=0";
        }else{
            requests = starturl;
        }

        List<String> lastTimeNoUpdate = getLinksDao().selectNeedReload("douban");

        lastTimeNoUpdate.add(requests);

        String[] urls = new String[lastTimeNoUpdate.size()];

        urls = lastTimeNoUpdate.toArray(urls);

        Proxy.init();
        lsm = Spider.create(new DoubanProcesser());

        lsm.pipeline(new ConsolePipeline()).setProxys(Proxy.addSynProxyServers()).addUrl(urls).thread(5).runAsync();

        logger.info("lsm.getSchedulerSize() is {}",lsm.getSchedulerSize());
        while(!lsm.getSpiderExit()){
            try {
                Thread.sleep(3000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("not exit");
        }
        logger.info("douban stop begin");
        lsm.stop();
        logger.info("douban stop success");
        return;
    }

    public void stopSpider(){
        lsm.setSpiderexit(!lsm.getSpiderExit());
    }

    public void addUrl(List<String> list,Page page){
        logger.info("add target links size :{}",list.size());
        page.addTargetRequests(list);
    }

    public void addUrl(String s,Page page){
        logger.info("add target {}",s);
        page.addTargetRequest(s);
    }

    public static void main(String[] args){
        DoubanProcesser doubanProcesser = new DoubanProcesser();
        doubanProcesser.action(null);
    }


}
