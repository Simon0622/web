package lehuo.lsm.service.impl;

import lehuo.lsm.dao.ActivityDao;
import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.dao.HuodongDao;
import lehuo.lsm.model.Activity;
import lehuo.lsm.model.WechatModel.db.Huodong;
import lehuo.lsm.service.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class HuodongService extends BaseService<Huodong>{

    @Resource
    HuodongDao dao;

    @Override
    public CommonCRUD<Huodong> getDao() {
        return dao;
    }

    public List<Huodong> selectHuodongList(Huodong h,Integer offset,Integer limit){
        return dao.selectHuodongList(h,offset,limit);
    }

    public Huodong selectUserHuodong(Huodong h){
        return dao.selectUserHuodong(h);
    }

    public void updatehwm(Huodong update,Huodong item){
        dao.updatehwm(update,item);
    }
}
