package lehuo.lsm.service.impl;

import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.dao.ImagesDao;
import lehuo.lsm.model.Images;
import lehuo.lsm.model.User;
import lehuo.lsm.service.BaseService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class ImagesService extends BaseService<Images>{

    @Resource
    ImagesDao imagesDao;


    @Transactional
    public List<Images> selectRanklist(Integer offset,Integer pagesize,Integer catagory){
        return imagesDao.selectRanklist(offset,pagesize,catagory);
    }

    public List<Images> selectNotJudged(Integer offset,Integer limit){
        if(offset==null){
            offset = 0;
        }
        if(limit ==null){
            limit = 30;
        }
        return imagesDao.selectNotJudged(offset,limit);
    }

    @Override
    public CommonCRUD<Images> getDao() {
        return imagesDao;
    }

    public void updateImgWin(Integer imgid){
        imagesDao.updateImgWin(imgid);
    }

    public void updateImgLose(Integer imgid){
        imagesDao.updateImgLose(imgid);
    }

    public void updateImgStore(Integer imgid,Integer score){
        imagesDao.updateImgStore(imgid,score);
    }

    public List<Images> rankbypk(User user,Integer offset,Integer pagesize){
        return imagesDao.rankbypk(user,offset,pagesize);
    }

    public List<Images> rankbyscore(User user,Integer offset,Integer pagesize){
        return imagesDao.rankbyscore(user,offset,pagesize);
    }

    public List<Images> rankbyvisitnum(Integer offset,Integer pagesize){
        return imagesDao.rankbyvisitnum(offset,pagesize);
    }

    public List<Images> userrankbysome(Integer rankby,Integer offset,Integer pagesize){
        return imagesDao.userrankbysome(rankby,offset,pagesize);
    }

}
