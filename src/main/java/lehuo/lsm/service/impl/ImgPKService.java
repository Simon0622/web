package lehuo.lsm.service.impl;

import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.dao.ImagesDao;
import lehuo.lsm.dao.PkImgDao;
import lehuo.lsm.model.Images;
import lehuo.lsm.model.PkImg;
import lehuo.lsm.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class ImgPKService extends BaseService<PkImg>{

    protected static Logger logger = LoggerFactory.getLogger(ImgPKService.class);
    @Resource
    PkImgDao pkImgDao;

    @Resource
    private ImagesDao imagesDao;

    @Transactional
    public void insertPKSubmit(String sessionid,List<PkImg> list){
        try{

            for(PkImg pkimg:list){
                //pkimg.setUserid();
            }

            pkImgDao.batchInsert(list);
        }catch (Exception e){
            logger.error("insertPKSubmit error",e);
        }
    }

    public List<Images> selectLownumPkImg(Integer offset,Integer num){
        return imagesDao.selectLowpknumImg(offset,num);
    }

    @Override
    public CommonCRUD<PkImg> getDao() {
        return pkImgDao;
    }
}
