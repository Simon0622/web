package lehuo.lsm.service.impl;

import lehuo.lsm.dao.*;
import lehuo.lsm.model.*;
import lehuo.lsm.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class ImgScoreService extends BaseService<ImgScore> {

    protected static Logger logger = LoggerFactory.getLogger(ImgScoreService.class);
    @Resource
    ImgScoreDao dao;

    @Override
    public CommonCRUD getDao() {
        return dao;
    }

}
