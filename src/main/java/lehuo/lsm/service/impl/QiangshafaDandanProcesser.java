package lehuo.lsm.service.impl;

import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.model.Links;
import lehuo.lsm.util.Proxy;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by simonliu on 2014/10/21.
 */
@Service
public class QiangshafaDandanProcesser implements PageProcessor {

    public Logger logger = LoggerFactory.getLogger(getClass());

    private Site site = Site.me().setDomain("www.oiegg.com")
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0")
            ;

    private List<Links> result = new ArrayList<Links>();

    Spider lsm;

    public static ClassPathXmlApplicationContext context;

    private static HttpClient httpClient;

    private static Map<String,String> idmap = new HashMap<String,String>();

    @Resource
    private LinksDao linksDao;

    public LinksDao getLinksDao(){
        if(linksDao!=null){
            return linksDao;
        }else{
            return (LinksDao)context.getBean("linksDao");
        }
    }

    @Override
    public void process(Page page) {
        logger.info("process start : {}",page.getUrl().get());

        Html html = page.getHtml();

        addUrl(page.getUrl().get(), page);

        List<String> trs = html.xpath("//table[@class='board-list']//tr/outerHtml()").all();

        for(String tr:trs){
            Html trhtml = new Html("<table>"+tr+"</table>");
            String top = trhtml.xpath("//tr[@class='top']/html()").get();

            if(!StringUtils.isBlank(top)){
                continue;
            }else{
                String title = trhtml.xpath("//td[@class='title_9']/a/text()").get();
                String count = trhtml.xpath("//td[7]/text()").get();
                String titlehref = trhtml.xpath("//td[@class='title_9']/a/@href").get();

                if(StringUtils.isBlank(title)){
                    continue;
                }
                String[] args = titlehref.split("/");
                String id = args[args.length-1];
                String broad = args[args.length-2];
                String url = "http://www.newsmth.net/nForum/article/"+broad+"/ajax_post.json";
                try{
                    if(!StringUtils.isBlank(count)){
                        Integer c = Integer.parseInt(count);
                        if(c<=5){
                            Thread.sleep(15*1000);
                            if(idmap.containsKey(id)){
                                System.out.println("重复了，"+id);
                                continue;
                            }else{
                                idmap.put(id,"tata");
                            }
                            HttpPost httpPost = qiangshafa(url,id,"Re: "+title,"沙发，本姑娘也为自己代言，欢迎关注签名档");
                            HttpResponse response=httpClient.execute(httpPost);
                            int statusCode = response.getStatusLine().getStatusCode();
                            System.out.println(statusCode);

                            String body= EntityUtils.toString(response.getEntity());
                            System.out.println(body);
                        }
                    }

                }catch (Exception e){
                    logger.error("cast integer error",e);
                }
            }

        }



    }

    public HttpPost qiangshafa(String url,String id,String subject,String content) throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("X-Requested-With","XMLHttpRequest");
        httpPost.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36");

        List<NameValuePair> pairs=new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("id",id));
        pairs.add(new BasicNameValuePair("subject",subject));
        pairs.add(new BasicNameValuePair("content",content));
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
        return httpPost;
    }

    @Override
    public Site getSite() {
        return site;
    }

    public void action(String url) throws IOException {
        context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        String loginurl="http://www.newsmth.net/nForum/user/ajax_login.json";
        String id = "lsm0622";
        String passwd = "870622";
        httpClient = new DefaultHttpClient();

        HttpPost httpPost = new HttpPost(loginurl);
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        httpPost.setHeader("X-Requested-With","XMLHttpRequest");
        httpPost.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36");
        //建立一个NameValuePair数组，用于存储欲传送的参数
        params.add(new BasicNameValuePair("id",id));
        params.add(new BasicNameValuePair("passwd",passwd));
        //添加参数
        httpPost.setEntity(new UrlEncodedFormEntity(params,HTTP.UTF_8));
        HttpResponse response=httpClient.execute(httpPost);
        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println(statusCode);

        String body=EntityUtils.toString(response.getEntity());
        System.out.println(body);

        List<String> list = new ArrayList<String>();

        list.add("http://www.newsmth.net/nForum/board/Age?ajax&p=1");
        list.add("http://www.newsmth.net/nForum/board/PieLove?ajax&p=1");
        list.add("http://www.newsmth.net/nForum/board/Single?ajax&p=1");
        list.add("http://www.newsmth.net/nForum/board/Picture?ajax&p=1");
        list.add("http://www.newsmth.net/nForum/board/Joke?ajax&p=1");
        if(!StringUtils.isBlank(url)){
            list.add(url);
        }

        String[] urls = new String[list.size()];

        urls = list.toArray(urls);


        Proxy.init();
        lsm = Spider.create(new QiangshafaDandanProcesser());

        lsm.pipeline(new ConsolePipeline()).setProxys(Proxy.addSynProxyServers()).addUrl(urls).thread(2).runAsync();

    }


    public void addUrl(List<String> list,Page page){
        logger.info("add target links size :{},targets are {}",list.size(),list.toString());
        page.addTargetRequests(list);
    }

    public void addUrl(String s,Page page){
        logger.info("add target {}",s);
        page.addTargetRequest(s);
    }


    public void stopSpider(){
        lsm.setSpiderexit(true);
    }

    public static void main(String[] args){
        QiangshafaDandanProcesser shuimuProcesser = new QiangshafaDandanProcesser();
        try{
            shuimuProcesser.action(null);
        }catch (Exception e){
            System.out.println(e);
        }


    }
}