package lehuo.lsm.service.impl;

import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.model.Links;
import lehuo.lsm.util.Proxy;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by simonliu on 2014/10/21.
 */
@Service
public class QiangshafaDoubanProcesser implements PageProcessor {

    public Logger logger = LoggerFactory.getLogger(getClass());

    private Site site = Site.me().setDomain("www.douban.com")
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0");

    private List<Links> result = new ArrayList<Links>();

    public static volatile Spider lsm;

    public static ClassPathXmlApplicationContext context;

    private static HttpClient httpClient;

    private static Map<String, String> idmap = new HashMap<String, String>();

    @Resource
    private LinksDao linksDao;

    public LinksDao getLinksDao() {
        if (linksDao != null) {
            return linksDao;
        } else {
            return (LinksDao) context.getBean("linksDao");
        }
    }

    @Override
    public void process(Page page) {
        logger.info("process start : {}", page.getUrl().get());

        Html html = page.getHtml();

        addUrl(page.getUrl().get(), page);

        List<String> trs = html.xpath("//table[@class='olt']//tr/outerHtml()").all();

        for (String tr : trs) {
            Html trhtml = new Html("<table>" + tr + "</table>");
            String count = trhtml.xpath("//td[3]/text()").get();
            String href = trhtml.xpath("//td[@class='title']/a/@href").get();

            if(StringUtils.isBlank(count)){
                continue;
            }else{
                Integer icount;
                try{
                    icount = Integer.parseInt(count);
                }catch (Exception e){
                    continue;
                }

                if(icount<=20){
                    String[] args = href.split("/");
                    String id= args[args.length-1];

                    String url = "http://www.douban.com/group/topic/"+id+"/add_comment";

                    String ck = "rLtV";
                    String rv_comment = "tatashequ，靠谱的微信线下交友公众号\n" +
                            "发起自清华，北师大等高校。现也面向已经工作的高素质人士。\n" +
                            "20天粉丝已过万，还等什么！";

                    try {
                        if(idmap.containsKey(id)){
                            System.out.println("重复了"+id);
                            continue;
                        }else{
                            idmap.put(id,"E");
                        }


                        //httpClient = lsm.getHttpClient();
                        HttpPost httpPost = qiangshafa(url, ck, rv_comment, "0", "加上去");
                        System.out.println("发请求了准备");
                        HttpResponse response=httpClient.execute(httpPost);
                        System.out.println(url);
                        int statusCode = response.getStatusLine().getStatusCode();
                        System.out.println(statusCode);

                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }else {
                    System.out.println("回复太多了");
                }
            }



        }

    }

    public HttpPost qiangshafa(String url, String ck, String rv_comment, String start, String submit_btn) throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Cookie", "bid=\"H2uvfmg4vGc\"; ps=y; ll=\"108288\"; _pk_ref.100001.8cb4=%5B%22%22%2C%22%22%2C1422882005%2C%22https%3A%2F%2Fwww.google.com%2F%22%5D; ap=1; ue=\"lsmsilence@126.com\"; dbcl2=\"50117226:7PX/Rwp1Jag\"; ck=\"rLtV\"; __utmt=1; push_noty_num=0; push_doumail_num=2; _pk_id.100001.8cb4=8c5fdd028216fdc0.1421044865.11.1422882811.1422857050.; _pk_ses.100001.8cb4=*; __utma=30149280.868614914.1421044868.1422855348.1422882005.11; __utmb=30149280.26.10.1422882005; __utmc=30149280; __utmz=30149280.1422602699.9.6.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmv=30149280.5011");
        httpPost.setHeader("X-Requested-With", "XMLHttpRequest");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36");

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        pairs.add(new BasicNameValuePair("ck", ck));
        pairs.add(new BasicNameValuePair("rv_comment", rv_comment));
        pairs.add(new BasicNameValuePair("start", start));
        pairs.add(new BasicNameValuePair("submit_btn", submit_btn));
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, HTTP.UTF_8));
        return httpPost;
    }

    @Override
    public Site getSite() {
        return site;
    }

    public void action(String url) throws IOException {
        context = new ClassPathXmlApplicationContext("spring-servlet.xml");

        List<String> list = new ArrayList<String>();

        list.add("http://www.douban.com/group/beijing/discussion?start=0");
        if (!StringUtils.isBlank(url)) {
            list.add(url);
        }

        String[] urls = new String[list.size()];

        urls = list.toArray(urls);


        Proxy.init();
        lsm = Spider.create(new QiangshafaDoubanProcesser());

        lsm.pipeline(new ConsolePipeline()).setProxys(Proxy.addSynProxyServers()).addUrl(urls).thread(2).runAsync();

    }


    public void addUrl(List<String> list, Page page) {
        logger.info("add target links size :{},targets are {}", list.size(), list.toString());
        page.addTargetRequests(list);
    }

    public void addUrl(String s, Page page) {
        logger.info("add target {}", s);
        page.addTargetRequest(s);
    }


    public void stopSpider() {
        lsm.setSpiderexit(true);
    }

    public static void main(String[] args) {
        QiangshafaDoubanProcesser shuimuProcesser = new QiangshafaDoubanProcesser();
        try {
            shuimuProcesser.action(null);
        } catch (Exception e) {
            System.out.println(e);
        }


    }
}