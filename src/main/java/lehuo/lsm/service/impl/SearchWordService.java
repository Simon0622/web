package lehuo.lsm.service.impl;

import lehuo.lsm.dao.ActivityCommentDao;
import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.dao.SearchWordDao;
import lehuo.lsm.model.ActivityComment;
import lehuo.lsm.model.SearchWord;
import lehuo.lsm.service.BaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by simonliu on 2014/10/26.
 */
@Service
public class SearchWordService extends BaseService<SearchWord>{

    @Resource
    SearchWordDao dao;

    @Override
    public CommonCRUD<SearchWord> getDao() {
        return dao;
    }
}
