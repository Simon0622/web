package lehuo.lsm.service.impl;

import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.model.Links;
import lehuo.lsm.model.ShuimuVo;
import lehuo.lsm.util.Proxy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.LsmSpider;
import us.codecraft.webmagic.pipeline.ConsolePipeline;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simonliu on 2014/10/21.
 */
@Service
public class ShuimuProcesser implements PageProcessor {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    private Site site = Site.me().setDomain("www.newsmth.net")
            .addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0")
            ;

    private List<Links> result = new ArrayList<Links>();

    Spider lsm;

    public static ClassPathXmlApplicationContext context;

    @Resource
    private LinksDao linksDao;

    public LinksDao getLinksDao(){
        if(linksDao!=null){
            return linksDao;
        }else{
            return (LinksDao)context.getBean("linksDao");
        }
    }

    @Override
    public void process(Page page) {
        logger.info("process start : {}",page.getUrl().get());

        Html html = page.getHtml();

        if(page.getUrl().regex("http://www.newsmth.net/nForum/article/*").get()!=null){
            //TODO
            logger.info("link is "+page.getUrl().get());
            String title = html.xpath("//div[@class='b-head corner']/span[@class='n-left']/text()").get();
            String author = html.xpath("//span[@class='a-u-name']/a/text()").get();
            String content = html.xpath("//td[@class='a-content']/p[1]/text()").get();
            List<String> imgLinks = html.xpath("//td[@class='a-content']//img/@src").all();
            ShuimuVo vo = new ShuimuVo(title,author,content,imgLinks);
            logger.info("result is {}",vo);

            Links lvo = new Links(vo);

            getLinksDao().update(lvo,new Links(page.getUrl().toString()));

        }else{
            //添加url
            List<String> tlinks = html.xpath("//table[@class='board-list tiz']/tbody/tr/td[1]/a/@href").all();
            String nextpage = html.xpath("//ol[@class='page-main']//a[@title='下一页']/@href").get();
            tlinks.add(nextpage);
            addUrl(tlinks,page);


            String currentpage = html.xpath("//ol[@class='page-main']//a[@title='当前页']/text()").get();
            List<String> reply = html.xpath("//table[@class='board-list tiz']/tbody/tr/td[7]/text()").all();
            List<Links> LinksList = new ArrayList<Links>();

            if(tlinks.size()==reply.size()+1){
                //多最后一个currentpage
                for(int i=0;i<reply.size();i++){
                    Links v = new Links();
                    v.setLink(tlinks.get(i));
                    v.setReply(Integer.parseInt(reply.get(i)));
                    v.setDomains("http://www.newsmth.net");
                    v.setExtra(currentpage);
                    LinksList.add(v);
                }
            }else if(tlinks.size()!=0&&reply.size()!=0){
                int size = (tlinks.size()-1)<reply.size()?(tlinks.size()-1):reply.size();
                for(int i=0;i<size;i++){
                    Links v = new Links();
                    v.setLink(tlinks.get(i));
                    v.setReply(Integer.parseInt(reply.get(i)));
                    v.setDomains("http://www.newsmth.net");
                    v.setExtra(currentpage);
                    LinksList.add(v);
                }
            }else{

            }

            getLinksDao().batchInsert(LinksList);
            //批量更新完毕
        }

    }

    @Override
    public Site getSite() {
        return site;
    }

    public void action(String url){
        context = new ClassPathXmlApplicationContext("spring-servlet.xml");

        List<String> lastTimeNoUpdate = getLinksDao().selectNeedReload("newsmth");

        lastTimeNoUpdate.add("http://www.newsmth.net/nForum/board/Age?ajax&p=1");
        lastTimeNoUpdate.add("http://www.newsmth.net/nForum/board/MyPhoto?ajax&p=1");
        lastTimeNoUpdate.add("http://www.newsmth.net/nForum/board/PieFriends?ajax&p=1");
        if(!StringUtils.isBlank(url)){
            lastTimeNoUpdate.add(url);
        }

        String[] urls = new String[lastTimeNoUpdate.size()];

        urls = lastTimeNoUpdate.toArray(urls);


        Proxy.init();
        lsm = Spider.create(new ShuimuProcesser());

        lsm.pipeline(new ConsolePipeline()).setProxys(Proxy.addSynProxyServers()).addUrl(urls).thread(5).runAsync();

        logger.info("lsm.getSchedulerSize() is {}",lsm.getSchedulerSize());
        while(!lsm.getSpiderExit()){
            try {
                Thread.sleep(3000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("not exit");
        }
        logger.info("shuimu stop begin");
        lsm.stop();
        logger.info("shuimu stop success");
        return;
    }


    public void addUrl(List<String> list,Page page){
        logger.info("add target links size :{},targets are {}",list.size(),list.toString());
        page.addTargetRequests(list);
    }

    public void addUrl(String s,Page page){
        logger.info("add target {}",s);
        page.addTargetRequest(s);
    }


    public void stopSpider(){
        lsm.setSpiderexit(true);
    }

    public static void main(String[] args){
        ShuimuProcesser shuimuProcesser = new ShuimuProcesser();
        shuimuProcesser.action(null);

    }
}