/********************************************************************
 * File Name:    ZhenghunService.java
 *
 * Date Created: May 11, 2015
 *
 * ------------------------------------------------------------------
 *
 *******************************************************************/

// PACKAGE/IMPORTS --------------------------------------------------
package lehuo.lsm.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import lehuo.lsm.dao.CommonCRUD;
import lehuo.lsm.dao.ZhenghunDao;
import lehuo.lsm.service.BaseService;

/**
 * TODO: Update with a detailed description of the interface/class.
 *
 */
@Service
public class ZhenghunService extends BaseService
{

  @Resource
  ZhenghunDao dao;

  @Override
  public CommonCRUD getDao()
  {
    // TODO Auto-generated method stub
    return dao;
  }
  // CONSTANTS ------------------------------------------------------

  // CLASS VARIABLES ------------------------------------------------

  // INSTANCE VARIABLES ---------------------------------------------

  // CONSTRUCTORS ---------------------------------------------------

  // PUBLIC METHODS -------------------------------------------------

  // PROTECTED METHODS ----------------------------------------------

  // PRIVATE METHODS ------------------------------------------------

  // ACCESSOR METHODS -----------------------------------------------

}
