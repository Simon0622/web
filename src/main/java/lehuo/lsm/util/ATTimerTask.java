package lehuo.lsm.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.TimerTask;

/**
 * Created by simon on 14/12/27.
 */
public class ATTimerTask extends TimerTask {

    protected static Logger logger = LoggerFactory.getLogger(ATTimerTask.class);

    public static volatile String accessToken = "";

    public static volatile String jsapiTicket = "";

    private static String aturl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx75f426630ab9a4fd&secret=81668fc80beb4691b2e6bf80d8459999";

    @Override
    public void run() {
        getAccessToken();
        System.out.println("aaa");
    }

    private void getAccessToken(){
        try{
            String result = HttpUtils.httpsCon(aturl);

            JSONObject jsonObject = JSON.parseObject(result);

            accessToken = jsonObject.getString("access_token");

            String jsResult = HttpUtils.httpsCon("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+accessToken+"&type=jsapi");

            JSONObject jsObject = JSON.parseObject(jsResult);

            jsapiTicket = jsObject.getString("ticket");

            logger.info("accessToken is {},jsapiTicket is {}",accessToken,jsapiTicket);
        }catch (Exception e){

        }

    }
}
