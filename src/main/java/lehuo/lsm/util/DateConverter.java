/**
 * 
 */
package lehuo.lsm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Long
 *
 */
public class DateConverter {
	private static final DateFormat[] ACCEPT_DATE_FORMATS = {
		new SimpleDateFormat("yyyy-MM-dd"),
		new SimpleDateFormat("dd/MM/yyyy"),
		new SimpleDateFormat("yyyy/MM/dd") };
	
	public static Date convertFromString(String value) {
		if (value == null)
			return null;
		for (DateFormat format : ACCEPT_DATE_FORMATS) {
			try {
				return format.parse(value);
			} catch (ParseException e) {
				continue;
			}
		}
		return null;
	}
	
	public static String convertToString(Date d) {
		return d == null ? "" : ACCEPT_DATE_FORMATS[0].format(d);
	}
}
