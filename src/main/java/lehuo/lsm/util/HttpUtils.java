package lehuo.lsm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by simon on 14/12/27.
 */
public class HttpUtils {

    protected static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    public static String httpsCon(String aturl){
    	HttpsURLConnection httpsConn = null;
		try {
			URL reqURL = new URL(aturl);
			httpsConn = (HttpsURLConnection) reqURL.openConnection();
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpsConn.getInputStream(), "utf-8"));
			StringBuffer sb = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			rd.close();
			return sb.toString();
		} catch (Exception e) {
			logger.info("https error", e);
			return "";
		} finally {
			if (httpsConn != null)
				httpsConn.disconnect();
		}
	}

    public static String sendPost(String url, String params) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");

            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            //out = new PrintWriter(conn.getOutputStream());
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "utf-8")));
            // 发送请求参数
            out.print(params);
            // flush输出流的缓冲
            out.flush();

            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += "\n" + line;
            }
        } catch (Exception e) {
            System.out.println("发送POST请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static String sendKFMsg(String toid,String content){
        String result = HttpUtils.sendPost("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + ATTimerTask.accessToken,
                "{\n" +
                        "    \"touser\":\""+toid+"\",\n" +
                        "    \"msgtype\":\"text\",\n" +
                        "    \"text\":\n" +
                        "    {\n" +
                        "         \"content\":\" "+content+" \"}\n" +
                        "}");
        return result;
    }

}
