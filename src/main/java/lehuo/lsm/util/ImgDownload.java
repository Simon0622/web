package lehuo.lsm.util;

import lehuo.lsm.dao.ImagesDao;
import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.model.Images;
import lehuo.lsm.model.Links;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by simonliu on 2014/10/24.
 */
public class ImgDownload {

    protected static String path = PropReadUtil.getProp("imgpath");

    protected static Logger logger = LoggerFactory.getLogger(ImgDownload.class);

    public static void generate(Integer start){

        if(start==null){
            start = 0;
        }

        path = PropReadUtil.getProp("imgpath");

        if(StringUtils.isBlank(path)){
            return;
        }

        List<Images> list = null;

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        ImagesDao imagesDao = (ImagesDao) context.getBean("imagesDao");

        Integer max = imagesDao.selectMax();
        logger.info("ImgDownload max id is {}",max);
        for (int i = start; i < max;) {
            logger.info("ImgDownload i is {}",i);
            list = imagesDao.selectbyoffset(i,5000);

            Runnable runnable = new DownloadThread(list);
            Thread thread = new Thread(runnable);
            logger.info("thread {} will start,start num is {},list.size is {}",thread.getName(),i,list.size());
            i = list.get(list.size()-1).getId();
            thread.start();
        }
        logger.info("ImgDownload complete");
    }

    /**
     * 从输入流中获取数据
     *
     * @param inStream 输入流
     * @return
     * @throws Exception
     */
    public static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }

    /**
     * 将图片写入到磁盘
     *
     * @param img      图片数据流
     * @param fileName 文件保存时的名称
     */
    public static void writeImageToDisk(byte[] img, String fileName) {
        try {
            File file = new File(path + fileName);
            file.createNewFile();
            FileOutputStream fops = new FileOutputStream(file);
            fops.write(img);
            fops.flush();
            fops.close();
            System.out.println("write success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

class DownloadThread implements Runnable{

    protected static String path = PropReadUtil.getProp("imgpath");

    protected static Logger logger = LoggerFactory.getLogger(ImgDownload.class);

    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-servlet.xml");

    ImagesDao imagesDao = (ImagesDao) context.getBean("imagesDao");

    ImagesDao getImagesDao() {
        if(imagesDao==null){
            imagesDao = (ImagesDao) context.getBean("imagesDao");
            return imagesDao;
        }else{
            return imagesDao;
        }
    }

    public DownloadThread(List<Images> list){
        this.list = list;
    }

    void setImagesDao(ImagesDao imagesDao) {
        this.imagesDao = imagesDao;
    }

    List<Images> list = null;

    List<Images> getList() {
        return list;
    }

    void setList(List<Images> list) {
        this.list = list;
    }

    @Override
    public void run() {
        logger.info("current thread is {}",Thread.currentThread());
        download(list);
    }

    public void download(List<Images> list){
        for (Images vo : list) {
            Images image = new Images();
            try{
                String url = vo.getImg_link();
                logger.info("ImgDownload url is {},id is {}",url,vo.getId());
                if (StringUtils.isBlank(url)) {
                    continue;
                } else {
                    String fileName = vo.getId() + ".jpg";
                    if(!needDown(fileName)){
                        logger.info("file exist {}", fileName);
                        continue;
                    }
                    byte[] btImg = getImageFromNetByUrl(url);
                    if (null != btImg && btImg.length > 3000) {
                        logger.info("从{}读取到：" + btImg.length + " 字节", url);

                        writeImageToDisk(btImg, fileName);


                        image.setImgshow(1);

                    } else {
                        logger.info("没有从该连接获得内容,{}", url);
                        image.setImgshow(0);
                    }
                    image.setDeal(1);
                    getImagesDao().update(image, new Images(vo.getId()));

                }
            }catch(Exception e){
                logger.error("error",e);
                image.setImgshow(0);
                getImagesDao().update(image, new Images(vo.getId()));
                continue;
            }

        }
    }

    /**
     * 将图片写入到磁盘
     *
     * @param img      图片数据流
     * @param fileName 文件保存时的名称
     */
    public static void writeImageToDisk(byte[] img, String fileName) {
        try {
            File file = new File(path + fileName);
            file.createNewFile();
            FileOutputStream fops = new FileOutputStream(file);
            fops.write(img);
            fops.flush();
            fops.close();
            System.out.println("write success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据地址获得数据的字节流
     *
     * @param strUrl 网络连接地址
     * @return
     */
    public static byte[] getImageFromNetByUrl(String strUrl) {
        try {
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(10 * 1000);
            InputStream inStream = conn.getInputStream();//通过输入流获取图片数据
            byte[] btImg = readInputStream(inStream);//得到图片的二进制数据
            return btImg;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(strUrl, e);
        }
        return null;
    }

    /**
     * 从输入流中获取数据
     *
     * @param inStream 输入流
     * @return
     * @throws Exception
     */
    public static byte[] readInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        inStream.close();
        return outStream.toByteArray();
    }

    public static boolean needDown(String fileName){
        File file = new File(path + fileName);
        if (!file.exists()) {
            return true;
        } else {
            return false;
        }
    }
}
