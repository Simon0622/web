package lehuo.lsm.util;

import lehuo.lsm.dao.ImagesDao;
import lehuo.lsm.dao.LinksDao;
import lehuo.lsm.model.Images;
import lehuo.lsm.model.Links;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: simonliu
 * Date: 14-11-1
 * Time: 下午10:55
 * To change this template use File | Settings | File Templates.
 */
public class ImgUtils {
    protected static Logger logger = LoggerFactory.getLogger(ImgUtils.class);

    public static void generate(Integer start){
        if(start==null){
            start = 0;
        }
        List<Links> list = null;
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        LinksDao linksDao = (LinksDao) context.getBean("linksDao");
        ImagesDao imagesDao = (ImagesDao) context.getBean("imagesDao");


        Integer max = linksDao.selectMax();

        logger.info("ImgUtils max id is {}",max);
        for(int i=start;i<max;i+=1000){
            logger.info("ImgUtils i is {}",i);
            list = linksDao.selectbyoffset(i);
            logger.info("start i={},list size={}",i,list.size());
            for(Links vo:list){
                try{
                    String url = vo.getImg();
                    try{

                        url = url.substring(1,url.length()-1);
                        linksDao.update(new Links(vo.getId(),1),new Links(vo.getId()));
                    }catch (Exception e){
                        logger.error("imgUtil update links error",e);
                        continue;
                    }

                    if(StringUtils.isBlank(url)){
                        continue;
                    }else{
                        logger.info("url is {}",url);
                        String[] surl = url.split("\\, ");
                        Integer k=0;
                        for(String s:surl){
                            imagesDao.insert(new Images(vo.getId(),s,0));
                        }
                    }
                }catch (Exception e){
                    logger.error("error",e);
                }

            }
        }
        logger.info("imgUtils complete");
    }

    public static void main(String[] args){
        ImgUtils.generate(142792);

    }
}
