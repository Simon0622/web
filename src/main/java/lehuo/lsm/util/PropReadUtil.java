package lehuo.lsm.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by simon on 14/11/17.
 */
public class PropReadUtil {
    private static Properties prop = new Properties();;

    public static String getProp(String s){
        try {
            prop.load(PropReadUtil.class.getClassLoader().getResourceAsStream("config.properties"));
            return prop.getProperty(s);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
