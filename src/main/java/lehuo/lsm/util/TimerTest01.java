package lehuo.lsm.util;

import java.util.Timer;

/**
 * Created by simon on 14/12/27.
 */
public class TimerTest01 {
    Timer timer;
    public TimerTest01(int time){
        timer = new Timer();
        timer.schedule(new ATTimerTask(), time*1000,1*1000);
    }

    public static void main(String[] args) {
        System.out.println("timer begin....");
        new TimerTest01(5);
    }
}
