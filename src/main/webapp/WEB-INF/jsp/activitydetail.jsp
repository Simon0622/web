<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>
<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <div style="width: 100%" id="${activity.id}" data-index="${status.index}">
                <div class="userDiv col-xs-12">
                    <span>${activity.username==""?"匿名用户":activity.username}</span>
                </div>
                <div class="activityDiv col-xs-12">
                    <div style="float:left;margin: 5px">
                        <h3>${activity.activityname}</h3>
                    </div>
                    <div style="float:left;margin-top:28px">
                        <select type="select" name="activitytype" id="activitytype" disabled="disabled">
                            <option value="11" ${activity.activitytype=="11"?'selected':''}>会所</option>
                            <option value="12" ${activity.activitytype=="12"?'selected':''}>酒吧KTV</option>
                            <option value="13" ${activity.activitytype=="14"?'selected':''}>个人发布</option>
                        </select>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.activitycontent}</span>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.address}</span>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.tel}</span>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.qq}</span>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.staffnum}</span>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.renjun}</span>
                    </div>
                    <div style="float:left;width: 80%">
                        <span>${activity.renqi}</span>
                    </div>
                    <span id="location" style="display:none">${activity.location}</span>
                </div>

            </div>
            <div class="comment row">
                <form action="<%=request.getContextPath() %>/activity/addcomment" method="post" id="commentForm">
                    <textarea id="comment" name="comment" class="valtype col-xs-6" >
                    </textarea>
                    <input type="hidden" name="activityId" value="${activity.id}"/>
                    <input type="submit" value="提交"/>
                </form>
            </div>
            <div id="commentshow row">
                <c:forEach var="comment" items="${requestScope.comment}">
                    <div class="commentdiv">

                            ${comment.comment}
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-12" id="allmap" style="height: 400px">

                </div>

            </div>
            <div class="row">
                <c:forEach var="img" items="${requestScope.images}" varStatus="status">
                    <div class="item">
                        <a href="<%=request.getContextPath() %>/images/${img.id}.jpg" class="jqzoom" rel='gal1' title="triumph">
                            <img src="<%=request.getContextPath() %>/images/${img.id}.jpg" class="img-responsive img-rounded"
                                 style="vertical-align: bottom;width: 250px;height: auto;"/>
                        </a>

                    </div>
                </c:forEach>
            </div>
        </div>
    </div>


</div>
<script>
    $(function(){

        $('#commentForm').submit(function() {

            $(this).ajaxSubmit(function(result) {
                /*$("#commentshow").prepend("<div class='commentdiv'>"+result+"</div>");*/
                location.reload(true);
            });
            return false; //阻止表单默认提交

        });
    });
</script>
<script type="text/javascript">
    var map = new BMap.Map("allmap");
    map.enableScrollWheelZoom();
    map.centerAndZoom(new BMap.Point(116.404, 39.915), 11);
    var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
    var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

    map.addControl(top_left_control);
    map.addControl(top_left_navigation);
    //获取点击的经纬度
    var plocation = $('#location').html();
    if(plocation!=""){
        plocation = plocation.split(",");
        var point = new BMap.Point(plocation[0],plocation[1]);
        var marker = new BMap.Marker(point);        // 创建标注
        map.addOverlay(marker);

        map.centerAndZoom(point, 13);
    }


</script>
<%@include file="headerfooter/footer.jsp" %>

