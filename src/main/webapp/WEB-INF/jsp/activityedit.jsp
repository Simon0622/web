<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>
<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <form class="form-horizontal col-xs-6" role="form" action="<%=request.getContextPath()%>/activity/addactivity"
                  method="post">

                <div class="form-group">
                    <label for="activityname" class="col-sm-2 control-label">标题</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="activityname" id="activityname"
                               placeholder="标题" value="${activity.activityname}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="activitytype" class="col-sm-2 control-label">类型</label>

                    <div class="col-sm-4">
                        <select type="select" name="activitytype" id="activitytype" disabled="disabled">
                            <option value="11" ${activity.activitytype=="11"?'selected':''}>会所</option>
                            <option value="12" ${activity.activitytype=="12"?'selected':''}>酒吧KTV</option>
                            <option value="13" ${activity.activitytype=="14"?'selected':''}>个人发布</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="activitycontent" class="col-sm-2 control-label">简介</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="activitycontent" id="activitycontent"
                               placeholder="本店简介" value="${activity.activitycontent}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="location" class="col-sm-6 control-label">本店位置</label>

                    <div class="col-sm-1">
                        <input type="hidden" class="form-control" name="location" id="location" placeholder="">
                    </div>
                </div>

                <input type="hidden" name="extra" id="extra"/>

                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">地址</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="address" id="address"
                                value="${activity.address}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="tel" class="col-sm-2 control-label">电话</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="tel" id="tel"
                               placeholder="联系电话" value="${activity.tel}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="qq" class="col-sm-2 control-label">QQ</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="qq" id="qq"
                                value="${activity.qq}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="staffnum" class="col-sm-2 control-label">人员数量</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="staffnum" id="staffnum"
                                value="${activity.staffnum}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="renjun" class="col-sm-2 control-label">人均消费</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="renjun" id="renjun"
                                value="${activity.renjun}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="renqi" class="col-sm-2 control-label">人气</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="renqi" id="renqi"
                               value="${activity.renqi}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-default">更新</button>
                    </div>
                </div>

            </form>
        </div>
        <div class="col-xs-6" id="allmap" style="height: 450px;">

        </div>
    </div>
    <div class="row">
           <c:forEach var="img" items="${requestScope.images}" varStatus="status">
               <div class="item">
                   <a href="<%=request.getContextPath() %>/images/${img.id}.jpg" class="jqzoom" rel='gal1' title="triumph">
                       <img src="<%=request.getContextPath() %>/images/${img.id}.jpg" class="img-responsive img-rounded"
                            style="vertical-align: bottom;width: 250px;height: auto;"/>
                   </a>

               </div>
           </c:forEach>
    </div>

</div>
<script>
    $(function () {

        var $container = $('#containera');

        $container.imagesLoaded(function () {
            $container.masonry({
                itemSelector: '.item'
            });
        });

        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();
        map.centerAndZoom("北京", 12);
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

        map.addControl(top_left_control);
        map.addControl(top_left_navigation);

        //从数据库中读取点击的经纬度
        var plocation = $('#location').html();
        if(plocation!=""){
            plocation = plocation.split(",");
            var point = new BMap.Point(plocation[0], plocation[1]);
            var marker = new BMap.Marker(point);        // 创建标注
            map.addOverlay(marker);

            map.centerAndZoom(point, 12);

        }else{
            //单击获取点击的经纬度
            map.addEventListener("click", function (e) {
                alert(e.point.lng + "," + e.point.lat);
                var point = new BMap.Point(e.point.lng, e.point.lat);
                map.centerAndZoom(point, 14);
                var marker = new BMap.Marker(point);        // 创建标注
                map.addOverlay(marker);                     // 将标注添加到地图中
                marker.setAnimation(BMAP_ANIMATION_BOUNCE); // 跳动的动画
                $("#location").val(e.point.lng + "," + e.point.lat);
            });
        }




    });
</script>
<%@include file="headerfooter/footer.jsp" %>

