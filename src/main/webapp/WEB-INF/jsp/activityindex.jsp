<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <button class="btn btn-success" type="button"
                    onclick="location.href='<%=request.getContextPath()%>/activity/addacpage'">点我添加
            </button>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <ul>
                        <input name="keyword" id="keyword" placeholder="输入关键字,多个以空格分隔" value="${keyword}" style="width: 200px"/>
                        <button id="search" type="submit">搜索</button>
                        有地图标记<input type="checkbox" checked="true" id="hasloc"/>
                        <input id="currentPage" type="hidden" value="${currentPage}">
                    </ul>
                </div>
            </div>
            <div class="row demo-row">
                <div class="col-xs-6">

                    <div>
                        <ul class="nav nav-tabs" id="activityTab">
                            <li class="${querytype=="11"?'active':''}"><a href="#huisuo" data-type="11">会所</a></li>
                            <li class="${querytype=="12"?'active':''}"><a href="#ktvjb" data-type="12">酒吧KTV</a></li>
                            <li class="${querytype=="13"?'active':''}"><a href="#grfb" data-type="13">个人发布</a></li>
                        </ul>
                        <div class="tab-content" id="activitylist">
                            <div class="tab-pane active" id="huisuo">
                                <c:forEach var="activity" items="${requestScope.list}" varStatus="status">
                                    <div class="item" id="${activity.id}" data-index="${status.index}">
                                        <div class="activityDiv">
                                            <div style="float:left;margin: 5px">
                                                <a href="<%=request.getContextPath() %>/activity/activitydetail?id=${activity.id}">
                                                    <h3>${activity.activityname}</h3></a>
                                                地址:<span>${activity.address}</span>
                                                电话:<span>${activity.tel}</span>
                                            </div>

                                            <span class="mappoint" style="display: none">${activity.location}</span>
                                            <%
                                                if (username != null) {
                                            %>
                                            <a href="<%=request.getContextPath() %>/activity/activityedit?id=${activity.id}">编辑</a>
                                            <%
                                                }
                                            %>
                                        </div>

                                    </div>
                                </c:forEach>
                            </div>
                            <div class="tab-pane" id="ktvjb">
                                <c:forEach var="activity" items="${requestScope.list}" varStatus="status">
                                    <div class="item" id="${activity.id}" data-index="${status.index}">
                                        <div class="activityDiv">
                                            <div style="float:left;margin: 5px">
                                                <a href="<%=request.getContextPath() %>/activity/activitydetail?id=${activity.id}">
                                                    <h3>${activity.activityname}</h3></a>
                                                地址:<span>${activity.address}</span>
                                                电话:<span>${activity.tel}</span>
                                            </div>

                                            <span class="mappoint" style="display: none">${activity.location}</span>
                                            <%
                                                if (username != null) {
                                            %>
                                            <a href="<%=request.getContextPath() %>/activity/activityedit?id=${activity.id}">编辑</a>
                                            <%
                                                }
                                            %>
                                        </div>

                                    </div>
                                </c:forEach>
                            </div>
                            <div class="tab-pane" id="grfb">
                                <c:forEach var="activity" items="${requestScope.list}" varStatus="status">
                                    <div class="item" id="${activity.id}" data-index="${status.index}">
                                        <div class="activityDiv">
                                            <div style="float:left;margin: 5px">
                                                <a href="<%=request.getContextPath() %>/activity/activitydetail?id=${activity.id}">
                                                    <h3>${activity.activityname}</h3></a>
                                                地址:<span>${activity.address}</span>
                                                电话:<span>${activity.tel}</span>
                                            </div>

                                            <span class="mappoint" style="display: none">${activity.location}</span>
                                            <%
                                                if (username != null) {
                                            %>
                                            <a href="<%=request.getContextPath() %>/activity/activityedit?id=${activity.id}">编辑</a>
                                            <%
                                                }
                                            %>
                                        </div>

                                    </div>
                                </c:forEach>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-xs-6" id="allmap" style="height: 450px;">

                </div>

            </div>
        </div>
    </div>
</div>


<div class="catabutton">
    <ul id='bp-3-element-normal-test2'></ul>
</div>
<script>
    var element2 = $('#bp-3-element-normal-test2');



    options = {
        size: "normal",
        bootstrapMajorVersion: 3,
        currentPage: cpage,
        numberOfPages: 5,
        totalPages: 100,
        pageUrl: function (type, page, current) {

            return "<%=request.getContextPath() %>/activity/index?currentPage=" + page +"&querytype="+ querytype +
                    "&hasloc="+ hasloc +"&keyword=" +keyword;

        }
    };

    element2.bootstrapPaginator(options);
</script>
<script>
    $(document).ready(function () {
        var cpage = $('#currentPage').val();
        var querytype = '11';
        var hasloc = 'on';
        var keyword = $('#keyword').val();

        $('#hasloc').change(function(){
            hasloc = $(this).attr('checked');
            if(hasloc=='undefined'){
                hasloc = 'off';
            }
            search();
        });

        $('#activityTab a').click(function (e) {
            e.preventDefault();//阻止a链接的跳转行为
            $(this).tab('show');//显示当前选中的链接及关联的content
            $('#activityTab li').removeClass('active');
            $(this).parent().attr('class','active');
            querytype = $(this).data('type');
            $('#currentPage').val("1");
            searchNew();
        })

        $(".querySelect").on('change',function(){
            $('#currentPage').val("1");
            searchNew();
        });

        $("#search").on('click',function(){
            search();
        });

        function search(){
            $('#currentPage').val("1");
            searchNew();
        }

        function searchNew(){

            window.location="<%=request.getContextPath() %>/activity/index?currentPage=" + cpage +"&querytype="+ querytype +
                    "&hasloc="+ hasloc +"&keyword=" +keyword;
        }

        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();
        map.centerAndZoom("北京", 12);
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

        map.addControl(top_left_control);
        map.addControl(top_left_navigation);
        //单击获取点击的经纬度

        var markers = [];
        $(".item").mouseover(function () {
            var index = $(this).data('index');
            markers[index].setAnimation(BMAP_ANIMATION_BOUNCE);
        });

        $(".item").mouseout(function () {
            var index = $(this).data('index');
            markers[index].setAnimation();
        });

        $(".mappoint").each(function (i, o) {
            var plocation = $(o).html();
            plocation = plocation.split(",");
            var point = new BMap.Point(plocation[0], plocation[1]);
            var marker = new BMap.Marker(point);        // 创建标注
            markers[i] = marker;
            map.addOverlay(markers[i]);                     // 将标注添加到地图中
            markers[i].addEventListener("click", function () {
                $(".item").css("background", "#fff");
                $(o).parent().parent().css("background", "#bbb");
            });
        });

        map.centerAndZoom(point, 12);

    });
</script>
<style>
    .item {
        width: 97%;
        margin: 5px;
        padding: 5px;
        float: left;
        border-style: dotted;
        border-width: 1px;
    }

    .userDiv {
        float: left;
        height: 100px;
        padding: 5px;
        width: 18%;
    }

    .activityDiv {
        float: left;
        margin-top: -20px;
        width: 68%;
        padding-left: 10px;
    }

    #activitylist {
        padding: 10px;
        background-color: #fff;
        height:500px;
        overflow-y:auto;

    }
</style>
<%@include file="headerfooter/footer.jsp" %>