<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>

<div id="container" class="container">
    <div class="row">
        <div class="col-xs-6">
            <form class="form-horizontal col-xs-12" role="form" action="<%=request.getContextPath()%>/activity/addactivity"
                  method="post">

                <div class="form-group">
                    <label for="activityname" class="col-sm-4 control-label">标题</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="activityname" id="activityname"
                               placeholder="标题">
                    </div>
                </div>

                <div class="form-group">
                    <label for="activitytype" class="col-sm-2 control-label">类型</label>

                    <div class="col-sm-4">
                        <select type="select" name="activitytype" id="activitytype" class="form-control">
                            <option value="11">会所</option>
                            <option value="12">酒吧KTV</option>
                            <option value="13">个人发布</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="activitycontent" class="col-sm-2 control-label">简介</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="activitycontent" id="activitycontent"
                               placeholder="本店简介">
                    </div>
                </div>
                <div class="form-group">
                    <label for="location" class="col-sm-6 control-label">本店位置</label>

                    <div class="col-sm-1">
                        <input type="hidden" class="form-control" name="location" id="location" placeholder="">
                    </div>
                </div>

                <input type="hidden" name="extra" id="extra"/>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-6">
                        <button type="submit" class="btn btn-default">发布</button>
                    </div>
                </div>

            </form>
        </div>
        <div class="col-xs-6">
            <div id="allmap" class="col-sm-12" style="height:450px">

            </div>
        </div>

        <div class="row">
            <form action="<%=request.getContextPath()%>/imgpk/addpkimg" method="POST" enctype="multipart/form-data"
                  id="fileform">
                <input type="hidden" name="catagory" id="fmcatagory" value="11"/>
                本店图片: <input type="file" name="myfile"/><br/>
                <input type="submit" value="上传"/>
            </form>
        </div>
        <div class="demo-row">

        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        // 百度地图API功能
        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();
        map.centerAndZoom("北京", 12);
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

        map.addControl(top_left_control);
        map.addControl(top_left_navigation);
        //单击获取点击的经纬度
        map.addEventListener("click", function (e) {
            alert(e.point.lng + "," + e.point.lat);
            var point = new BMap.Point(e.point.lng, e.point.lat);
            map.centerAndZoom(point, 14);
            var marker = new BMap.Marker(point);        // 创建标注
            map.addOverlay(marker);                     // 将标注添加到地图中
            marker.setAnimation(BMAP_ANIMATION_BOUNCE); // 跳动的动画
            $("#location").val(e.point.lng + "," + e.point.lat);
        });


        var ctx = "<%=request.getContextPath()%>";

        var myFormoptions = {
            success: uploadSuccess  // post-submit callback
        };

        $('#fileform').submit(function () {

            $(this).ajaxSubmit(function (responseText) {
                uploadSuccess(responseText);
            });
            return false; //阻止表单默认提交

        });

        function uploadSuccess(responseText) {
            json = JSON.parse(responseText);
            $(".demo-row").append("<div class='item'>" +
                    "<a href='" + ctx + "/images/" + json + ".jpg' class='jqzoom' rel='gal1'  title='upload'>" +
                    "<img src='" + ctx + "/images/" + json + ".jpg' class='img-responsive img-rounded' style='vertical-align: bottom;width: 250px;height: auto;'/>" +
                    "</a>" +
                    "</div>");
            var imgsvalue = $("#extra").val();
            $("#extra").val(imgsvalue  + json + ",");
        }

        $('#activitytype').on('change', function () {
            var typetxt = $(this).val();
            $('#fmcatagory').val(typetxt);
        });

    });
</script>

<%@include file="headerfooter/footer.jsp" %>

