<%--
  Created by IntelliJ IDEA.
  User: simon
  Date: 14/11/17
  Time: 下午10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>

<div class="catabutton">
    <ul id='bp-3-element-normal-1'></ul>
</div>
<div class="container" id="containera">

    <c:forEach var="img" items="${requestScope.list}" varStatus="status">
        <div class="item">
            <div id="url_kill_referrer${img.id}" style="width:236px;height:auto;"></div>
            <script>
                $('#url_kill_referrer${img.id}').html(ReferrerKiller.imageHtml("${img.img_link}",{width:'240px',height:'auto'}));
            </script>
            <hr style="clear: both;margin-bottom: 10px">
            <%--<button type="button" class="btn btn-danger" data-id="${img.links_id}">不显示</button>
            <button type="button" class="btn btn-success" data-id="${img.id}" style="display:none">显示</button>--%>
            <a href="<%=request.getContextPath() %>/admin/openimport?id=${img.links_id}" class="btn btn-success" >导入</a>
        </div>
    </c:forEach>
    <input id="pageno" value="${currentPage}" type="hidden">
</div>
<div class="catabutton">
    <ul id='bp-3-element-normal-2'></ul>
</div>
<script type="text/javascript">
    $(document).ready(function() {
       /* $(".btn-danger").on('click',function(){
            $.get("<%=request.getContextPath() %>/admin/changeshow",{Action:"get",imgid:$(this).data('id'),imgshow:0});
            var sibbtn = $(this).siblings('.btn-success');
            sibbtn.show();
            $(this).hide();
        });

        $(".btn-success").on('click',function(){
            $.get("<%=request.getContextPath() %>/admin/changeshow",{Action:"get",imgid:$(this).data('id'),imgshow:1});
            var sibbtn = $(this).siblings('.btn-danger');
            sibbtn.show();
            $(this).hide();
        });*/

        $("#btn-import").on('click',function(){
            window.location.href="<%=request.getContextPath() %>/admin/openimport?id="+$(this).data('id');
        });

        $(".cataselect").on('change',function(){
            $.get("<%=request.getContextPath() %>/admin/changecatagory",{Action:"get",imgid:$(this).data('id'),catagory:$(this).val()});
        })

        var element = $('#bp-3-element-normal-1');
        var element1 = $('#bp-3-element-normal-2');

        var cpage = $('#pageno').val();
        options = {
            size:"normal",
            bootstrapMajorVersion:3,
            currentPage:cpage,
            numberOfPages: 5,
            totalPages: 100,
            pageUrl: function(type, page, current){

                return "<%=request.getContextPath() %>/admin/judgeimg?offset="+page;

            }
        };

        element.bootstrapPaginator(options);
        element1.bootstrapPaginator(options);



    });


</script>
<style>
    .item {
        width:272px;
        margin: 5px;
        padding:5px;
        float: left;
        border: 1px solid #ccc;
        background: #eee;
    }



</style>
<%@include file="headerfooter/footer.jsp" %>
