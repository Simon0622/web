<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <form method="post" action="<%=request.getContextPath() %>/ucenter/clsearch">
                <input name="words" id="words" placeholder="CL搜索,多个关键字以空格分隔" value="${keyword}" style="width: 200px"/>
                <button id="search" type="submit">搜索</button>
            </form>
        </div>
        <div class="panel-body">
            <c:forEach var="caoliu" items="${requestScope.list}" varStatus="status">
                <div class="row demo-row">
                    名称：${caoliu.title}
                    链接：<a href="${caoliu.link}" >点我</a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<%@include file="headerfooter/footer.jsp" %>