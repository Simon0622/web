<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-2
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>
<script>
    $(function(){
        $('input').on('change', function(){
            $.get("<%=request.getContextPath() %>/rank/score",{Action:"post",data:this.id,score:$(this).val()});
            $(this).parent().off();

        });

        $('#commentForm').submit(function() {

            $(this).ajaxSubmit(function(result) {
                /*$("#commentshow").prepend("<div class='commentdiv'>"+result+"</div>");*/
                location.reload(true);
            });
            return false; //阻止表单默认提交

        });
    });
</script>
<div id="container" class="container">
    <div class="row">
        <div class="col-xs-8">
            <h1>${link.title}</h1>
            <div class="content-meta">
                <span title="作者" class="icon-user"></span> <a href="${link.authorlink}">楼主</a>
                <span title="发帖时间" class="icon-time ml5"></span>${link.posttime}
                <span title="原链接" class="icon-arrow-right ml5"></span> <a href="${link.link}" target="_blank">原链接</a>
                <span title="点击量" class="icon-arrow-right ml5"></span> 点击量: ${(link.visit_num)*2+173}</a>
            </div>
            <div class="content">
                <span>${link.content}</span>
                <c:forEach var="img" items="${requestScope.images}">

                    <div class="image">
                        <img src="<%=request.getContextPath() %>/images/${img.imagesId}.jpg" class="img-responsive img-rounded" style="max-width: 800px;height: auto"/>
                        <input type="number" name="your_awesome_parameter" id="rating:${img.imagesId}" class="rating" data-caption="['负分', '不中', '不错哟', '一级棒', '女神']" value="" data-min="1" data-max="5" />

                    </div>

                </c:forEach>
            </div>
        </div>
        <div class="col-xs-4">
            <div id="commentshow">
                <c:forEach var="comment" items="${requestScope.comment}">
                    <div class="commentdiv">

                            ${comment.comment}
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-12">
                <form action="<%=request.getContextPath() %>/rank/comment" method="post" id="commentForm">
                    <textarea id="comment" name="comment" class="valtype col-xs-6" >
                    </textarea>
                    <input type="hidden" name="linkId" value="${link.id}"/>
                    <input type="submit" value="提交"/>
                </form>

            </div>
        </div>
    </div>
</div>
<%@include file="headerfooter/footer.jsp" %>