<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title>他她社区</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta charset="utf-8">
    <title>Flat UI - Free Bootstrap Framework and Theme</title>
    <meta name="description" content="他她社区"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Loading Flat UI -->
    <link href="<%=request.getContextPath() %>/css/lsm.css" rel="stylesheet">
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/jquery.jqzoom.css" type="text/css">

    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/star-rating.css">

    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/qunit-1.11.0.css">

    <script src="<%=request.getContextPath() %>/js/jquery-1.8.3.min.js"></script>

    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.form.js"></script>

    <script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>

    <%--<script src="<%=request.getContextPath() %>/js/jquery.cityselect.js"></script>--%>

    <script src="<%=request.getContextPath() %>/js/jquery.slotmachine.js"></script>

    <script src="<%=request.getContextPath() %>/js/jquery.jqzoom-core.js" type="text/javascript"></script>

    <script src="<%=request.getContextPath() %>/js/jquery.masonry.min.js" type="text/javascript"></script>

    <script src="<%=request.getContextPath() %>/js/bootstrap-rating-input.js"></script>

    <script src="<%=request.getContextPath() %>/js/bootstrap-paginator.js"></script>

    <script src="<%=request.getContextPath() %>/js/qunit-1.11.0.js"></script>

    <script src="<%=request.getContextPath() %>/js/referrer-killer.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">

    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->


    <script type="text/javascript">

        $(document).ready(function () {
            $('.jqzoom').jqzoom({
                zoomType: 'standard',
                lens: true,
                preloadImages: false,
                alwaysOn: false
            });

        });

    </script>
</head>

<body style="padding-top:50px">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<%=request.getContextPath() %>/imgpk/index">他她社区</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <%--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">排行榜<span class="caret"/></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<%=request.getContextPath() %>/rank/rankList">豆瓣贴吧排行榜</a></li>
                            <li><a href="<%=request.getContextPath() %>/rank/userImgs">用户排行榜</a></li>
                        </ul>
                    </li>
                    <li><a href="<%=request.getContextPath() %>/activity/index?querytype=11&hasloc=on">会所酒吧ktv</a></li>
                    &lt;%&ndash;<li class=""><a href="<%=request.getContextPath() %>/activity/index">组局蹭饭</a></li>&ndash;%&gt;
                    <li><a href="#">关于我们</a></li>
                </ul>

                <%
                    String username = (String) session.getAttribute("name");
                    if (username == null) {
                %>
                <ul class="nav navbar-nav navbar-right">
                    <a href="<%=request.getContextPath() %>/user/login"><h4>登录</h4></a>
                </ul>
                <%
                } else {
                %>
                <ul class="nav navbar-nav navbar-right">
                    <a href="<%=request.getContextPath() %>/ucenter/index?username=<%= username %>"><%= username %>
                    </a>
                </ul>
                <%
                    }
                %>

            </div>--%>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </div>
</nav>