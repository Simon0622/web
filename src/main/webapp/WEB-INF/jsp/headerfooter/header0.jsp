<!DOCTYPE html><%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %><%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html lang="zh-CN"><c:set var="webRoot" value="<%=request.getContextPath() %>" />
<head><c:set var="assetRoot" value="<%=request.getContextPath() %>" />
	<title>他她社区</title>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="description" content="他她社区"/>
	<link rel="shortcut icon" href="${assetRoot}/img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/lightbox.css">
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/star-rating.css"><c:if test="${requireDatePicker}">
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/mobiscroll.custom-2.5.2.min.css">
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/mobiscroll.android-ics-2.5.2.css"></c:if>
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/tata.css" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="${assetRoot}/css/tata.ie.css" />
	<script src="${assetRoot}/js/vendor/html5shiv.min.js"></script>
	<script src="${assetRoot}/js/vendor/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">window.q=[];window.$=function(f){q.push(f);}</script>
    <script src="${assetRoot}/js/referrer-killer.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="${webRoot}/imgpk/index">他她社区</a>
			</div>
		</div>
	</nav>