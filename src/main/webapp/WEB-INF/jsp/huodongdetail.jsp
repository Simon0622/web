<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>
<div class="container">

    <h1>${hd.hdname}</h1>

    <div class="col-xs-offset-1 col-xs-10" id="allmap" style="margin-top:10px;height: 260px;">

    </div>
    <span class="col-xs-12">地址：${hd.address}</span>

    <div class="row">
        <div class="col-xs-12">
            <label for="activitycontent" class="col-xs-12 control-label">内容</label>

            <div class="col-xs-12">
                <span id="activitycontent">${hd.activitycontent}</span>
            </div>

            <input type="hidden" name="latitude" value="${hd.latitude}" id="latitude">
            <input type="hidden" name="longitude" value="${hd.longitude}" id="longitude">

        </div>

        <c:if test="${not empty hd.hdewm}">
            <div class="col-xs-12">
                <span>(下图是活动发起者所建微信群二维码。苹果手机长按可识别，安卓用户需要先保存再通过扫码从相册中选择可直接进群)</span>
                <img src="${hd.hdewm}" class="col-xs-12" style="height: auto">
            </div>
        </c:if>


        <div id="commentshow" class="col-xs-12">
            <c:forEach var="comment" items="${requestScope.comments}">
                <div class="commentdiv">
                    <a href="<%=request.getContextPath() %>/wechatuser/detail?fromid=${comment.userfromid}&viewid=${viewid}">
                        <span class="col-xs-12">${comment.nickname}</span>
                    </a>
                    <img src="${comment.headimgurl}" class="col-xs-2"/>
                    <span class="col-xs-10">${comment.content}</span>

                    <a class="col-xs-12" href="javascript:void(0)" onclick="$('#huifu').show()">回复</a>

                    <div style="display: none" id="huifu">
                        <form action="<%=request.getContextPath() %>/huodong/comment" method="post"
                              class="commentForm">
                            <textarea class="content" name="content" class="col-xs-12">
                            </textarea>
                            <input type="hidden" name="nickname" value="${hd.nickname}">
                            <input type="hidden" name="fromid" value="${hd.userfromid}"/>
                            <input type="hidden" name="toid" value="${comment.fromid}"/>
                            <input type="hidden" name="pageId" value="${hd.id}"/>
                            <input type="hidden" name="replyId" value="${comment.id}"/>
                            <input type="submit" value="提交"/>
                        </form>
                    </div>
                </div>
            </c:forEach>
        </div>


        <div class="col-xs-12">
            <span class="col-xs-12">微信将会向发起者推送您的留言</span>
            <form action="<%=request.getContextPath() %>/huodong/comment" method="post" class="commentForm col-xs-12">
                <textarea class="content" name="content" class="col-xs-12">
                </textarea>
                <input type="hidden" name="nickname" value="${hd.nickname}">
                <input type="hidden" name="fromid" value="${viewid}"/>
                <input type="hidden" name="toid" value="${hd.userfromid}"/>
                <input type="hidden" name="pageId" value="${hd.id}"/>
                <input type="submit" value="提交"/>
            </form>
        </div>

    </div>

</div>
<script>
    $(function () {

        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

        map.addControl(top_left_control);
        map.addControl(top_left_navigation);

        //从数据库中读取点击的经纬度
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();

        var point = new BMap.Point(longitude, latitude);
        var marker = new BMap.Marker(point);        // 创建标注
        map.addOverlay(marker);

        map.centerAndZoom(point, 12);


    });
</script>
<script>
    $(function () {

        $('.commentForm').submit(function () {

            $(this).ajaxSubmit(function (result) {
                /*$("#commentshow").prepend("<div class='commentdiv'>"+result+"</div>");*/
                location.reload(true);
            });
            return false; //阻止表单默认提交

        });
    });
</script>
<%@include file="headerfooter/footer.jsp" %>