<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="<%=request.getContextPath() %>/js/api-6.1.js"></script>
<div class="container">
    <div class="col-xs-offset-1 col-xs-10" id="allmap" style="margin-top:10px;height: 260px;">

    </div>

    <div class="row col-xs-12">

        <form class="form-horizontal col-xs-12" role="form"
              action="<%=request.getContextPath()%>/huodong/addhuodong"
              method="post">
            <input id="id" type="hidden" name="id" value="${hd.id}">
<%--
            <input id="appIds" type="hidden" name="appIds" value="${appId}">
            <input id="timestamps" type="hidden" name="timestamps" value="${timestamp}">
            <input id="nonceStrs" type="hidden" name="nonceStrs" value="${nonceStr}">
            <input id="signatures" type="hidden" name="signatures" value="${signature}">--%>

            <div class="col-xs-12">
                <span class="col-xs-12">地址</span>

                <div class="col-xs-12">
                    <input type="text" class="form-control" name="address" id="address" class="col-xs-12"
                           placeholder="标题" value="${hd.address}">
                </div>
            </div>

            <div class="col-xs-12">
                <span class="col-xs-12">标题</span>

                <div class="col-xs-12">
                    <input type="text" class="form-control" name="hdname" id="hdname" class="col-xs-12"
                           placeholder="标题" value="${hd.hdname}">
                </div>
            </div>

            <div class="col-xs-12">
                <span class="col-xs-12">内容</span>

                <div class="col-xs-12">
                    <textarea id="activitycontent" name="activitycontent" class="col-xs-12">
                        ${hd.activitycontent}
                    </textarea>
                </div>
            </div>


            <input type="hidden" name="latitude" value="${hd.latitude}" id="latitude">
            <input type="hidden" name="longitude" value="${hd.longitude}" id="longitude">

            <c:if test="${empty hd.hdewm}">
            <div class="col-xs-12">
                你可以通过公共账户的活动菜单上传您为这次活动所建群的二维码
            </div>
                <%--<button class="btn btn_primary" id="chooseImage">chooseImage</button>--%>
            </c:if>
            <div class="col-xs-12">
                <img src="${hd.hdewm}" class="col-xs-12" style="height: auto">
            </div>

            <div class="form-group">
                <div class="col-xs-offset-4 col-xs-4">
                    <button type="submit" class="btn btn-default col-xs-12">更新</button>
                </div>
            </div>

        </form>


        <div class="col-xs-12" style="margin-top: 20px">
            <c:forEach var="comment" items="${requestScope.comments}">
                <div class="commentdiv">
                    <a href="<%=request.getContextPath() %>/wechatuser/detail?fromid=${comment.userfromid}&viewid=${user.fromid}">
                        <span class="col-xs-12">${comment.nickname}</span>
                    </a>
                    <img src="${comment.headimgurl}" class="col-xs-3"/>
                    <span class="col-xs-9">${comment.content}</span>
                </div>
            </c:forEach>
        </div>

    </div>

</div>
<script>
    $(function () {

        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

        map.addControl(top_left_control);
        map.addControl(top_left_navigation);

        //从数据库中读取点击的经纬度
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();

        var point = new BMap.Point(longitude, latitude);
        var marker = new BMap.Marker(point);        // 创建标注
        map.addOverlay(marker);

        map.centerAndZoom(point, 12);


    });
</script>

<%@include file="headerfooter/footer.jsp" %>