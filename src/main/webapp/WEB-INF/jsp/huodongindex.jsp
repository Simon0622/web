<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=anGvf7LkCrhGdGDClf0hpfw9"></script>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">

        </div>
        <div class="panel-body">
            <div class="row demo-row">
                <div class="col-xs-12">
                    <c:forEach var="activity" items="${requestScope.list}" varStatus="status">

                            <div class="col-xs-12">
                                <div class="col-xs-12">
                                    <a href="<%=request.getContextPath()%>/huodong/huodongedit?id=${activity.id}"><h3>${activity.hdname}</h3></a>
                                </div>
                                <div class="col-xs-12">
                                    <c:if test="${not empty activity.address}">
                                        <span>${activity.address}</span>
                                    </c:if>
                                </div>
                                <span class="mappoint"
                                      style="display: none">${activity.longitude},${activity.latitude}</span>
                            </div>

                    </c:forEach>
                </div>

                <div class="col-xs-offset-1 col-xs-10" id="allmap" style="margin-top:10px;height: 300px;">

                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(function () {


        var map = new BMap.Map("allmap");
        map.enableScrollWheelZoom();
        map.centerAndZoom("北京", 12);
        var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
        var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件

        map.addControl(top_left_control);
        map.addControl(top_left_navigation);
        //单击获取点击的经纬度

        var markers = [];
        $(".item").mouseover(function () {
            var index = $(this).data('index');
            markers[index].setAnimation(BMAP_ANIMATION_BOUNCE);
        });

        $(".item").mouseout(function () {
            var index = $(this).data('index');
            markers[index].setAnimation();
        });

        $(".mappoint").each(function (i, o) {
            var plocation = $(o).html();
            plocation = plocation.split(",");
            var point = new BMap.Point(plocation[0], plocation[1]);
            var marker = new BMap.Marker(point);        // 创建标注
            markers[i] = marker;
            map.addOverlay(markers[i]);                     // 将标注添加到地图中
            markers[i].addEventListener("click", function () {
                $(".item").css("background", "#fff");
                $(o).parent().parent().css("background", "#bbb");
            });
        });

        map.centerAndZoom(point, 12);


    });
</script>
<%@include file="headerfooter/footer.jsp" %>