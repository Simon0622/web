<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>


<div class="container">

    <form action="<%=request.getContextPath()%>/imgpk/addpkimg" method="POST" enctype="multipart/form-data" id="fileform">
        选择图片: <input type="file" name="myfile" multiple="multiple"/><br/>
        <input type="submit" value="上传"/>
    </form>

    <div class="row demo-row">

    </div>
</div>
<script type="text/javascript">



    $(document).ready(function() {

        var ctx = "<%=request.getContextPath()%>";

        var myFormoptions = {
            success:uploadSuccess  // post-submit callback
        };

        $('#fileform').submit(function() {

            $(this).ajaxSubmit(function(responseText) {
                uploadSuccess(responseText);
            });
            return false; //阻止表单默认提交

        });

        function uploadSuccess(responseText){
            json = JSON.parse(responseText);
            $(".row").append("<div class='item'>" +
                    "<a href='"+ctx+"/images/"+json+".jpg' class='jqzoom' rel='gal1'  title='upload'>" +
                    "<img src='"+ctx+"/images/"+json+".jpg' class='img-responsive img-rounded' style='vertical-align: bottom;width: 250px;height: auto;'/>" +
                    "</a>" +
                    "</div>");
        }

    });
</script>

<%@include file="headerfooter/footer.jsp" %>
<%--<img id='responseText' src='"+ctx+"/images/"+json+".jpg'>--%>

