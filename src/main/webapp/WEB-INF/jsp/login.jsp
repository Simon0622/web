<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>
<div class="container">
    <form class="form-signin" id="signinForm" method="POST" action="<%=request.getContextPath() %>/user/userlogin">
        <h2 class="form-signin-heading">请登录</h2>
        <div id="login_alert" >
        </div>
        <input type="text" name="username" id="username" class="input-block-level" placeholder="用户名">
        <input type="password" name="password" id="password" class="input-block-level" placeholder="密码<长度大于6>">
        <label class="checkbox">
            <input type="checkbox" name="remember" value="remember-me"> 记住我 <a style="float:right;" href="/forget_pass">找回密码</a>
        </label>
        <button class="btn btn-large btn-primary btn-block" type="submit">登录</button>
        <a class="btn btn-large btn-block" href="<%=request.getContextPath() %>/user/register">注册</a>
    </form>
</div>
<style>
    form {
        margin: 0 0 20px;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin input[type="text"], .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox {
        margin-bottom: 10px;
    }
</style>
<script>
    function isEmail(str){
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
        return reg.test(str);
    }

    $('#signinForm').submit(function() {
        var uname = $('#username').val();
        var ismail = isEmail(uname);
        if(!ismail){
            alert("请输入合法的邮箱");
            return false;
        }
        $(this).ajaxSubmit();
        return false; //阻止表单默认提交

    });

</script>
<%@include file="headerfooter/footer.jsp" %>
