<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-1
  Time: 下午4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>

<style>
    .slotMachineBox {
        padding: 150px 0px;
        text-align: center;
    }

    .slotMachineButton {
        width: 100px;
        height: 100px;
        overflow: hidden;
        display: inline-block;
        text-align: center;
    }

    .slotMachineButton {
        -moz-box-shadow: inset 0px 1px 0px 0px #fce2c1;
        -webkit-box-shadow: inset 0px 1px 0px 0px #fce2c1;
        box-shadow: inset 0px 1px 0px 0px #fce2c1;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffc477), color-stop(1, #fb9e25));
        background: -moz-linear-gradient(center top, #ffc477 5%, #fb9e25 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
        background-color: #ffc477;
        border-radius: 75px;
        text-indent: 0px;
        border: 6px solid #eeb44f;
        display: inline-block;
        color: #ffffff;
        font: 30px/100px "Microsoft Yahei";
        height: 100px;
        line-height: 100px;
        width: 100px;
        text-align: center;
        text-shadow: 1px 1px 0px #cc9f52;
        margin-left: 50px;
        cursor: pointer;
    }

    .slotMachineButton:hover {
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #fb9e25), color-stop(1, #ffc477));
        background: -moz-linear-gradient(center top, #fb9e25 5%, #ffc477 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fb9e25', endColorstr='#ffc477');
        background-color: #fb9e25;
    }

    .slotMachineButton:active {
        position: relative;
        top: 1px;
    }

    #slotMachineButtonPrev {
        margin: 0 50px 0 0;
    }

    .slotMachine {
        max-width: auto;
        height: 400px;
        overflow: hidden;
        display: inline-block;
        text-align: center;
        border: 5px solid #000;
        background-color: #ffffff;
    }

    .slotMachine .slot {
        height: 400px;
        max-width: auto;
    }

    img {
        width: 350px;
        height: auto;
        max-height: 400px;
        max-width: 100%;
    }
</style>

<div class="container">
    <div class="row demo-row">
        <div class="col-xs-4"></div>
        <div class="col-xs-6">
            <form action="#" id="oneround" class="form-inline" role="form">

                <div class="form-group">
                    <div class="input-group">
                        <h3>年龄</h3>
                        <label class="sr-only" for="age">年龄</label>

                        <select type="select" name="age" id="age" class="form-control">
                            <option value="0">全部</option>
                            <option value="1" ${age=="1"?'selected':''}>16-18</option>
                            <option value="2" ${age=="2"?'selected':''}>18-23</option>
                            <option value="3" ${age=="3"?'selected':''}>24-31</option>
                            <option value="4" ${age=="4"?'selected':''}>32+</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <h3>星座</h3>
                    <label class="sr-only" for="birth">星座</label>
                    <select type="select" name="birth" id="birth" class="form-control">
                        <option value="0">全部</option>
                        <option value="1" ${birth=="1"?'selected':''}>摩羯座</option>
                        <option value="2" ${birth=="2"?'selected':''}>水瓶座</option>
                        <option value="3" ${birth=="3"?'selected':''}>双鱼座</option>
                        <option value="4" ${birth=="4"?'selected':''}>白羊座</option>
                        <option value="5" ${birth=="5"?'selected':''}>金牛座</option>
                        <option value="6" ${birth=="6"?'selected':''}>双子座</option>
                        <option value="7" ${birth=="7"?'selected':''}>巨蟹座</option>
                        <option value="8" ${birth=="8"?'selected':''}>狮子座</option>
                        <option value="9" ${birth=="9"?'selected':''}>处女座</option>
                        <option value="10" ${birth=="10"?'selected':''}>天秤座</option>
                        <option value="11" ${birth=="11"?'selected':''}>天蝎座</option>
                        <option value="12" ${birth=="12"?'selected':''}>射手座</option>
                    </select>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <h3>来自</h3>
                        <label class="sr-only" for="age">来自:</label>

                        <div id="city_1">
                            <select class="prov form-control" name="province" id="province"></select>
                            <%--<select class="city form-control" disabled="disabled"></select>--%>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="col-xs-2"></div>
    </div>
    <div class="row demo-row">
        <div class="col-xs-1"></div>
        <div class="col-xs-5">
            <div id="left" class="slotMachine">
                <c:forEach var="img" items="${requestScope.left}" varStatus="status">
                    <div class="slot">
                        <img id="${img.id}" src="<%=request.getContextPath() %>/images/${img.id}.jpg"/>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="col-xs-1"></div>
        <div class="col-xs-5">
            <div id="right" class="slotMachine">
                <c:forEach var="img" items="${requestScope.right}" varStatus="status">
                    <div class="slot">
                        <img id="${img.id}" src="<%=request.getContextPath() %>/images/${img.id}.jpg"/>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<span id="result" style="display: none"></span>
<script>

    var leftid = 1;
    var rightid = 1;
    var offset = 2;

    $("#city_1").citySelect({
        prov: "北京",
        nodata: "none"
    });

    $('#age').on('change', function () {
        if ($('#age').val() == 0 || $('birth').val() == 0) {

        } else {
            $.get("<%=request.getContextPath() %>/imgpk/agebirth", {Action: "get", age: $('#age').val(), birth: $('#birth').val(),province:$('province').val()});
        }

    });

    $('#birth').on('change', function () {
        if ($('#age').val() == 0 || $('birth').val() == 0) {

        } else {
            $.get("<%=request.getContextPath() %>/imgpk/agebirth", {Action: "get", age: $('#age').val(), birth: $('#birth').val(),province:$('province').val()});
        }

    });

    $('#province').on('change', function () {
        if ($('#age').val() == 0 || $('birth').val() == 0) {

        } else {
            $.get("<%=request.getContextPath() %>/imgpk/agebirth", {Action: "get", age: $('#age').val(), birth: $('#birth').val(),province:$('province').val()});
        }

    });

    $(document).ready(function () {
        var left = $("#left").slotMachine({
            active: 0,
            delay: 5000,
            repeat: false
        });

        var right = $("#right").slotMachine({
            active: 0,
            delay: 5000,
            repeat: false
        });


        $("#left .slot img").click(function () {

            if ($('#age').val() == 0 || $('birth').val() == 0) {
                alert("请选择您的年龄和星座");
                return;
            }

            right.next();
            var leftnowid = $('#left').find('.slot:nth-child(' + leftid + ') img').attr('id');
            var rightnowid = $('#right').find('.slot:nth-child(' + rightid + ') img').attr('id');
            //$('#result').append(':'+leftnowid+','+rightnowid);
            $.get("<%=request.getContextPath() %>/imgpk/change", {Action: "get", win: leftnowid, lose: rightnowid});
            rightid = rightid + 1;
            if (rightid > 10) {

                offset = offset + 1;
                $.get("<%=request.getContextPath() %>/imgpk/nextbatch", {Action: "get", offset: offset}, function (result) {
                    json = JSON.parse(result);
                    for (i = 0; i < json.length; i++) {
                        $('#right').find('.slot:nth-child(' + i + ') img').attr('id', json[i].id);
                        $('#right').find('.slot:nth-child(' + i + ') img').attr('src', '<%=request.getContextPath() %>/images/' + json[i].id + '.jpg')
                    }
                });
                rightid = 1;
                //$('#result').html("");

            }
        });


        $("#right .slot img").click(function () {

            if ($('#age').val() == 0 || $('birth').val() == 0) {
                alert("请选择您的年龄和星座");
                return;
            }

            left.next();
            var leftnowid = $('#left').find('.slot:nth-child(' + leftid + ') img').attr('id');
            var rightnowid = $('#right').find('.slot:nth-child(' + rightid + ') img').attr('id');
            //$('#result').append(':'+rightnowid+','+leftnowid);
            $.get("<%=request.getContextPath() %>/imgpk/change", {Action: "get", win: rightnowid, lose: leftnowid});
            leftid = leftid + 1;
            if (parseInt(leftid) > parseInt(10)) {

                offset = offset + 1;
                $.get("<%=request.getContextPath() %>/imgpk/nextbatch", {Action: "get", offset: offset}, function (result) {
                    json = JSON.parse(result);
                    for (i = 0; i < json.length; i++) {
                        $('#left').find('.slot:nth-child(' + i + ') img').attr('id', json[i].id);
                        $('#left').find('.slot:nth-child(' + i + ') img').attr('src', '<%=request.getContextPath() %>/images/' + json[i].id + '.jpg')
                    }
                });
                leftid = 1;
                //$('#result').html("");


            }
        });
    });
</script>
<%@include file="headerfooter/footer.jsp" %>

