<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-2
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<div class="catabutton">
    <div class="row">
        <div class="col-xs-4">
            <button type="button" class="btn btn-primary" data-rank="0">全部</button>
            <button type="button" class="btn btn-info" data-rank="1">胜率排行</button>
            <button type="button" class="btn btn-info" data-rank="2">得分排行</button>
            <button type="button" class="btn btn-info" data-rank="3">点击量排行</button>
        </div>
        <div class="col-xs-2">
            根据年龄
            <select type="select" name="age" id="age" class="form-control">
                <option value="0">全部</option>
                <option value="1">14-18</option>
                <option value="2">19-23</option>
                <option value="3">24-28</option>
                <option value="4">29-32</option>
                <option value="5">32+</option>
            </select>
        </div>
        <div class="col-xs-2">
            根据星座
            <select type="select" name="birth" id="birth" class="form-control">
                <option value="0">全部</option>
                <option value="1">摩羯座</option>
                <option value="2">水瓶座</option>
                <option value="3">双鱼座</option>
                <option value="4">白羊座</option>
                <option value="5">金牛座</option>
                <option value="6">双子座</option>
                <option value="7">巨蟹座</option>
                <option value="8">狮子座</option>
                <option value="9">处女座</option>
                <option value="10">天秤座</option>
                <option value="11">天蝎座</option>
                <option value="12">射手座</option>
            </select>
        </div>
    </div>


</div>
<div class="container" style="width:1210px">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<%=request.getContextPath()%>/imgpk/ipk">
                <button class="btn btn-success" type="button">我要上传</button>
            </a>

        </div>
        <div class="panel-body">
            <div id="containera">

                <span id="rank" style="display: none"></span>


                <c:forEach var="img" items="${requestScope.list}" varStatus="status">
                    <div class="item">
                        <a href="<%=request.getContextPath() %>/images/${img.id}.jpg" class="jqzoom" rel='gal1'
                           title="triumph">
                            <img src="<%=request.getContextPath() %>/images/${img.id}.jpg"
                                 class="img-responsive img-rounded"
                                 style="vertical-align: bottom;width: 250px;height: auto;"/>
                        </a>
                        <hr style="clear: both;margin-bottom: 10px">
                        <a href="<%=request.getContextPath() %>/rank/detail?id=${img.links_id}"><h4>${img.title}</h4>
                        </a>

                        <div class="image">
                            <input type="number" name="your_awesome_parameter" id="rating:${img.id}" class="rating"
                                   data-caption="['负分', '不中', '不错哟', '一级棒', '女神']" value="" data-min="1" data-max="5"/>
                        </div>
                        <div class="images">

                            <span>胜率:${(img.pkscore+1)/(img.pknum+1)}</span>
                            <span>平均分:${(img.startscore+3)/(img.startnum+1)}</span>


                        </div>
                    </div>
                </c:forEach>


                <input id="pageno" value="${currentPage}" type="hidden">
            </div>
        </div>
    </div>
</div>
<div class="catabutton">
    <ul id='bp-3-element-normal-test2'></ul>
</div>


<script>
    $(function () {
        var $container = $('#containera');

        $container.imagesLoaded(function () {
            $container.masonry({
                itemSelector: '.item'
            });
        });

        var element = $('#bp-3-element-normal-test');

        var element2 = $('#bp-3-element-normal-test2');

        var cpage = $('#pageno').val();
        options = {
            size: "normal",
            bootstrapMajorVersion: 3,
            currentPage: cpage,
            numberOfPages: 5,
            totalPages: 100,
            pageUrl: function (type, page, current) {

                return "<%=request.getContextPath() %>/rank/rankList?offset=" + page + "&rank=" + $("#rank").html() + '&age=' + $('#age').val() + '&birth=' + $('birth').val();

            }
        };

        element.bootstrapPaginator(options);
        element2.bootstrapPaginator(options);

        $(".btn").on('click', function () {
            $("#rank").html($(this).data('rank'));
            window.location.href = '<%=request.getContextPath() %>/rank/rankList?offset=1&rank=' + $("#rank").html() + '&age=' + $('#age').val() + '&birth=' + $('birth').val();
        });

        $('input').on('change', function () {
            $.get("<%=request.getContextPath() %>/rank/score", {Action: "post", data: this.id, score: $(this).val()});
            $(this).parent().off();

        });

    });
</script>
<%@include file="headerfooter/footer.jsp" %>
