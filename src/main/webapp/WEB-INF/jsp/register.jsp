<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="headerfooter/header.jsp" %>
<div class="container">
    <form class="form-signin" method="post" action="<%=request.getContextPath() %>/user/userregister">
        <h2 class="form-signin-heading">请输入您的用户名密码</h2>
        <div id="login_alert"> </div>
        <input class="input-block-level" type="text" placeholder="用户名(限英文)" name="username">
        <input class="input-block-level" type="password" placeholder="密码" name="password">
        <button class="btn btn-large btn-primary btn-block" type="submit">注册</button>
    </form>
</div>
<%@include file="headerfooter/footer.jsp" %>
