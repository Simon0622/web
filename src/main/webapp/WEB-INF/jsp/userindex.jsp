<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<div class="container">
    <div class="row">

        <div class="col-xs-12">

            <form method="post" action="<%=request.getContextPath() %>/ucenter/clsearch">
                <input name="words" id="words" placeholder="CL搜索,多个关键字以空格分隔" value="${keyword}" style="width: 200px"/>
                <input name="username" id="username" value="<%=username%>" type="hidden"/>
                <button id="search" type="submit">搜索</button>
            </form>

            <div id="pictureDiv">
                <c:forEach var="img" items="${requestScope.userimgs}" varStatus="status">
                    <div class="item">
                        <a href="<%=request.getContextPath() %>/images/${img.id}.jpg" class="jqzoom" rel='gal1'  title="triumph">
                            <img src="<%=request.getContextPath() %>/images/${img.id}.jpg" class="img-responsive img-rounded" style="vertical-align: bottom;width: 250px;height: auto;"/>
                        </a>
                        <hr style="clear: both;margin-bottom: 10px">
                        <a href="<%=request.getContextPath() %>/ucenter/statistics"><h4>查看统计</h4></a>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<%@include file="headerfooter/footer.jsp" %>
