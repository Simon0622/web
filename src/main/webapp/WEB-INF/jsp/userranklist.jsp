<%--
  Created by IntelliJ IDEA.
  User: simonliu
  Date: 14-11-2
  Time: 下午3:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@include file="headerfooter/header.jsp" %>
<div class="catabutton">
    <div class="row">
        <div class="col-xs-4">
            <button type="button" class="btn btn-primary" data-rank="0">全部</button>
            <button type="button" class="btn btn-info" data-rank="1">胜率排行</button>
            <button type="button" class="btn btn-info" data-rank="2">得分排行</button>
        </div>

    </div>


</div>
<div class="container" style="width:1210px">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="<%=request.getContextPath()%>/imgpk/ipk">
                <button class="btn btn-success" type="button">我要上传</button>
            </a>

        </div>
        <div class="panel-body">
            <div id="containera">

                <span id="rank" style="display: none"></span>


                <c:forEach var="img" items="${requestScope.list}" varStatus="status">
                    <div class="item">
                        <a href="<%=request.getContextPath() %>/images/${img.id}.jpg" class="jqzoom" rel='gal1'
                           title="triumph">
                            <img src="<%=request.getContextPath() %>/images/${img.id}.jpg"
                                 class="img-responsive img-rounded"
                                 style="vertical-align: bottom;width: 250px;height: auto;"/>
                        </a>
                        <hr style="clear: both;margin-bottom: 10px">

                        <div class="image">
                            <input type="number" name="your_awesome_parameter" id="rating:${img.id}" class="rating"
                                   data-caption="['负分', '不中', '不错哟', '一级棒', '女神']" value="" data-min="1" data-max="5"/>
                        </div>
                    </div>
                </c:forEach>


                <input id="pageno" value="${currentPage}" type="hidden">
            </div>
        </div>
    </div>
</div>
<div class="catabutton">
    <ul id='bp-3-element-normal-test2'></ul>
</div>


<script>
    $(function () {
        var $container = $('#containera');

        $container.imagesLoaded(function () {
            $container.masonry({
                itemSelector: '.item'
            });
        });

        var element = $('#bp-3-element-normal-test');

        var element2 = $('#bp-3-element-normal-test2');

        var cpage = $('#pageno').val();
        options = {
            size: "normal",
            bootstrapMajorVersion: 3,
            currentPage: cpage,
            numberOfPages: 5,
            totalPages: 100,
            pageUrl: function (type, page, current) {

                return "<%=request.getContextPath() %>/rank/userImgs?offset=" + page + "&rank=" + $("#rank").html();

            }
        };

        element.bootstrapPaginator(options);
        element2.bootstrapPaginator(options);

        $(".btn").on('click', function () {
            $("#rank").html($(this).data('rank'));
            window.location.href = '<%=request.getContextPath() %>/rank/userImgs?offset=1&rank=' + $("#rank").html();
        });

        $('input').on('change', function () {
            $.get("<%=request.getContextPath() %>/rank/score", {Action: "post", data: this.id, score: $(this).val()});
            $(this).parent().off();

        });

    });
</script>
<%@include file="headerfooter/footer.jsp" %>