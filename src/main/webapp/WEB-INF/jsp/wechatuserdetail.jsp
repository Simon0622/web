<%@ page language="java" pageEncoding="UTF-8" %><%@include file="headerfooter/header0.jsp" %><jsp:useBean id="now" class="java.util.Date"/>
	<div class="page"><c:set var="photo" value="${user.weChatImageList[0]}"/>
		<div class="vcard">
			<c:if test="${photo ne null}"><img src="${photo.imgurl}" alt="${user.nickname}"/></c:if>
			<div class="info">
				<h3 class="name"><c:out value="${user.nickname}"/> <small><img src="${assetRoot}/img/${user.sex eq '1' ? 'cupid' : 'venus'}.png" alt="${user.sex eq '1' ? '帅哥' : '美女'}" title="${user.sex eq '1' ? '帅哥' : '美女'}"/></small></h3>
				<dl>
					<c:if test="${not empty user.birth}"><dt>年龄：</dt><dd class="age"><fmt:formatDate var="yearNow" value="${now}" pattern="yyyy"/><fmt:formatDate var="yearBirth" value="${user.birthday}" pattern="yyyy"/>${yearNow - yearBirth} <small>岁</small></dd></c:if>
					<c:if test="${not empty user.height}"><dt>身高：</dt><dd class="height"><i class="icon icon-white icon-text-height"></i> <c:out value="${user.height}"/> <small>cm</small></dd></c:if>
					<c:if test="${not empty user.province}"><dt>城市：</dt><dd class="location"><i class="icon icon-white icon-map-marker"></i> <c:out value="${user.province}"/>，<c:out value="${user.city}"/></dd></c:if>
					<c:if test="${not empty user.career}"><dt>职业：</dt><dd class="career"><i class="icon icon-white icon-briefcase"></i> <c:out value="${user.career}"/></dd></c:if>
				</dl>
				<hr/>
				<div style="margin-left:20px">
					<input type="number" class="rating" data-caption="['不喜欢', '一般', '不错哟', '赞一个', '求交往:)']" value="" data-min="1" data-max="5" id="rating:${photo.id}"/>
				</div>
			</div>
		</div>
		<c:if test="${viewid eq user.fromid}">
		<div class="container">
			<a class="btn btn-info btn-sm pull-right" href="${webRoot}/wechatuser/edit?fromid=${user.fromid}">编辑个人资料</a>
		</div></c:if>
		<c:if test="${not empty user.brief}"><hr/>
		<div class="container">
			<c:out value="${user.brief}"/>
		</div></c:if>
		<hr/>
		<div class="container thumbs">
			<div class="row"><c:forEach var="img" items="${user.weChatImageList}">
				<div class="col-xs-3 col-sm-2 col-md-2">
					<a href="${img.imgurl}" class="thumbnail" data-lightbox="photo"><img src="${img.imgurl}" alt="" /></a>
				</div></c:forEach>
			</div>
		</div>
		<hr/>
		<div class="container comments">
			<c:if test="${viewid ne user.fromid}"><form class="row" action="${webRoot}/wechatuser/comment" method="post" id="commentForm">
				<div class="form-group col-xs-12 col-sm-10">
					<textarea id="content" name="content" class="form-control" placeholder="交个朋友吧~"></textarea>
				</div>
				<div class="form-group col-xs-4 col-xs-offset-8 col-sm-2 col-sm-offset-0">
					<input type="hidden" name="fromid" value="${viewid}"/>
					<input type="hidden" name="toid" value="${user.fromid}"/>
					<input type="hidden" name="pagefromid" value="${user.fromid}"/>
					<button type="submit" class="btn btn-primary btn-block">提交</button>
				</div>
			</form>
			<p><small>友情提示：这里只显示您和楼主的留言</small></p></c:if>
			<c:forEach var="comment" items="${requestScope.comments}"><div class="media">
				<a class="media-left avatar" href="${webRoot}/wechatuser/detail?fromid=${comment.userfromid}&amp;viewid=${user.fromid}">
					<img src="${comment.headimgurl}" alt="<c:out value="${comment.nickname}"/>">
				</a>
				<div class="media-body">
					<h4 class="media-heading name">
						<a href="${webRoot}/wechatuser/detail?fromid=${comment.userfromid}&amp;viewid=${user.fromid}"><c:out value="${comment.nickname}"/></a>
						<small><c:choose><c:when test="${now.date != comment.createtime.date}"><fmt:formatDate value="${comment.createtime}" pattern="MM-dd HH:mm"/></c:when><c:otherwise><fmt:formatDate value="${comment.createtime}" pattern="HH:mm"/></c:otherwise></c:choose></small>
					</h4>
					<c:if test="${viewid eq user.fromid}"><button class="pull-right btn btn-xs btn-info reply" data-reply-text="取消" data-toid="${comment.userfromid}" data-touser="<c:out value="${comment.nickname}"/>">回复</button></c:if>
					<p><c:out value="${comment.content}"/></p>
				</div>
			</div></c:forEach>
			<c:if test="${viewid eq user.fromid}"><form id="form-reply" class="row hide" action="${webRoot}/wechatuser/comment" method="post">
				<div class="form-group col-xs-12 col-sm-10">
					<textarea name="content" class="form-control" placeholder="回复"></textarea>
				</div>
				<div class="form-group col-xs-4 col-xs-offset-8 col-sm-2 col-sm-offset-0">
					<input type="hidden" name="fromid" value="${user.fromid}"/>
					<input type="hidden" name="toid" value=""/>
					<input type="hidden" name="pagefromid" value=""/>
					<button type="submit" class="btn btn-primary btn-sm btn-block">提交</button>
				</div>
			</form>
			</c:if>
		</div>
		<hr/>
	</div>
	<script type="text/javascript">
	$(function() {
		$('.rating').on('change', function() {
			$.get("${webRoot}/wechatuser/updatescore", { Action: "post", data: this.id, score: $(this).val() });
			$(this).parent().off();
		});
		$('#commentForm, #form-reply').on('submit', function() {
			$(this).ajaxSubmit(function(result) {
				/*$("#commentshow").prepend("<div class='commentdiv'>"+result+"</div>");*/
				location.reload(true);
			});
			return false;
		});

		var $replyForm = $('#form-reply').remove().removeClass('hide'), $replyBtn;
		$('.comments').on('click', 'button.reply', function(e) {
			var $btn = $(e.target), $container = $btn.closest('.media-body');
			if ($container.get(0) === $replyForm.parent().get(0)) {
				$btn.button('reset');
				$replyForm.remove()
					.find('[name="toid"], [name="pagefromid"]').val('');
			} else {
				if ($replyBtn && $replyBtn !== $btn) {
					$replyBtn.button('reset');
				}
				$replyBtn = $btn;
				$btn.button('reply');
				$replyForm.appendTo($container)
					.find('[name="toid"], [name="pagefromid"]').val($btn.data('toid')).end()
					.find('[name="content"]').val('回复' + $btn.data('touser') + '：').focus();
			}
		});
	});
	</script>
<%@include file="headerfooter/footer0.jsp" %>