<%@ page language="java" pageEncoding="UTF-8" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><c:set var="requireDatePicker" value="${true}"/><%@include file="headerfooter/header0.jsp" %>
	<div class="page">
		<hr/>
		<div class="container">
			<form class="info-form" action="${webRoot}/wechatuser/updatedetail" method="post">
				<div class="form-group clearfix">
					<label class="control-label" for="brief">个性签名：</label>
					<div class="control hide">
						<textarea id="brief" name="brief" class="form-control" placeholder="介绍一下自己吧~"><c:out value="${user.brief}"/></textarea>
					</div>
					<div class="form-control-static"><c:choose>
						<c:when test="${empty user.brief}"><span class="text-muted">介绍一下自己吧~</span></c:when><c:otherwise><c:out value="${user.brief}"/></c:otherwise>
					</c:choose></div>
				</div>
				<hr/>
				<div class="form-group clearfix">
					<label class="control-label" for="sex">性别：</label>
					<div class="control">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-info${user.sex eq '1' ? ' active' : ''}">
								<input type="radio" name="sex" id="sex-male" value="1" autocomplete="off"${user.sex eq '1' ? ' checked' : ''}/> &nbsp;&nbsp;&nbsp;男&nbsp;&nbsp;&nbsp;
							</label>
							<label class="btn btn-danger${user.sex eq '2' ? ' active' : ''}">
								<input type="radio" name="sex" id="sex-female" value="2" autocomplete="off"${user.sex eq '2' ? ' checked' : ''}/> &nbsp;&nbsp;&nbsp;女&nbsp;&nbsp;&nbsp;
							</label>
						</div>
					</div>
				</div>
				<hr/>
				<div class="form-group clearfix">
					<label class="control-label" for="birth">生日：</label>
					<div class="control hide">
						<div class="row">
							<div class="col-xs-9 col-sm-6 pull-right"><input type="date" name="birth" id="birth" class="form-control" value="${user.birth}" placeholder="2012-12-20"/></div>
						</div>
					</div>
					<div class="form-control-static"><c:choose>
						<c:when test="${empty user.birth}"><span class="text-muted">填写</span></c:when><c:otherwise><c:out value="${user.birth}"/></c:otherwise>
					</c:choose></div>
				</div>
				<hr/>
				<div class="form-group clearfix">
					<label class="control-label" for="height">身高：</label>
					<div class="control hide">
						<div class="row">
							<div class="col-xs-5 col-sm-3 pull-right"><input type="number" name="height" id="height" class="form-control" maxlength="3" value="<c:out value="${user.height}"/>" placeholder="是厘米噢"/></div>
						</div>
					</div>
					<div class="form-control-static" data-format="{0} cm"><c:choose>
						<c:when test="${empty user.height}"><span class="text-muted">填写</span></c:when><c:otherwise><c:out value="${user.height}"/> cm</c:otherwise>
					</c:choose></div>
				</div>
				<hr/>
				<div class="form-group clearfix">
					<label class="control-label" for="career">职业：</label>
					<div class="control hide">
						<div class="row">
							<div class="col-xs-9 col-sm-6 pull-right"><input type="text" name="career" id="career" class="form-control" value="<c:out value="${user.career}"/>"/></div>
						</div>
					</div>
					<div class="form-control-static"><c:choose>
						<c:when test="${empty user.career}"><span class="text-muted">填写</span></c:when><c:otherwise><c:out value="${user.career}"/></c:otherwise>
					</c:choose></div>
				</div>
				<c:if test="${empty user.province}"><hr/>
				<div class="form-group clearfix">
					<label class="control-label" for="province">省份：</label>
					<div class="control hide">
						<div class="row">
							<div class="col-xs-9 col-sm-6 pull-right"><input type="text" name="province" id="province" class="form-control" value="<c:out value="${user.province}"/>"/></div>
						</div>
					</div>
					<div class="form-control-static"><c:choose>
						<c:when test="${empty user.province}"><span class="text-muted">填写</span></c:when><c:otherwise><c:out value="${user.province}"/></c:otherwise>
					</c:choose></div>
				</div>
				<hr/>
				<div class="form-group clearfix">
					<label class="control-label" for="city">市区：</label>
					<div class="control hide">
						<div class="row">
							<div class="col-xs-9 col-sm-6 pull-right"><input type="text" name="city" id="city" class="form-control" value="<c:out value="${user.city}"/>"/></div>
						</div>
					</div>
					<div class="form-control-static"><c:choose>
						<c:when test="${empty user.city}"><span class="text-muted">填写</span></c:when><c:otherwise><c:out value="${user.city}"/></c:otherwise>
					</c:choose></div>
				</div></c:if>
				<hr/>
				<p class="text-center">
					<input type="hidden" name="fromid" id="fromid" value="${user.fromid}"/>
					<button type="submit" class="btn btn-primary">&nbsp;&nbsp;保存修改&nbsp;&nbsp;</button>
				</p>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	$(function() {
		$('input[type="number"]').on('keypress', function(evt) {
			var e = window.event ? window.event : evt,
				key = e.charCode ? e.charCode : e.keyCode,
				ret = false;
			if (key == 8     // backspace
					|| key == 37 // left
					|| key == 39 // right
					|| key == 38 // up
					|| key == 40 // down
					|| key == 9  // TAB
					|| (key >= 48 && key <= 57))
				return true;
			else
				return false;
		});

		$('input[type="date"]').scroller($.extend(mobiscrollOption['date'], mobiscrollOption['default']));
		
		$('.info-form .form-control-static').on('click', function() {
			var $this = $(this).addClass('hide');
			$this.prevAll('.control').removeClass('hide').find('.form-control').focus();
		});
		$('.info-form .control-label').on('click', function() {
			$(this).nextAll('.form-control-static').trigger('click');
		});
		$('.info-form .form-control').each(function() {
			var $this = $(this), $control = $this.closest('.control'),
				$info = $control.nextAll('.form-control-static'), format = $info.data('format');
			$this.on('focus', function() {
				$control.removeClass('hide');
				$info.addClass('hide');
			}).on('blur', function() {
				$control.addClass('hide');
				$info.removeClass('hide');
			}).on('change', function() {
				var val = $this.val();
				if (val)
					$info.text(format ? format.replace(/\{0\}/g, val) : val);
				else
					$info.html('<span class="text-muted">填写</span>');
			});
		});
	});
	</script>
<%@include file="headerfooter/footer0.jsp" %>