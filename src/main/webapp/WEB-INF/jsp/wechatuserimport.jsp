<%@ page language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="requireDatePicker" value="${true}"/>
<%@include file="headerfooter/header0.jsp" %>
<div class="page">
    <hr/>
    <div class="container">
        <form class="info-form" action="${webRoot}/wechatuser/importuser" method="post">
            昵称:<input name="nickname" id="nickname" type="text" value="${link.author}"/>

            <div class="form-group clearfix">
                <label class="control-label" for="brief">个性签名：</label>

                <div class="control">
                    <textarea id="brief" name="brief" class="form-control" placeholder="介绍一下自己吧~"><c:out
                            value="${link.content}"/></textarea>
                </div>
            </div>
            <hr/>
            <div class="form-group clearfix">
                <label class="control-label" for="sex">性别：</label>

                <div class="control">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-info">
                            <input type="radio" name="sex" id="sex-male" value="1"
                                   autocomplete="off"${user.sex eq '1' ? ' checked' : ''}/> &nbsp;&nbsp;&nbsp;男&nbsp;&nbsp;&nbsp;
                        </label>
                        <label class="btn btn-danger">
                            <input type="radio" name="sex" id="sex-female" value="2"
                                   autocomplete="off"${user.sex eq '2' ? ' checked' : ''}/> &nbsp;&nbsp;&nbsp;女&nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group clearfix">
                <label class="control-label" for="birth">生日：</label>

                <div class="control">
                    <div class="row">
                        <div class="col-xs-9 col-sm-6 pull-right"><input type="date" name="birth" id="birth"
                                                                         class="form-control" value="1987-12-20"
                                                                         placeholder="2012-12-20"/></div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group clearfix">
                <label class="control-label" for="height">身高：</label>

                <div class="control">
                    <div class="row">
                        <div class="col-xs-5 col-sm-3 pull-right"><input type="number" name="height" id="height"
                                                                         class="form-control" maxlength="3" value=""
                                                                         placeholder="是厘米噢"/></div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group clearfix">
                <label class="control-label" for="career">职业：</label>

                <div class="control">
                    <div class="row">
                        <div class="col-xs-9 col-sm-6 pull-right"><input type="text" name="career" id="career"
                                                                         class="form-control" value=""/></div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group clearfix">
                <label class="control-label" for="province">省份：</label>

                <div class="control">
                    <div class="row">
                        <div class="col-xs-9 col-sm-6 pull-right"><input type="text" name="province" id="province"
                                                                         class="form-control"
                                                                         value="<c:out value="${user.province}"/>"/>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group clearfix">
                <label class="control-label" for="city">市区：</label>

                <div class="control">
                    <div class="row">
                        <div class="col-xs-9 col-sm-6 pull-right"><input type="text" name="city" id="city"
                                                                         class="form-control"
                                                                         value="<c:out value="${user.city}"/>"/></div>
                    </div>
                </div>
            </div>
            <hr/>
            <c:forEach var="img" items="${requestScope.images}">

                <div class="item">
                    <input type="hidden" value="${img.img_link}" name="imgurl" id="input${img.id}"/>

                    <div id="url_kill_referrer${img.id}" style="width:236px;height:auto;"></div>
                    <script>
                        $(function () {
                            $('#url_kill_referrer${img.id}').html(ReferrerKiller.imageHtml("${img.img_link}", {width: '240px', height: 'auto'}));
                        });
                    </script>
                    <button type="button" class="btn btn-danger" data-id="${img.id}">不显示</button>
                        <%--<button type="button" class="btn btn-success" data-id="${img.id}" style="display:none">显示</button>--%>
                </div>

            </c:forEach>
            <p class="text-center">
                <button type="submit" class="btn btn-primary">&nbsp;&nbsp;保存修改&nbsp;&nbsp;</button>
            </p>
        </form>
    </div>
</div>

<%@include file="headerfooter/footer0.jsp" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-danger").on('click', function () {
            var tmp = $(this).data('id');
            $('#input' + tmp).remove();
        });
    });
    /*
     $(".btn-success").on('click',function(){

     });*/
</script>