﻿(function(window, $, undefined) {
var
	currYear = (new Date()).getFullYear(),
	
	opt = {
		date : { preset: 'date' },
		//datetime : { preset : 'datetime', minDate: new Date(2012,3,10,9,22), maxDate: new Date(2014,7,30,15,44), stepMinute: 5 },
		datetime : { preset: 'datetime' },
		time : { preset: 'time' },
		'default' : {
			theme: 'android-ics light', // 皮肤样式
			display: 'modal', // 显示方式 
			mode: 'scroller', // 日期选择模式
			lang: 'zh',
			startYear: currYear - 70, // 开始年份
			endYear: currYear,  // 结束年份
			onSelect: function () { },
			onCancel: function () { }
		}
	};

window.mobiscrollOption = opt;

$.mobiscroll.i18n.zh = $.extend($.mobiscroll.i18n.zh, {
	dateFormat: 'yyyy-mm-dd',
	dateOrder: 'yymmdd',
	dayNames: ['周日', '周一;', '周二;', '周三', '周四', '周五', '周六'],
	dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
	dayText: '日',
	hourText: '时',
	minuteText: '分',
	monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
	monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
	monthText: '月',
	secText: '秒',
	timeFormat: 'HH:ii',
	timeWheels: 'HHii',
	yearText: '年',
	setText: '确定',
	cancelText: '取消'
});
})(window, jQuery);
