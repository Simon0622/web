package lehuo.lsm.test.dao;


import lehuo.lsm.dao.CommentDao;
import lehuo.lsm.model.*;
import lehuo.lsm.service.impl.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class CommentDaoTest {
	private ClassPathXmlApplicationContext context;
    private ActivityService activityService;
    CommentService commentService;
    ImagesService imagesService;
    ImgPKService imgPKService;
    ImgScoreService imgScoreService;
    LinksService linksService;
    UserService userService;
    CommentDao commentDao;

	@Before
	public void init() {
		context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        activityService = (ActivityService) context.getBean("activityService");
        commentService = (CommentService) context.getBean("commentService");
        imagesService = (ImagesService) context.getBean("imagesService");
        imgPKService = (ImgPKService) context.getBean("imgPKService");
        imgScoreService = (ImgScoreService) context.getBean("imgScoreService");
        linksService = (LinksService) context.getBean("linksService");
        userService = (UserService) context.getBean("userService");
        commentDao = (CommentDao)context.getBean("commentDao");
	}

    @Test
    public void testUserInsert() {
        //activityService.insert(new Activity("name",0,0,"content","location"));
        Comment c = new Comment(1,"1",1);
        commentService.insert(c);
        System.out.println(c.getId());
        User user = new User("simon","aaa","bbb","127.0.0.1");
        userService.insert(user);
        System.out.println(user.getId());
        /*imagesService.insert(new Images(0,"",0,"path"));
        imgPKService.insert(new PkImg(1,1,2,2));
        imgScoreService.insert(new ImgScore(0,0,1));
        linksService.insert(new Links("link","domain",10,"",10));
        userService.insert(new User());*/

    }

    @Test
    public void testBatchInsert(){
        Activity activity = new Activity("tutu",0,11,"","","","asd");
        Activity activity1 = new Activity("tutu",0,11,"","","","abc");
        List<Activity> list = new ArrayList<Activity>();
        list.add(activity);
        list.add(activity1);
        activityService.batchInsert(list);
    }

    @Test
    public void testUserSelect() {
        Activity a = new Activity();
        a.setId(1);
        activityService.select(a);
        Images i = new Images();
        i.setId(1);
        imagesService.select(i);
        PkImg pk = new PkImg();
        pk.setId(1);
        imgPKService.select(pk);
        ImgScore is = new ImgScore();
        is.setId(1);
        imgScoreService.select(is);
        Links links = new Links();
        links.setId(1);
        linksService.select(links);
        User u = new User();
        u.setId(1);
        userService.select(u);

        Comment c = new Comment();
        c.setId(1);
        commentService.select(c);
    }

    @Test
    public void testUserdelete() {
        Activity a = new Activity();
        a.setId(1);
        activityService.delete(a);
        Images i = new Images();
        i.setId(1);
        imagesService.delete(i);
        PkImg pk = new PkImg();
        pk.setId(1);
        imgPKService.delete(pk);
        ImgScore is = new ImgScore();
        is.setId(1);
        imgScoreService.delete(is);
        Links links = new Links();
        links.setId(1);
        linksService.delete(links);
        User u = new User();
        u.setId(1);
        userService.delete(u);

        Comment c = new Comment();
        c.setId(1);
        commentService.delete(c);
    }

    @Test
    public void testUserUpdate() {
        Activity a = new Activity();
        a.setId(1);
        a.setActivityname("aa");
        activityService.update(a,a);
        Images i = new Images();
        i.setId(1);
        i.setImgshow(2);
        imagesService.update(i,i);
        PkImg pk = new PkImg();
        pk.setId(1);
        pk.setDeal(2);
        //imgPKService.update(pk,pk);
        ImgScore is = new ImgScore();
        is.setId(1);
        is.setScore(1);
        imgScoreService.update(is,is);
        Links links = new Links();
        links.setId(1);
        links.setDeal(1);
        linksService.update(links,links);
        User u = new User();
        u.setId(1);
        u.setSessionid("asd");
        userService.update(u,u);

        Comment c = new Comment();
        c.setId(1);
        c.setComment("a");
        commentService.update(c,c);
    }

    @Test
    public void testselectList() {
        activityService.selectList(null,null,null);
        imagesService.selectList(null,null,null);
        imgPKService.selectList(null,null,null);
        imgScoreService.selectList(null,null,null);
        linksService.selectList(null,null,null);
        userService.selectList(null,null,null);
        commentService.selectList(null,null,null);
    }

    @Test
    public void testselectCount() {
        activityService.selectCount(null);
        imagesService.selectCount(null);
        imgPKService.selectCount(null);
        imgScoreService.selectCount(null);
        linksService.selectCount(null);
        userService.selectCount(null);
        commentService.selectCount(null);

    }

    @Test
    public void testImgupdate(){
        Images update = new Images();
        update.setUserid(123168);
        Images where = new Images();
        where.setId(123168);
        imagesService.update(update,where);
    }
}