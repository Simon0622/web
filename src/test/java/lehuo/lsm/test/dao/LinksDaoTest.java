package lehuo.lsm.test.dao;

import lehuo.lsm.dao.LinksDao;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;

public class LinksDaoTest {
	private ClassPathXmlApplicationContext context;

    @Resource
    private LinksDao linksDao;

	@Before
	public void init() {
		context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        linksDao = (LinksDao) context.getBean("linksDao");

	}

	@Test
	public void testMaxid(){
        Integer i = linksDao.selectMax();
        System.out.println(i);
    }
}