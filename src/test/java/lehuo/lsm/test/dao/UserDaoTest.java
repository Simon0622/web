package lehuo.lsm.test.dao;

import lehuo.lsm.dao.UserDao;
import lehuo.lsm.model.User;

import lehuo.lsm.service.impl.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class UserDaoTest {
	private ClassPathXmlApplicationContext context;
	private UserDao userDao;

    @Resource
    private UserService userService;
	@Before
	public void init() {
		context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        userService = (UserService) context.getBean("userService");
	}

	@Test
	public void testUserInsert() {

	}

    @Test
    public void testUserSelect() {

    }

    @Test
    public void testUserUpdate() {
        User user = new User();
        user.setId(4);
        user.setCity("大连");
        userService.update(user,new User("simon"));
    }

    @Test
    public void testselectList() {
        List<User> list = userService.selectList(null,0,20);
        System.out.println(list.size());
    }

    @Test
    public void testselectCount() {
        Integer integer = userService.selectCount(null);
        System.out.println(integer);
    }
}