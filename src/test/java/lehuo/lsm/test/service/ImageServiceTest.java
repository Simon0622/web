package lehuo.lsm.test.service;

import lehuo.lsm.model.Images;
import lehuo.lsm.model.PkImg;
import lehuo.lsm.service.impl.ImagesService;
import lehuo.lsm.service.impl.ImgPKService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class ImageServiceTest {
	private ClassPathXmlApplicationContext context;

    @Resource
    private ImagesService imagesService;

	@Before
	public void init() {
		context = new ClassPathXmlApplicationContext("spring-servlet.xml");
        imagesService = (ImagesService) context.getBean("imagesService");
	}

    @Test
    public void selectRankList() {
        List<Images> list = imagesService.selectList(null,0,20);
        System.out.printf(list.toString());
    }

}