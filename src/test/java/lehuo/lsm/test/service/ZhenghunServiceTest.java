/********************************************************************
 * File Name:    ZhenghunServiceTest.java
 *
 * Date Created: May 11, 2015
 *
 * ------------------------------------------------------------------
 * Copyright (C) 2010 Symantec Corporation. All Rights Reserved.
 *
 *******************************************************************/

// PACKAGE/IMPORTS --------------------------------------------------
package lehuo.lsm.test.service;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import lehuo.lsm.model.WechatModel.db.Zhenghun;
import lehuo.lsm.service.impl.ZhenghunService;

/**
 * TODO: Update with a detailed description of the interface/class.
 *
 */
public class ZhenghunServiceTest
{
  private ClassPathXmlApplicationContext context;
  @Resource
  private ZhenghunService zhenghunService;

  @Before
  public void init()
  {
    context = new ClassPathXmlApplicationContext("spring-servlet.xml");
    zhenghunService = (ZhenghunService) context.getBean("zhenghunService");
  }

  @Test
  public void testZhSelect()
  {
    Zhenghun zh = (Zhenghun) zhenghunService.select(new Zhenghun("MM1"));
    System.out.println(zh);
    System.out.println(zh.getTitle());
  }
  // CONSTANTS ------------------------------------------------------

  // CLASS VARIABLES ------------------------------------------------

  // INSTANCE VARIABLES ---------------------------------------------

  // CONSTRUCTORS ---------------------------------------------------

  // PUBLIC METHODS -------------------------------------------------

  // PROTECTED METHODS ----------------------------------------------

  // PRIVATE METHODS ------------------------------------------------

  // ACCESSOR METHODS -----------------------------------------------

}
