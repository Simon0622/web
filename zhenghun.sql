-- MySQL dump 10.13  Distrib 5.6.21, for Win64 (x86_64)
--
-- Host: localhost    Database: gaoxiao
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zhenghun`
--

DROP TABLE IF EXISTS `zhenghun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zhenghun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `imgurl` varchar(150) DEFAULT NULL,
  `num` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zhenghun`
--

LOCK TABLES `zhenghun` WRITE;
/*!40000 ALTER TABLE `zhenghun` DISABLE KEYS */;
INSERT INTO `zhenghun` VALUES (23,'他她鹊桥|第一期女嘉宾:美女就是任性','http://mp.weixin.qq.com/s?__biz=MjM5OTc1MjQ0Ng==&mid=204860321&idx=1&sn=4d1a7d9b213b1067cd37a2bb67a3874d#rd','https://mmbiz.qlogo.cn/mmbiz/BJxHz63KT76cbEBatylVObQYfwkAKHiaibibxRbCv3bZX7KZCZzqurERb6ZHtBPzPGZb5aibDgiar97kAa6pNTHOy4g/0?wx_fmt=jpeg','MM1'),(24,'他她鹊桥|第二期女嘉宾：文艺理工女孩儿','http://mp.weixin.qq.com/s?__biz=MjM5OTc1MjQ0Ng==&mid=204860696&idx=1&sn=a5d8831ce6863fa767fe42e17a382d3a#rd','https://mmbiz.qlogo.cn/mmbiz/BJxHz63KT76cbEBatylVObQYfwkAKHiaibI6I2oEMEZTnoicyYj5miazxOwFYNO8U99JCS60Tz6q2WRMoCic3zCmofw/0?wx_fmt=jpeg','MM2'),(25,'他她鹊桥|第一期男嘉宾:重感情的89央企GG静等你来','http://mp.weixin.qq.com/s?__biz=MjM5OTc1MjQ0Ng==&mid=204860926&idx=1&sn=46f65a9f27ec0e207996f46717ab4169#rd','https://mmbiz.qlogo.cn/mmbiz/BJxHz63KT76cbEBatylVObQYfwkAKHiaibcMMAFmjmD02pbGXa564S2aUrT68rDSHNeHZ2zuZBL0ZLwLkSwXSNAg/0?wx_fmt=jpeg','GG1'),(26,'他她鹊桥|第二期男嘉宾:88年北京土著男','http://mp.weixin.qq.com/s?__biz=MjM5OTc1MjQ0Ng==&mid=204861146&idx=1&sn=ab9c1a0a17e15bca47a5ff802598b614#rd','https://mmbiz.qlogo.cn/mmbiz/BJxHz63KT76cbEBatylVObQYfwkAKHiaibEbCnAg2LoicjNibBgtxIKmFib8mmz2NUEaDM2R1rmmLe9cQsbG3lbKeicQ/0?wx_fmt=jpeg','GG2');
/*!40000 ALTER TABLE `zhenghun` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-10 18:47:24
